# SureBell

[![CI Status](http://img.shields.io/travis/Krishna Thakur/SureBell.svg?style=flat)](https://travis-ci.org/Krishna Thakur/SureBell)
[![Version](https://img.shields.io/cocoapods/v/SureBell.svg?style=flat)](http://cocoapods.org/pods/SureBell)
[![License](https://img.shields.io/cocoapods/l/SureBell.svg?style=flat)](http://cocoapods.org/pods/SureBell)
[![Platform](https://img.shields.io/cocoapods/p/SureBell.svg?style=flat)](http://cocoapods.org/pods/SureBell)

SureBellSDK is a complete SureBell integration library written in Swift.

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

    -iOS 10.0+ / watchOS 2.0+
    -Xcode 8.3+
    -Swift 3.2+

## Installation

### CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

> CocoaPods 1.1+ is required to build SureBell.

To integrate SureBell into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '10.0'
use_frameworks!

target '<Your Target Name>' do
    pod 'SureBell', :git => 'https://gitlab.com/thakurkrishna/SureBell.git'
end
```

Then, run the following command:

```bash
$ pod install
```

SureBell is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

### Manually

If you prefer not to use any of the aforementioned dependency managers, you can integrate SureBell into your project manually.

#### Embedded Framework

- Open up Terminal, `cd` into your top-level project directory, and run the following command "if" your project is not initialized as a git repository:

  ```bash
  $ git init
  ```

- Add SureBell as a git [submodule](http://git-scm.com/docs/git-submodule) by running the following command:

  ```bash
  $ git submodule add https://gitlab.com/thakurkrishna/SureBell.git
  ```

- Open the new `SureBell` folder, and drag the `SureBell.xcodeproj` into the Project Navigator of your application's Xcode project.

    > It should appear nested underneath your application's blue project icon. Whether it is above or below all the other Xcode groups does not matter.

- Select the `SureBell.xcodeproj` in the Project Navigator and verify the deployment target matches that of your application target.
- Next, select your application project in the Project Navigator (blue project icon) to navigate to the target configuration window and select the application target under the "Targets" heading in the sidebar.
- In the tab bar at the top of that window, open the "General" panel.
- Click on the `+` button under the "Embedded Binaries" section.
- You will see two different `SureBell.xcodeproj` folders each with two different versions of the `SureBell.framework` nested inside a `Products` folder.

    > It does not matter which `Products` folder you choose from, but it does matter whether you choose the top or bottom `SureBell.framework`.

- Select the top `SureBell.framework` for iOS.

    > You can verify which one you selected by inspecting the build log for your project. The build target for `SureBell`.

- And that's it!

  > The `SureBell.framework` is automagically added as a target dependency, linked framework and embedded framework in a copy files build phase which is all you need to build on the simulator and a device.
  
# AccessToken (Login)
The following getToken() function will generate access_token for the particular registered user.
If the specified user exists then the called function returns success response with the generated
access_token or it gives the failure response with the respected error code.
```swift
Token.instance.getToken(vendorKey: "vendor key",userName: "sample@gmail.com", password: "sample", callback: {
(token) in
    print(token.access_token)
}) { (error) in
if let error =  error as? APIError{
    guard  let des = error.error_description else{return}
	print(des)	
 }else if let error =  error as? Error{
    guard let error = error as? Error else{return}
	print(error)
 }
}
```
The response you will get.

```swift
{
  	"token_type": "bearer",
  	"Access_token":
"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjbGllbnRJZCI6IjU2ODNhYTMzZTU2NGQ2MTQyNWIyOWNiMSIsIm5hbWUiOiJOaXRlZXNoIEt1bWFyIiwiaWF0IjoxNTI1MzM5NzUyLCJleHAiOjE1MjUzNDMzNTIsImF1ZCI6IjVhNjBiOWRiZTQzMjlmMDAxMGFmYWEyOCIsImlzcyI6ImF1dGgucGlvY3RhdmUuY29tIiwic3ViIjoiYWNjZXNzVG9rZW4ifQ.Wwwck8ObVp8YNDw5fGN2fml0hUV_ToGpQQxkrys_i3M",
  	"expires_in": 3600,
 	 "refresh_token": "8224636dcc808ab7615a76f9b5a28f6d693b6d86"
} 
```
# Error in response
```swift
if let error =  error as? APIError{
    guard  let des = error.error_description else{return}
	print(des)	
 }else if let error =  error as? Error{
    guard let error = error as? Error else{return}
	print(error)
 }
```

# Register New User 
The create() function helps to register new user on sure server.
If the creation is proper then sure server gives success response with successful registration or it gives failure response with respective error code. 
```swift
User.instance.create(firstName: "sample", lastName: "test", email: "sample@gmail.com",phone: "9876598765", password: "123456", callback: { (user) in 
    print(user.firstname)
}) { (error) in
    //Deal with error
}

```

# Forgot Password 
The forgotPassword() function helps user to reset their password using the code they have received if they have forgotten their old password.
If it successful then server gives success response with generated code for the particular user to reset their password or gives failure response with respective error code.
```swift
User.instance.forgotPassword(userName: "sample2@gmail.com", callback: {(StatusResponse) in 
    print(StatusResponse)
}) { (error) in            
    //Deal with error
}

```

# Reset Password 
As specified in forgotPassword() function, it gives unique code(OTP), by using that user can reset their password using resetPassword() function.
Unique code will be sent to their registered email ID. If the reset
is done properly then it gives success response or it gives failure response with the respective error code.
```swift
User.instance.resetPassword(parameters: ["password_reset_code": "42252","email": "sample2@gmail.com", "password": "123456"], callback: { (StatusResponse) in 
    print(StatusResponse) 
}) { (error) in            
    //Deal with error
}

```

# Generate Login OTP 
User will be having two options to login either through their email or through the OTP.
While registering user should provide his email and phone number.
This generateLoginOtp() function helps user to get OTP to their registered phone number to login.
If the called function is successful then it sends OTP to the registered phone number with success response or it gives failure response with the respective error code.
```swift
User.instance.generateLoginOtp(phone: "+919876543210", callback: { (StatusResponse) in 
    print(StatusResponse)
}) { (error) in            
    //Deal with error
}

```

# Verify Email
Once the registration is successful then user has to verify their email and phone number to avoid mistakes and for security purpose.
This verifyEmail() function helps user to get unique code(OTP) to verify their registered email ID.
Success response says that verification is successful or it gives failure response with the respective error code.  
```swift
User.instance.verifyEmail(id: "532bcd456gfd1b2fc3g4sd36", code: "95023", callback: {(StatusResponse) in 
    print(StatusResponse)
}) { (error) in            
    //Deal with error
}

```

# Verify Phone
Once the registration is successful then user has to verify their email and phone number to avoid mistakes and for security purpose.
This verifyPhone() function helps user to get unique code(OTP) to verify their registered Phone number.
Success response says that verification is successful or it gives failure response with the respective error code.  
```swift
User.instance.verifyPhone(id: "532bcd456gfd1b2fc3g4sd36", code: "63438", callback: {(StatusResponse) in 
    print(StatusResponse) 
}) { (error) in            
    //Deal with error
}

```

# Get User Details
This getMe() function helps to get details of successfully registered user. If the requested user is available
on sure server then it responses back success response with the user details or it gives failure response with
the respective error code.
```swift
User.instance.getMe(callback: { (user) in
    print(user.firstname) 
}) { (error) in            
    //Deal with error
}

```

# Resend Email Verification Code
While verifying user’s email and phone number, sometimes user won’t get unique code(OTP) in their inbox.
In this case user will be having one more option to regenerate this code that is resendEmailVerification() function.
It sends Verification code to the specified user’s email once again.
If the called function is successful then it gives success response with resent code or it gives failure response with the respective error code.
```swift
User.instance.resendEmailVerification(callback: {(StatusResponse) in
    print(StatusResponse)
}) { (error) in
    //Deal with error
}

```

# Resend Phone Verification Code
While verifying user’s email and phone number, sometimes user won’t get unique code(OTP) in their inbox.
In this case user will be having one more option to regenerate this code that is resendPhoneVerificationCode() function.
It sends Verification code to the specified user’s phone number once again.
If the called function is successful then it gives success response with resent code or it gives failure response with the respective error code.
```swift
User.instance.resendPhoneVerificationCode(id: "532bcd456gfd1b2fc3g4sd36", callback: {(StatusResponse) in 
    print(StatusResponse)
}){(error)in
    //Deal with error
}

```

# Logout From Other Devices
As we have mentioned in the NOTE, logoutFromOtherDevices() function requires generation of
access_token. Once it is done this function logs out the specified email ID from all other devices other
than the currently working one which will be the success response from server. If the called function
fails then it gives failure  response with the respective error code.
```swift
User.instance.logoutFromOtherDevices(callback:{(StatusResponse)in 
    print(StatusResponse)
}){(error)in
    //Deal with error
}

```

# Update User Details
This updateMe() function helps user to update their profile information it can be their name, email or
phone number. Success response comes with the updated user information or it gives failure response
with the respective error code.
```swift
let user = User()
user._id = "543209876543210987654321"
User.instance.updateMe(userUpdate: user, callback: { (user) in 
    print(user.firstname)
}) { (error) in
    //Deal with error
}

```

# updateNotification Token
Here if user wants to get notifications from their device then they have to send this notification token
from their registered device to the server using this updateNotificationToken() function,  then server
will push all notifications to the specified device. If the updation of this Notification Token to the server
is successful then server sends notifications or it gives failure response with the respective error code.
```swift
User.instance.updateNotificationToken(token: "abcd", device: "ios", callback: { (user) in 
    print(user.firstname)
}) { (error) in
    //Deal with error
}

```

# User Logout
When user wants to logout from their application then they can use these  deleteNotificationToken() and deleteToken() functions.
```swift
User.instance.deleteNotificationToken(token: "abcd", callback: { (user) in
    print(user.firstname)
}) { (error) in
    //Deal with error
}
Token.instance.deleteToken()
 
* Delete your all local cached data from phone

```

# Update Password
This updatePassword() function helps user to change their password. Success response from the server
will be the updated password and failure response gives error with the respective error code.
```swift
User.instance.updatePassword(parameters: ["currentPassword": "sample","newPassword":"sample"], callback: { (user) in 
    print(user.firstname)
}) { (error) in
    //Deal with error
}

```

# Get Device with Id
To get configurations of a successfully registered device on server, user needs to call getDevice() call.
If you want to fetch updated device details from server, pass the boolean value as  true otherwise for false sdk gives device details from cache, if it is available or else it will fetch from the server.
Failure response gives error with the respective error code.
```swift
Device.instance.getDevice(id: "585326897542157909654321", callback: {(device) in 
    print)(device.name)
}) { (error) in
    //Deal with error
}

```

# Invite User
User is having a option to invite other users through their email/phone number using inviteUser()
function to access specific device. After becoming a user or admin by accepting that invitation other
users can also access the same device. This function needs generation of access_token. If the called
function fails then the invited user can’t access specific device. Failure response gives error with the
respective error code.
```swift
Device.instance.inviteUser(id: "585326897542157909654321", user: "sample1@gmail.com", callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Get Event
To get latest notification on device (Mobile Phone) we need getEvent() function. Successful
response gives latest notification as per specified event id and failure response gives error with the
respective error code.
```swift
Device.instance.getEvents(id: "585326897542157909654321", callback: {(event) in 
    print(event)
}) { (error) in
    //Deal with error
}

```

# Get Events
To get notifications from the device, it should have access token and the notification token of that device
should be there on the server. After this by calling getEvents() function user can get notifications from that
particular device. Failure response gives error with the respective error code.
```swift
Device.instance.getEvents(id: "585326897542157909654321", callback: {(event) in 
    print(event)
}) { (error) in
    //Deal with error
}

```

# Reject Invite
To reject the invitation, user can use rejectInvite() function. Failure response gives error with the
respective error code.
```swift
Device.instance.rejectInvite(id: "585326897542157909654322", callback: {(StatusResponse) in 
    print(StatusResponse)
}) { (error) in
    //Deal with error
}

```

# Accept Invite
After receiving invitation from the user, if the invited person wants to access that device then he can
accept that invitation using acceptInvite() function and this function needs generation of
access_token.Failure returns error with the respective error code.
```swift
Device.instance.acceptInvite(id: "585326897542157909654322", callback: {(device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Get Admin Rights
To give admin right to existing particular user, user can use inviteAdmin() function. Failure response
gives error with the respective error code.
```swift
Device.instance.inviteAdmin(id: "585326897542157909654321", user:"532bcd456gfd1b2fc3g4sd36", callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Remove Admin
To revoke the admin rights for the specific user, user can use removeAdmin() function. Failure
response gives error with the respective error code.
```swift
Device.instance.removeAdmin(id: "585326897542157909654321", user:"532bcd456gfd1b2fc3g4sd36", callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Remove User
If user wants to remove a particular user from device access then they can use this removeUser()
function. Failure response gives the error with the respective error code.
```swift
Device.instance.removeUser(id: "585326897542157909654321", user:"532bcd456gfd1b2fc3g4sd36", callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Remove Invite
After sending invitation to a particular person if user wants to remove that invitation then they can
using this removeInvite() function. Failure response gives error with the respective error code.
```swift
Device.instance.removeInvite(id: "585326897542157909654321", user:"sample@gmail.com", callback: { (device) in
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Remove Me
If user wants to remove himself from a particular there is an option called remove device in application
for that it needs removeMe() function. Failure response gives error with the respective error code.
```swift
Device.instance.removeMe(id: "585326897542157909654321", user:"532bcd456gfd1b2fc3g4sd36", callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Get Devices
The following getDevices() function lists down the number of devices which are successfully
registered on the server with their configurations.
This function needs generation of access_token.
If you want to fetch updated devices lists from server pass the boolean value as true otherwise for false sdk gives devices lists from cache if it is available or else it will fetches from the server.
If the registered devices are not found or if it finds any other error it returns failure response with the respected error code.
```swift
Device.instance.getDevices(callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Update Cloud Upload Status
User can enable and disable uploading of events to cloud to avoid unnecessary use of data by using
changeCloudUploadStatus() function. Failure response gives error with the respective error code.
```swift
Device.instance.changeCloudUploadStatus(id: "585326897542157909654321",type: true,callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Change Chime Status
The following changeChimeStatus() function will be used to enable and disable the chime settings.
Failure response gives error with the respective error code.
```swift
Device.instance.changeChimeStatus(id: "585326897542157909654321", type:true,callback:{ (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Motion Sensitivity
To avoid unnecessary generation of events user is having option to set the sensitivity level of the device
as low(0), medium(1) and high(2) using motionSensitivity() function . As per user settings events will
be generated in less numbers. This function also needs the access_token. If the called function fails it
gives failure response with the respective error code.
```swift
Device.instance.motionSensitivity(id: "585326897542157909654321",motionSensitivity:1, callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Audio Settings
User is also having an option to set audio quality of the device as low(0), medium(1) and high(2) using
audioSettings() function. So this includes both mic and speaker audio quality. If call fails it
gives failure response with the respective error code.
```swift
Device.instance.audioSettings(id: "585326897542157909654321", micGain: 2,speakerVol: 2, callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Change Device TimeZone
User can change their timezone as per their location by using this timeZone() call. To do that
it’s needed to be provide only  + or - value in string format. Example India is having “GMT +5:30”
time, from this string it requires only “+5:30”. Failure response gives error with the respective error
code.
```swift
Device.instance.timeZone(id: "585326897542157909654321", timeZone:"+5.30",callback:{ (device) in
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Delete Event
If the user is a admin of particular device, then he can delete unwanted events from his notification list
by using deleteEvent() function. Failure response gives error with the respective error code.
```swift
Device.instance.deleteEvent(device_id: "585326897542157909654321",event_id:"5adebb6edda13b001a1337a0", callback: { (status) in
    print(status)
}) { (error) in
    //Deal with error
}

```

# Share Event
If user wants to share a particular event video with others he can share using shareEvent() function
only if that user is a admin for that specified device. If the called function fails it gives failure response
with the respective error code.
```swift
Device.instance.shareEvent(event_id: "5adef3addda123001a123d31", shared:true,callback: { (event) in 
    print(event)
}) { (error) in
    //Deal with error
}

```

# Change Event Status
Following changeEventStatus() function helps user to distinguish attended and missed ring/motion
events. By default events status will be missed, if user receives ring event or if he views the received
motion event, then the status of that event will change as attended event.Successful response reflects
the changes in notification list as attended or missed events. Failure response gives error with the
respective code.
```swift
Device.instance.changeEventStatus(event_id: "5adef3addda123001a123d31",status:"missed", callback: { (event) in 
    print(event)
}) { (error) in
    //Deal with error
}

```

# Get Device Data Usage
To get the total data used by device to upload events to cloud in specific period of time we need
getDataUseByDevice() function. This call returns response in DataUseByDevice model class, where
it gives total image size and video size in bytes. Failure response gives error with the respective error
code.
```swift
Device.instance.getDataUseByDevice(id: "585326897542157909654321", from:"2018-03-01", to: "2018-04-01", callback: { (data) in 
    print(data)
}) { (error) in
    //Deal with error
}

```

# Get Events By Date and Offset
If user wants to view the events on particular date of the month he can view by using getEventsByDate()
function. If called function is successful it gives the list of events on that particular date or gives failure
response with the respective error code.
```swift
let tomorrow = (Calendar.current as NSCalendar).date(byAdding: .day,value: 1,to: Date(),options: [])
Device.instance.getEventsByDate("585326897542157909654321", from: Date(),to:tomorrow!, offset: 0, callback: { (event) in 
    print(event)
}) { (error) in
    //Deal with error
}

```

# Video Settings
User can also set the quality for recording and live videos as low, medium and high by using
videoSettings() as function and by passing device id and value for recording and live video as
parameters. If the called function fails then server returns failure response.
```swift
let live = Live()
let recording = Recording()
live.quality = 2
recording.quality = 2
Device.instance.videoSettings(id: "585326897542157909654321", recording:recording,live: live, callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Device Available
User can check whether the required device is available on server or it is erased from server and whether is it working or not using deviceAvailable() function.
Failure response gives the respective error code.
```swift
Device.instance.deviceAvailable("585326897542157909654321", callback: {(device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Devices Available
User can also check whether the required list of devices are available on server or they erased from server and whether are they working or not using deviceAvailable() function.
Failure response gives the respective error code.
```swift
Device.instance.deviceAvailable(callback: { (device) in 
    print(device.name)
}) { (error) in
    //Deal with error
}

```

# Motion Settings
User can also set region of interest for the particular device to avoid unnecessary generation of events
by drawing a region in motion settings using motionSetting() function. In case of failure it
returns error with the respective error code.
```swift
   let controller =  MotionSettingViewController(device)
   self.present(controller, animated: false, completion: nil)

```

# Set Latest Image for Device
To get the latest captured image as the image to be displayed, when user gets call, user needs
setDeviceLatestImage() function. Failure response gives error with the respective error code.
```swift
Device.instance.setDeviceLatestImage(id: "585326897542157909654321",imageData:UIImagePNGRepresentation(im.image!)!, timeinms:
"\(Utils.getCurrentMillis())",callback: { (status) in
    print(status)
}) { (error) in
    //Deal with error
}

```

# Upload User Profile Image
If user wants to set their profile picture in sure application then they can use this
uploadProfilePicture() function. Successful response sets user’s profile picture and failure response
gives error with the respective error code.
```swift
User.instance.uploadProfileImage(imageData: UIImagePNGRepresentation(im.image!)!, callback: { (bool) in
    print(bool) 
}) { (error) in 
    //Deal with error
}}

```

# Get Ping Response
Make a ping to a device by using getPingResponse() function. Success response sends the pong
response and failure response gives the respective error code.
```swift
Device.instance.getPingResponse(ipAddress: "192.168.101.1", callback: { (ping) in 
    print(ping.response)
}) { (error) in
    //Deal with error
}

```

# Get SSID
After getPingRespone() function sends pong Response, make a getSSIDList() function. Success
response sends the ssid array list response and failure response gives the respective error code.
```swift
Device.instance.getSSIDList(ipAddress: "192.168.101.1", callback: { (ssids) in        
    print(ssids.first?.ssid)
    
}) { (error) in
    //Deal with error
}

```

# Device pairing

```swift
Device.instance.pairNewDevice(ipAddress: "192.168.101.1", channel: "3", ssid:
"Internet", encryption: "AES", password: "password12345", security:
"WPA2PSK", location: [21.28236, 19.28262], chime: true, name: "D364") { (Int) in 
    print(Int)
}

```

# Change Device Wifi

```swift
Device.instance.changeDeviceWifi(ipAddress: "192.168.101.1", channel: "4",
ssid: "internet2", encryption: "AES", password: "internetchange", security:
"WPA2PSK", adminToken: "12345", isSecondary: true) { (Int) in 
    print(Int)
}

```

# Set Chime

```swift
Device.instance.setChime(ipAddress: "192.168.101.1", isChimeEnabled: true,
user_id: "543209876543210987654321") { (Int) in 
    print(Int)
}

```

# Live Video Call
User can do a live view or video call to their device using getTurnServerValues() function.
Successful response connects the call and failure response fails to connect call.
You can initiate call (LiveViewController) in two ways one if user is making live call from app other if you are initiating call from notification better use CallKit framework. 
```swift
Process 1 : Make live view from Device List 

Device.instance.getTurnServerValues(callback: { (turns) in
let liveViewController =  LiveViewController(turns, device: devices, sessionKey: Utils.randomStringWithLength(15), callFrom: "Live")
self.present(liveViewController, animated: false, completion: nil)
 }) { (error) in }

Process 2 : Make call from ring notification using callKit framework 

*  Go to  ProviderDelegate class and write these lines in  reportIncomingCall method

Device.instance.getTurnServerValues(callback: { (turns) in
let liveViewController =  LiveViewController(turns, device: devices, sessionKey: Utils.randomStringWithLength(15), callFrom: "CallKit")
self.present(liveViewController, animated: false, completion: nil)
 }) { (error) in }

* Now in CXAnswerCallAction delegate method write these lines

internal func provider(_ provider: CXProvider, perform action: CXAnswerCallAction){
if let key = self.liveViewController?.session_key, let config = self.liveViewController?.sessionConfig,let phonertc = self.liveViewController?.phoneRtc{
     config.streams?.media = true
     if !phonertc.sessions.isEmpty {
     	phonertc.sendMediaSignal(session_key: key, config: config)
      }
     }
}


* while creating LiveViewController object make sure you are passing callFrom you need to pass as “CallKit” or “Live” case sensitive.

```

## Author

Krishna Thakur
krishna@pioctave.com
PiOctave Solutions (Pvt) Ltd

## License

SureBell is available under the MIT license. See the LICENSE file for more info.
