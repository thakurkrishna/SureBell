//
//  SSID.swift
//  SureBell
//
//  Created by PiOctave on 19/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import ObjectMapper
import SwiftyJSON
import Alamofire


/// <#Description#>
public class Ping:Mappable{
    public var response:String?
    public static var instance = Ping()
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self.response <- map["response"]
    }
}


/// <#Description#>
public class SSID:Mappable{
    
    public static var instance = SSID()
    
    public var encryption:String?
    public var ssid:String?
    public var sigstrength:String?
    public var security:String?
    public var mode:String?
    public var channel:String?
    public var frequency:String?
    public var bssid:String?
    public var connected:String?
    public var capability:String?
    
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    
    /// <#Description#>
    ///
    /// - Parameter map: <#map description#>
    public func mapping(map: Map) {
        self.encryption <- map["encryption"]
        self.ssid <- map["ssid"]
        self.sigstrength <- map["sigstrength"]
        self.security <- map["security"]
        self.mode <- map["mode"]
        self.channel <- map["channel"]
        self.frequency <- map["frequency"]
        self.bssid <- map["bssid"]
        self.connected <- map["connected"]
        self.capability <- map["capability"]
    }
}

