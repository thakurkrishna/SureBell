//
//  DeviceHttps.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 17/04/18.
//



import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

public class StatusResponse:Mappable{
    public static var instance = StatusResponse()
    
    public init(){}
    
    public var message:String?
    public var status:String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self.message <- map["message"]
        self.status <- map["status"]
    }
}

class DeviceHttps{
    
    static var instance = DeviceHttps()
    
    private init(){
        
    }
    
    enum Router: URLRequestConvertible {
        func asURLRequest() throws -> URLRequest {
            let url = try Router.baseURLString.asURL()
            
            var urlRequest = URLRequest(url: url.appendingPathComponent(path))
            urlRequest.httpMethod = method.rawValue
            switch self {
            case let .inviteUser(_ ,user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["user":user])
            case let .changeEventStatus(_ , status):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["status":status])
            case let .changeBulkEventStatus(ids, status):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["ids":ids, "status":status])
            case let .removeUser(_ , user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["user":user])
            case let .removeAdmin(_ , user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["user":user])
            case let .inviteAdmin(_ , user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["user":user])
            case let .removeInvite(_ , user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["user":user])
            case let .removeMe(_ , user):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["user":user])
            case let .acceptInvite(id):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["id":id])
            case let .rejectInvite(id):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["id":id])
            case let .deleteEvent(dId, eId):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["dId":dId, "eId":eId])
            case let .sendDeviceDiagnosis(id):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["id":id])
            case let .changeCloudUploadStatus(_ , status):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["status":status])
            case let .changeCloudUploadSettings(id, type):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["id":id, "type":type])
            case let .changeChimeStatus(_ , status):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["status":status])
            case let .changeTimeZone(id):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["id":id])
            case let .motionSensitivity(_ , motionSensitivity):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["motionSensitivity":motionSensitivity])
            case let .getEventsByDate(_ , from, to, offset):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: ["createdAt[$lt]":"\(to)", "createdAt[$gt]":"\(from)", "offset":"\(offset)"])
            case let .getDataUseByDevice( _, from, to):
                urlRequest = try URLEncoding.default.encode(urlRequest, with: ["from":"\(from)", "to":"\(to)"])
            case let .audioSettings(_ , audio):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: Mapper().toJSON(audio))
            case let .videoSettings(_ , recording, live):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["recording":Mapper().toJSON(recording), "live":Mapper().toJSON(live)])
            case let .motionSettings(_ , parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: parameters)
            case let .timeZone(_ , timeZone):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["timeZone":timeZone])
            case let .shareEvent(_ , shared):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["shared":shared])
            case let .changeDeviceName(_ , name):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["name":name])
            case let .setDeviceLatestImage(_,timeinms):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with: ["time":"\(timeinms)"])
            default:
                break
            }
            return urlRequest
        }
        
        
        static let baseURLString = OAuthConst.BASE_URL_STRING
        
        case getDevice(id:String)
        case inviteUser(id:String, user:String)
        case getEvents(id:String)
        case getDevices()
        case getEventsByDate(id:String, from:String, to:String, offset:Int)
        case shareEvent(event_id:String, shared:Bool)
        case changeEventStatus(event_id:String, status:String)
        case changeBulkEventStatus(event_ids:[String], status:String)
        case getDataUseByDevice(id:String, from:String, to:String)
        case removeUser(id:String, user:String)
        case removeAdmin(id:String, user:String)
        case inviteAdmin(id:String, user:String)
        case removeInvite(id:String, user:String)
        case deviceAvailable()
        case getDeviceAvailableWithId(id:String)
        case removeMe(id:String, user:String)
        case acceptInvite(id:String)
        case rejectInvite(id:String)
        case getEvent(dId:String, eId:String)
        case setDeviceLatestImage(id:String, timeinms:String)
        case deleteEvent(dId:String, eId:String)
        case getTurnServerValues()
        case sendDeviceDiagnosis(id:String)
        case changeCloudUploadStatus(id:String, type:Bool)
        case changeCloudUploadSettings(id:String, type:String)
        case changeChimeStatus(id:String, type:Bool)
        case changeTimeZone(id:String)
        case isDeviceConnected()
        case getWifiNetworkList()
        case pairDevice()
        case motionSensitivity(id:String, motionSensitivity:Int)
        case audioSettings(id:String, audio:Audio)
        case videoSettings(id:String, recording:Recording, live:Live)
        case motionSettings(id:String, motionSettings:Parameters)
        case timeZone(id:String, timeZone:String)
        case changeDeviceName(id:String, name:String)
        
        
        var method: HTTPMethod {
            switch self {
            case .getDevice:
                return .get
            case .inviteUser:
                return .put
            case .getEvents:
                return .get
            case .changeEventStatus:
                return .put
            case .changeBulkEventStatus:
                return .put
            case .getDevices:
                return .get
            case .getEventsByDate:
                return .get
            case .shareEvent:
                return .put
            case .getDataUseByDevice:
                return .get
            case .removeUser:
                return .put
            case .removeAdmin:
                return .put
            case .inviteAdmin:
                return .put
            case .removeInvite:
                return .put
            case .deviceAvailable:
                return .get
            case .getDeviceAvailableWithId:
                return .get
            case .removeMe:
                return .put
            case .acceptInvite:
                return .put
            case .rejectInvite:
                return .put
            case .getEvent:
                return .get
            case .setDeviceLatestImage:
                return .post
            case .deleteEvent:
                return .delete
            case .getTurnServerValues:
                return .get
            case .sendDeviceDiagnosis:
                return .post
            case .changeCloudUploadStatus:
                return .put
            case .changeCloudUploadSettings:
                return .put
            case .changeChimeStatus:
                return .put
            case .changeTimeZone:
                return .put
            case .isDeviceConnected:
                return .get
            case .getWifiNetworkList:
                return .get
            case .pairDevice:
                return .post
            case .motionSensitivity:
                return .put
            case .audioSettings:
                return .put
            case .videoSettings:
                return .put
            case .motionSettings:
                return .put
            case .timeZone:
                return .put
            case .changeDeviceName:
                return .put
            }
        }
        
        var path: String {
            switch self {
            case let .getDevice(id):
                return "/device/\(id)"
            case let .inviteUser(id , _):
                return "/device/\(id)/invite-user"
            case let .getEvents(id):
                return "/device/\(id)/event"
            case .getDevices:
                return "/device"
            case let .getEventsByDate(id, _, _, _):
                return "/device/\(id)/event"
            case let .shareEvent(eid, _):
                return "/piEvent/\(eid)/shared"
            case let .changeEventStatus(id, _):
                return "/piEvent/\(id)/status"
            case .changeBulkEventStatus:
                return "/piEvent/status-bulk-update"
            case let .getDataUseByDevice(id, _, _):
                return "/device/\(id)/usage"
            case let .removeUser(id, _):
                return "/device/\(id)/remove-user"
            case let .removeAdmin(id, _):
                return "/device/\(id)/remove-admin"
            case let .inviteAdmin(id, _):
                return "/device/\(id)/invite-admin"
            case let .removeInvite(id, _):
                return "/device/\(id)/remove-invite"
            case .deviceAvailable:
                return "/device/available"
            case let .getDeviceAvailableWithId(id):
                return "/device/\(id)/available"
            case let .removeMe(id, _):
                return "/device/\(id)/remove-me"
            case let .acceptInvite(id):
                return "/device/\(id)/accept-invite"
            case let .rejectInvite(id):
                return "/device/\(id)/reject-invite"
            case let .getEvent(dId, eId):
                return "/device/\(dId)/event/\(eId)"
            case let .setDeviceLatestImage(id, _):
                return "/device/\(id)/latest-image"
            case let .deleteEvent(dId, eId):
                return "/device/\(dId)/event/\(eId)"
            case .getTurnServerValues():
                return "/rtcCall/ice-servers"
            case let .sendDeviceDiagnosis(id):
                return "/device/\(id)/diagnostics"
            case let .changeCloudUploadStatus(id, _):
                return "/device/\(id)/cloud-upload-status"
            case let .changeCloudUploadSettings(id, type):
                return "/device/\(id)/cloud-upload-status/\(type)"
            case let .changeChimeStatus(id, _):
                return "/device/\(id)/chime-status"
            case let .changeTimeZone(id):
                return "/device/\(id)/time-zone"
            case .isDeviceConnected:
                return "/fcgi-bin/v1/pinge"
            case .getWifiNetworkList:
                return "/fcgi-bin/v1/networks"
            case .pairDevice:
                return "/fcgi-bin/v1/pair"
            case let .motionSensitivity(id, _):
                return "/device/\(id)/motion-sensitivity"
            case let .audioSettings(id, _):
                return "/device/\(id)/audio-settings"
            case let .videoSettings(id, _, _):
                return "/device/\(id)/video-settings"
            case let .motionSettings(id, _):
                return "/device/\(id)/motion-settings"
            case let .timeZone(id, _):
                return "/device/\(id)/time-zone"
            case let .changeDeviceName(id, _):
                return "/device/\(id)"
            }
            
        }
    }
    
    fileprivate func makeObjectRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping (Device) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: urlRequestConvertible).responseObject { (response:DataResponse<Device>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(device):
                callback(device)
            }
        }
    }
    
    fileprivate func makeArrayRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping ([Device]) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible:urlRequestConvertible).responseArray { (response:DataResponse<[Device]>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(devices):
                callback(devices)
            }
        }
    }
    
    fileprivate func makeDeviceAvailableObjectRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping (DeviceAvailable) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: urlRequestConvertible).responseObject { (response:DataResponse<DeviceAvailable>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(deviceAvailable):
                callback(deviceAvailable)
            }
        }
    }
    
    fileprivate func makeDeviceAvailableArrayRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping ([DeviceAvailable]) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible:urlRequestConvertible).responseArray { (response:DataResponse<[DeviceAvailable]>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(deviceAvailable):
                callback(deviceAvailable)
            }
        }
    }
    
    fileprivate func makeEventObjectRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping (Event) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: urlRequestConvertible).responseObject { (response:DataResponse<Event>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(event):
                callback(event)
            }
        }
    }
    
    fileprivate func makeDataUsageObjectRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping (DataUsage) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: urlRequestConvertible).responseObject { (response:DataResponse<DataUsage>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(dataUsage):
                callback(dataUsage)
            }
        }
    }
    
    fileprivate func makeStatusResponseObjectRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping (StatusResponse) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible: urlRequestConvertible).responseObject { (response:DataResponse<StatusResponse>) in
            print(String(data: response.data!, encoding: .utf8))
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(statusResponse):
                callback(statusResponse)
            }
        }
    }
    
    fileprivate func makeEventArrayRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping ([Event]) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible:urlRequestConvertible).responseArray { (response:DataResponse<[Event]>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(events):
                callback(events)
            }
        }
    }
    
    fileprivate func makeTurntArrayRequest(_ urlRequestConvertible: URLRequestConvertible, _ failure: @escaping (Any) -> Void, _ callback: @escaping ([Turn]) -> Void) -> Void {
        BaseController.instance.makeRequest(urlRequestConvertible:urlRequestConvertible).responseArray { (response:DataResponse<[Turn]>) in
            switch response.result {
            case let .failure(error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case let .success(turn):
                callback(turn)
            }
        }
    }
    
    public func getPingResponse(ipAddress:String,callback:@escaping (_ statuscode:Ping) -> Void, _ failure: @escaping (Any) -> Void) {
        //TODO: Supply access token
        let header = ["Content-type" : "application/json; charset=utf-8"]
        let url = "http://\(ipAddress)/fcgi-bin/v1/ping"
        Alamofire.request(url, headers: header).responseObject{ (response: DataResponse<Ping>) in
            print("DEVICE PAIRING RESPONSE \(response.response)")
            switch response.result {
            case .failure(let error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case .success(let pong):
                callback(pong)
            }
        }
    }
    
    public func getSSIDList(ipAddress:String,callback:@escaping (_ statuscode:[SSID]) -> Void, _ failure: @escaping (Any) -> Void) {
        //TODO: Supply access token
        let header = ["Content-type" : "application/json; charset=utf-8"]
        let url = "http://\(ipAddress)/fcgi-bin/v1/networks"
        Alamofire.request(url, headers: header).responseArray(keyPath: "networks"){ (response: DataResponse<[SSID]>) in
            print("DEVICE PAIRING RESPONSE \(response)")
            switch response.result {
            case .failure(let error):
                if let apiError = Mapper<APIError>().map(JSONString:String(data: response.data!, encoding: String.Encoding.utf8)! ){
                    failure(apiError)
                }else{
                    failure(error)
                }
            case .success(let ssids):
                callback(ssids)
                print(ssids.toJSONString())
            }
        }
    }

    
    func getDevice(id:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.getDevice(id: id), failure, callback)
    }
    
    func inviteUser(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.inviteUser(id: id, user: user), failure, callback)
    }
    
    func getEvents(id:String, callback:@escaping (_ newDevice:[Event])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeEventArrayRequest(Router.getEvents(id: id), failure, callback)
    }
    
    func getDevices(callback:@escaping (_ newDevice:[Device])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeArrayRequest(Router.getDevices(), failure, callback)
    }
    
    func getEventsByDate(_ id:String, from:Date, to:Date, offset:Int,callback:@escaping (_ newDevice:[Event])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeEventArrayRequest(Router.getEventsByDate(id: id, from: Utils.convertDateFormaterForRequest(from), to: Utils.convertDateFormaterForRequest(to), offset: offset), failure, callback)
    }
    
    func getDataUseByDevice(id:String, from:String, to:String, callback:@escaping (_ dataUsage:DataUsage)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeDataUsageObjectRequest(Router.getDataUseByDevice(id: id, from:from, to:to), failure, callback)
    }
    
    func removeUser(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.removeUser(id: id, user:user), failure, callback)
    }
    
    func removeAdmin(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.removeAdmin(id: id, user:user), failure, callback)
    }
    
    func inviteAdmin(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.inviteAdmin(id: id, user: user), failure, callback)
    }
    
    func removeInvite(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.removeInvite(id: id, user: user), failure, callback)
    }
    
    func deviceAvailable(callback:@escaping (_ newDevice:[DeviceAvailable])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeDeviceAvailableArrayRequest(Router.deviceAvailable(), failure, callback)
    }
    
    func deviceAvailable(_ id:String, callback:@escaping (_ newDevice:DeviceAvailable)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeDeviceAvailableObjectRequest(Router.getDeviceAvailableWithId(id: id), failure, callback)
    }
    
    func removeMe(id:String, user:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.removeMe(id: id, user: user), failure, callback)
    }
    
    func acceptInvite(id:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.acceptInvite(id: id), failure, callback)
    }
    
    func rejectInvite(id:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.rejectInvite(id: id), failure, callback)
    }
    
    func getEvent(device_id:String, event_id:String, callback:@escaping (_ newDevice:Event)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeEventObjectRequest(Router.getEvent(dId: device_id, eId: event_id), failure, callback)
    }
    
    func shareEvent(event_id:String, shared:Bool, callback:@escaping (_ newDevice:Event)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeEventObjectRequest(Router.shareEvent(event_id: event_id, shared: shared), failure, callback)
    }
    
    func changeEventStatus(event_id:String, status:String, callback:@escaping (_ newDevice:Event)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeEventObjectRequest(Router.changeEventStatus(event_id: event_id, status: status), failure, callback)
    }
    
    func setDeviceLatestImage(id:String, imageData: Data, timeinms:String, callback:@escaping (_ status:Bool)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.setDeviceLatestImage(id: id, timeinms: timeinms), failure, { (status) in
            if let url = status.message {
                self.uploadGoogleImage(url, imageData: imageData, callback: { (status) in
                    callback(status)
                })
            }else{
                let apiError = APIError()
                apiError.error_description = "failed to upload"
                apiError.code = 509
                apiError.error = "Failed"
                failure(failure)
                print(apiError)
            }
        })
    }
    
    func deleteEvent(device_id:String, event_id:String, callback:@escaping (_ newDevice:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.deleteEvent(dId: device_id, eId: event_id), failure, callback)
    }
    
    func getTurnServerValues(callback:@escaping (_ newDevice:[Turn])->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeTurntArrayRequest(Router.getTurnServerValues(), failure, callback)
    }
    
    //    case sendDeviceDiagnosis(id:String)
    
    func changeCloudUploadStatus(id:String, type:Bool, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.changeCloudUploadStatus(id: id, type: type), failure, callback)
    }
    
    func changeCloudUploadSettings(id:String, type:String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.changeCloudUploadSettings(id: id, type: type), failure, callback)
    }
    
    func changeChimeStatus(id:String, type:Bool, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.changeChimeStatus(id: id, type: type), failure, callback)
    }
    
    func motionSenstivity(id:String, motionSensitivity:Int, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.motionSensitivity(id: id, motionSensitivity: motionSensitivity), failure, callback)
    }
    
    func audioSettings(id:String, audio: Audio, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.audioSettings(id: id, audio: audio), failure, callback)
    }
    
    func videoSettings(id:String, recording: Recording, live: Live, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.videoSettings(id: id, recording: recording, live: live), failure, callback)
    }
    
    func motionSettings(id:String, motionSettings: Parameters, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.motionSettings(id: id, motionSettings: motionSettings), failure, callback)
    }
    
    func changeTimeZone(id:String, timeZone: String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.timeZone(id: id, timeZone: timeZone), failure, callback)
    }
    
    func changeDeviceName(id:String, name: String, callback:@escaping (_ newDevice:Device)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeObjectRequest(Router.changeDeviceName(id: id, name: name), failure, callback)
    }
    
    func changeBulkEventStatus(ids:[String], status: String, callback:@escaping (_ newDevice:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        makeStatusResponseObjectRequest(Router.changeBulkEventStatus(event_ids: ids, status: status), failure, callback)
    }

    func uploadGoogleImage(_ url:String, imageData:Data, callback:@escaping (_ status:Bool)->Void) {
        if let requestUrl = URL(string: url){
            let lobj_Request = NSMutableURLRequest(url: requestUrl)
            lobj_Request.httpMethod = "PUT"
            lobj_Request.httpBody = imageData
            lobj_Request.setValue("public-read", forHTTPHeaderField: "x-goog-acl")
            lobj_Request.timeoutInterval = 30
            lobj_Request.httpShouldHandleCookies=true
            lobj_Request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
            
            let task = URLSession.shared.dataTask(with: lobj_Request as URLRequest) {
                (data, response, error) -> Void in
                let httpResponse = response as? HTTPURLResponse
                var statuscode:Int?
                if let statusCode = httpResponse?.statusCode{
                    statuscode = statusCode
                    if statuscode == 200 {
                        callback(true)
                    }else{
                        callback(false)
                    }
                }else{
                    callback(false)
                }
            }
            task.resume()
        }
    }
    
}
