//
//  LiveViewController.swift
//  SureBell
//
//  Created by PiOctave on 20/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import UIKit
import WebRTC
import SwiftyJSON
import Photos
import GLKit
import ObjectMapper

open class LiveViewController: UIViewController, UIScrollViewDelegate {

    var loaderView: Loader!
    var remoteView: RTCEAGLVideoView!
    var endCallContainer: UIView?
    var speakerCallContainer: UIView?
    var micCallContainer: UIView?
    var screenCallContainer: UIView?
    
    var _callEventImageView: UIImageView?
    public var callImage : UIImageView{
        if let currentImage = _callEventImageView{
            return currentImage
        }
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true
        _callEventImageView = imageView
        return imageView
    }
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var _forthBtn: UIButton?
    
    public var sosButton : UIButton{
        if let currentView = _forthBtn{
            return currentView
        }
        let sos = UIButton()
        sos.frame = CGRect(x: 60, y: 200, width: 200, height: 150)
        sos.backgroundColor = UIColor.groupTableViewBackground
        sos.setTitle("SOS", for: .normal)
        sos.setTitleColor(UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0), for: .normal)
        sos.layer.cornerRadius = 19.0
        sos.transform = CGAffineTransform(translationX: 40.0, y: 0.0)
        sos.addTarget(self, action:#selector(self.forthBtnAction), for: .touchUpInside)
        _forthBtn = sos
        return sos
    }
    
    var _endCallIcon: UIButton?
    
    public var endCall : UIButton{
        if let currentImage = _endCallIcon{
            return currentImage
        }
        let end = UIButton()
        end.frame = CGRect(x: 60, y: 200, width: 200, height: 150)
        end.isUserInteractionEnabled = true
        end.contentMode = .scaleAspectFit
        end.backgroundColor = UIColor.red
        end.layer.cornerRadius = 19.0
        _endCallIcon = end
        end.addTarget(self, action:#selector(self.backAction), for: .touchUpInside)
        return end
    }
    
    var _speakerIcon: UIButton?
    
    public var audioOut : UIButton{
        if let currentImage = _speakerIcon{
            return currentImage
        }
        let imageView = UIButton()
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 19.0
        imageView.backgroundColor = UIColor.groupTableViewBackground
        imageView.addTarget(self, action:#selector(self.stopVideoAction), for: .touchUpInside)
        _speakerIcon = imageView
        return imageView
    }
    
    var _micIcon: UIButton?
    
    public var audioIn : UIButton{
        if let currentImage = _micIcon{
            return currentImage
        }
        let imageView = UIButton()
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 19.0
        imageView.backgroundColor = UIColor.groupTableViewBackground
        imageView.addTarget(self, action:#selector(self.muteAction), for: .touchUpInside)
        _micIcon = imageView
        return imageView
    }
    
    var _cameraIcon: UIButton?
    
    public var camera : UIButton{
        if let currentImage = _cameraIcon{
            return currentImage
        }
        let imageView = UIButton()
        imageView.isUserInteractionEnabled = true
        imageView.contentMode = .scaleAspectFit
        imageView.layer.cornerRadius = 19.0
        imageView.backgroundColor = UIColor.groupTableViewBackground
        imageView.addTarget(self, action:#selector(self.screenShotButtonAction), for: .touchUpInside)
        _cameraIcon = imageView
        return imageView
    }
    
    
    //MARK: *------Custom Variables------*
    
    var phoneRtc:PhoneRTCPlugin?
    var sessionConfig:SessionConfig?
    var timer = Timer()
    var calltimer = Timer()
    var tapGesture:UITapGestureRecognizer?
    var session_key = ""
    var signallingChannelDelegate:SignallingChannel?
    var device_id = ""
    var deviceName = ""
    var mac = ""
    var localIp:String?
    var localHost:String?
    var isLocalCall = false
    var callTryAftersessionExpired = 0
    var navigationFrom = "DEVICE_LIST"
    var navFrom = "Live"
    
    public var turnAddress = [Turn]()
    public var device:Device?
    
    var isChecked1: Bool = true {
        didSet{
            if isChecked1 == true {
                if let phoneRtc = self.phoneRtc{
                    phoneRtc.muteAudio(session_key: self.session_key, isAudio: true)
                    let vectors = VectorICONS()
                    vectors.speaker.frame.origin.x = self.audioOut.bounds.origin.x + 9
                    vectors.speaker.frame.origin.y = self.audioOut.bounds.origin.y + 12
                    self.audioOut.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    self.audioOut.layer.addSublayer(vectors.speaker)
                }
            } else {
                if let phoneRtc = self.phoneRtc{
                    phoneRtc.muteAudio(session_key: self.session_key, isAudio: false)
                    let vectors = VectorICONS()
                    vectors.noSpeaker.frame.origin.x = self.audioOut.bounds.origin.x + 9
                    vectors.noSpeaker.frame.origin.y = self.audioOut.bounds.origin.y + 12
                    self.audioOut.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    self.audioOut.layer.addSublayer(vectors.noSpeaker)
                }
            }
        }
    }
    
    var ischeckedbuttons: Bool = true {
        didSet{
            if ischeckedbuttons == true {
                hide()
            } else {
                unHide()
            }
        }
    }
    
    var isChecked2: Bool = true {
        didSet{
            if isChecked2 == true {
                if let phoneRtc = self.phoneRtc, let sessionConfig = self.sessionConfig{
                    sessionConfig.streams?.audio = true
                    phoneRtc.renegotiate(session_key: self.session_key, config: sessionConfig)
                    let vectors = VectorICONS()
                    vectors.mic.frame.origin.x = self.audioIn.bounds.origin.x + 14
                    vectors.mic.frame.origin.y = self.audioIn.bounds.origin.y + 11
                    self.audioIn.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    self.audioIn.layer.addSublayer(vectors.mic)
                }
            } else {
                if let phoneRtc = self.phoneRtc, let sessionConfig = self.sessionConfig{
                    sessionConfig.streams?.audio = false//TODO: changed by niteesh
                    phoneRtc.renegotiate(session_key: self.session_key, config: sessionConfig)
                    let vectors = VectorICONS()
                    vectors.muted.frame.origin.x = self.audioIn.bounds.origin.x + 14
                    vectors.muted.frame.origin.y = self.audioIn.bounds.origin.y + 11
                    self.audioIn.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                    self.audioIn.layer.addSublayer(vectors.muted)
                }
            }
        }
    }
    
    convenience public init(_ turn:[Turn], device:Device, sessionKey:String, callFrom:String){
        self.init()
        self.sessionConfig = SessionConfig()
        self.phoneRtc = PhoneRTCPlugin()
        self.turnAddress = turn
        self.navFrom = callFrom
        self.session_key = sessionKey
        self.device = device
        guard let deviceid = device._id else { return}
        guard let devicename = device.name else { return}
        self.deviceName = devicename
        self.device_id = deviceid
    }
    
    //MARK: *------IB Actions------*
    @objc func backAction() {
        hangupCall()
    }
    
    
   
    
    @objc func forthBtnAction() {
        resetTimer()
        if let phonertc = self.phoneRtc{
            phonertc.sosCall(self.session_key)
        }
        User.instance.getMe(callback: { (user) in
            guard let status = user.sosRegistered else { return }
            guard let mobile = user.phone else { return }
            guard let dialCode = user.countryDialCode else { return }
            guard let lat = self.device?.location?[0] else { return}
            guard let lng = self.device?.location?[1] else { return}
            if status {
                var phone = ""
                if mobile.hasPrefix(dialCode){
                    phone =  mobile.replacingOccurrences(of:  dialCode, with: "")
                }else{
                    phone = mobile
                }
                let params = ["Code":"policesos","Message":"I am in an Emergency","Latitude":lat,"Longitude":lng,"IMEI":UIDevice.current.identifierForVendor!.uuidString,"SIM_NO":phone,"deviceId":self.device_id,"datetime":Date(),"deviceType":"2"] as [String : Any]
//                Device.instance.trigersos(params: params, callback: { (status) in
//                    DispatchQueue.main.async {
//                        let alertController = UIAlertController(
//                            title: "Alert", message: "SOS Service triggered successfully.", preferredStyle: .alert)
//                        let defaultAction = UIAlertAction(
//                            title: "STOP", style: .default, handler: { (action: UIAlertAction!) in
//                                let abandonedParams = ["message":"Request Abandoned","imei":UIDevice.current.identifierForVendor!.uuidString,"sim_no":phone]
//                                Device.instance.abandonsos(params: abandonedParams, callback: { (status) in
//                                    DispatchQueue.main.async {
//                                        alertController.dismiss(animated: true)
//                                    }
//                                }, failure: { (error) in
//                                    DispatchQueue.main.async {
//                                        alertController.dismiss(animated: true)
//                                    }
//                                })
//                        })
//                        alertController.addAction(defaultAction)
//                        self.present(alertController, animated: true, completion: nil)
//                    }
//                }, failure: { (error) in
//
//                })
            }else{
                let alertController = UIAlertController(
                    title: "Alert", message: "You are not registered for SOS service.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(
                    title: "OK", style: .default, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }) { (error) in
            if let error = error as? APIError{
                print(error.error_description)
            }else{
                if let error = error as? Error{
                    print(error.localizedDescription)
                }
            }
        }
        
    }
    
    @objc func stopVideoAction() {
        resetTimer()
        if isChecked1 == true {
            isChecked1 = false
        } else {
            isChecked1 = true
        }
    }
    
    @objc func muteAction() {
        resetTimer()
        if isChecked2 == true {
            isChecked2 = false
        } else {
            isChecked2 = true
        }
    }
    
    @objc func screenShotButtonAction() {
        if self.navFrom == "Motion"{
            if let image = self.captureFrameFromRTCView(self.remoteView), let id = self.device?._id{
                self.loaderView.isHidden = false
                self.loaderView.startAnimating()
                Device.instance.setDeviceLatestImage(id: id, imageData: UIImagePNGRepresentation(image)!, timeinms: "\(self.getCurrentMillis())", callback: { (status) in
                    DispatchQueue.main.async {
                        self.loaderView.stopAnimating()
                    }
                    if status{
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(
                                title: "Alert", message: "Device background image uploaded successfully.", preferredStyle: .alert)
                            let defaultAction = UIAlertAction(
                                title: "OK", style: .default, handler: { action in
                                    self.hangupCall()
                                    
                                    DispatchQueue.main.async {
                                        self.dismiss(animated: false, completion: nil)
                                    }
                            })
                            alertController.addAction(defaultAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }else{
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(
                                title: "Alert", message: "Something went wrong.", preferredStyle: .alert)
                            let defaultAction = UIAlertAction(
                                title: "OK", style: .default, handler: nil)
                            alertController.addAction(defaultAction)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                }) { (error) in
                    DispatchQueue.main.async {
                        self.loaderView.stopAnimating()
                    }
                    if let error =  error as? APIError{
                        guard  let des = error.error_description else{return}
                        print(des)
                        self.showAlert(title: "Alert", message: des)
                    }else{
                        guard let error = error as? Error else{return}
                        print(error)
                        self.showAlert(title: "Alert", message: error.localizedDescription)
                    }
                }
            }
        }else{
            if let image = self.captureFrameFromRTCView(self.remoteView){
                if checkPhotoLibraryPermission(){
                    if self.navigationFrom == "ROI_MOTION"{
                        savePictureForRoiBackground(image)
                    }else{
                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                    }
                   self.showAlert(title: "Alert", message: "Snapshot captured successfully.")
                }else{
                    self.showAlert(title: "Alert", message: "Please allow Photo permission from Settings.")
                    // NotificationAlert.showStripBanner(SureConstantResources.PHOTO_ACCESS_PERMISSION)
                }
            }
        }
    }
    
    
    //MARK: *------UIView Controller delegate methods------*
    
    override open func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
        if self.view.window != nil {
            do {
                if UIDevice.current.userInterfaceIdiom != .pad{
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeDefault, options: [.duckOthers, .defaultToSpeaker])
                }
            } catch let error{
                print("SetCategory \(error)")
            }
        }else{
            do {
                if UIDevice.current.userInterfaceIdiom != .pad{
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeDefault, options: [.duckOthers])
                }
            } catch let error{
                print("SetCategory \(error)")
            }
        }
        // NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        //   GenralFunctions.lockOrientation([.portrait], andRotateTo: .landscapeRight)
       // GenralFunctions.rotateScreen(.landscapeRight)
        //   GenralFunctions.lockOrientation([.landscapeRight], andRotateTo: .landscapeRight)
    }
    
    
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if self.navigationFrom != "ROI_MOTION"{
            self.navigationFrom = "DEVICE_LIST"
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        }
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        //SharedData.DeviceDataInNavigation.VIDEO_CALL_IMAGE = UIImage()
    }
    
    
    override open func viewDidLoad() {
        super.viewDidLoad()
    
        self.view.backgroundColor = UIColor.lightGray
        self.view.addSubview(scrollView)
        // constrain the scroll view to 8-pts on each side
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0.0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0.0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0.0).isActive = true
        
//
//        self.view.addSubview(sosButton)
//        sosButton.translatesAutoresizingMaskIntoConstraints = false
//        // constrain the scroll view to 8-pts on each side
//        sosButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0).isActive = true
//        sosButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
//        sosButton.heightAnchor.constraint(equalToConstant: 45).isActive = true
//        sosButton.widthAnchor.constraint(equalToConstant: 45).isActive = true
        
        self.remoteView = RTCEAGLVideoView()
        self.remoteView.frame = self.view.bounds
        self.scrollView.addSubview(self.remoteView)
        
        remoteView.translatesAutoresizingMaskIntoConstraints = false
        
        remoteView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        remoteView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        remoteView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        remoteView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true

        let vectors = VectorICONS()
        vectors.endCall.frame.origin.x = self.endCall.bounds.origin.x + 10
        vectors.endCall.frame.origin.y = self.endCall.bounds.origin.y + 17
        self.endCall.layer.addSublayer(vectors.endCall)
        
        vectors.camera.frame.origin.x = self.camera.bounds.origin.x + 11.5
        vectors.camera.frame.origin.y = self.camera.bounds.origin.y + 13
        self.camera.layer.addSublayer(vectors.camera)
        
        vectors.mic.frame.origin.x = self.audioIn.bounds.origin.x + 14
        vectors.mic.frame.origin.y = self.audioIn.bounds.origin.y + 11
        self.audioIn.layer.addSublayer(vectors.mic)
        
        vectors.speaker.frame.origin.x = self.audioOut.bounds.origin.x + 9
        vectors.speaker.frame.origin.y = self.audioOut.bounds.origin.y + 12
        self.audioOut.layer.addSublayer(vectors.speaker)
     
        let stackView   = UIStackView()
        stackView.axis  = UILayoutConstraintAxis.vertical
        stackView.distribution  = UIStackViewDistribution.fillEqually
        stackView.alignment = UIStackViewAlignment.fill
        stackView.spacing   = 14.0
    

        stackView.addArrangedSubview(camera)
        stackView.addArrangedSubview(audioIn)
        stackView.addArrangedSubview(audioOut)
        stackView.addArrangedSubview(endCall)
        
        self.view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
 
        stackView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        stackView.widthAnchor.constraint(equalToConstant: 45.0).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 220.0).isActive = true
        stackView.transform = CGAffineTransform(translationX: -20.0, y: -30.0)
        
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
      
        UIApplication.shared.isIdleTimerDisabled = true
//        if !UserDefaults.standard.bool(forKey: SureConstantResources.VIDEO_CALL_SCRRENSHOT_TOOLTIP_KEY) {
//            UserDefaults.standard.set(true, forKey: SureConstantResources.VIDEO_CALL_SCRRENSHOT_TOOLTIP_KEY)
//            EasyTipView.show(forView: self.cameraIcon,
//                             withinSuperview: self.view,
//                             text: SureConstantResources.VIDEO_CALL_SCRRENSHOT_TOOLTIP,
//                             delegate : self)
//        }
        
      
        
        if #available(iOS 11.0, *) {
            remoteView.accessibilityIgnoresInvertColors = true
            callImage.accessibilityIgnoresInvertColors = true
        }
        
        Utils.isCallPadbusy = true
     
        self.scrollView.isScrollEnabled = false
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callNotConnectedNotification), name: NSNotification.Name(rawValue: "myNotification"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.disconnectVideoCallFromCallKit), name: NSNotification.Name(rawValue: "DISCONNECT_VIDEO_CALL"), object: nil)
        
        // This Notification Posted from PhoneRTCPlugin.swift
        NotificationCenter.default.addObserver(self, selector: #selector(self.removeImageAfterCallConnected), name: NSNotification.Name(rawValue: "REMOVE_CALL_IMAGE_AFTER_CALL_CONNECTED"), object: nil)
        // This Notification Posted from SilentNotification class
        
        //  NotificationCenter.default.addObserver(self, selector: #selector(self.callEventImageDownloadedSuccess), name: NSNotification.Name(rawValue: "CALL_EVENT_IMAGE_DOWNLOADED"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeCallTypeVideoToAudio), name: NSNotification.Name(rawValue: "CHANGE_CALL_TYPE_VIDEO_TO_AUDIO"), object: nil)
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(LiveViewController.tapViewAction))
        // Do any additional setup after loading the view.
        self.view.addGestureRecognizer(tapGesture!)
        
        startTimer()
        if self.navigationFrom == "ROI_MOTION"{
            audioIn.isHidden = true
            audioOut.isHidden = true
            sosButton.isHidden = true
        }else{
            audioIn.isHidden = false
            audioOut.isHidden = false
            sosButton.isHidden = false
        }
       // print(SharedData.UserDetails.USER_ACCESS_TOKEN)
        self.getROIImage(self.device_id)
        let doubleTap = UITapGestureRecognizer()
        doubleTap.numberOfTapsRequired = 2
        doubleTap.addTarget(self, action: #selector(self.doubleTapGestureHandler))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(doubleTap)
        
       // NotificationCenter.default.addObserver(self, selector: #selector(self.appDidBecomeActive), name:NSNotification.Name(rawValue: SureConstantResources.NotificationType.APP_IS_ACTIVE), object: nil)
        
        if var config = self.sessionConfig, let phonertc = self.phoneRtc{
          //  print("CALL_PROCESS_INITIATED \(GenralFunctions.getCurrentMillis())")
//            if !AppFlags.isCallPadbusy{
//                AppFlags.isCallPadbusy = true
//                //   let deviceNameForCallKit = SharedData.DeviceDataInNavigation.DEVICE_NAME //TODO: Bring device name from devicelist
//                //TODO : show name
//                // AppDelegate.callManager.startCall(handle: self.deviceName, video: true)
//            }
            // Add event image before call initiated
            // self.callEventImageView.image = SharedData.DeviceDataInNavigation.VIDEO_CALL_IMAGE
            // self.callEventImageView.addBlurEffect()
           // AppFlags.isTypeVideo = false
            
            self.remoteView.contentMode = UIViewContentMode.scaleAspectFit
             config.isInitiator = true
            var isSDESSupported = false
            var isDeferedVideoSupported = false
            
//            let deviceController = DeviceListCacheController()
//            if deviceController.isSDESSupported(forDevice: self.device_id){
                isSDESSupported = true
           // }
//            if deviceController.isDeferedVideoSupported(forDevice: self.device_id){
                isDeferedVideoSupported = true
           // }
            
            if isDeferedVideoSupported{
                if self.navFrom == "CallKit"{
                    config.streams = StreamsConfig(audio: true, video: false, media: false)
                }else{
                    config.streams = StreamsConfig(audio: true, video: false, media: true)
                }
            }else{
                config.streams = StreamsConfig(audio: true, video: false, media: true)
            }
            
            guard let access = Token.instance.getAccessToken()else {return}
            
            phonertc.pluginInitialize(access,remoteView: self.remoteView,localView: nil)
            phonertc.createSessionObject(session_key, config: config,isLocal: isLocalCall, isSDESSupported:isSDESSupported, callInItType: self.navFrom)
            Utils.isCallPadbusy = true
        }
        
        self.checkAndPlaceLocalCall()
        
        self.loaderView = Loader()
        self.view.addSubview(self.loaderView)
        
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        
        loaderView.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        loaderView.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        loaderView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        loaderView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.loaderView.tintColor = UIColor.white
        self.loaderView.startAnimating()
        
    }
    
    
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.landscapeRight
    }

   
    func appDidBecomeActive(){
        if self.view.window != nil {
            do {
                if UIDevice.current.userInterfaceIdiom != .pad{
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeDefault, options: [.duckOthers, .defaultToSpeaker])
                }
            } catch let error{
                print("SetCategory \(error)")
            }
        }else{
            do {
                if UIDevice.current.userInterfaceIdiom != .pad{
                    try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayAndRecord, mode: AVAudioSessionModeDefault, options: [.duckOthers])
                }
            } catch let error{
                print("SetCategory \(error)")
            }
        }
        
       // GenralFunctions.lockOrientation([.landscapeLeft,.landscapeRight], andRotateTo: .landscapeRight)
    }
    
    func resetTimer(){
        timer.invalidate()
        startTimer()
    }
    
    func startTimer(){
        timer = Timer.scheduledTimer(timeInterval: 15, target: self, selector: #selector(self.hide), userInfo: nil, repeats: false)
    }
    
    
    
    func checkAndPlaceLocalCall(){
        if let ip = self.localIp, let host = self.localHost{
            self.placeLoacalCall(localPort: ip, localHost: host)
        }else{
            if self.turnAddress == nil{
                getTurnAddress()// TODO:
            }else{
                placeCallFromServer()
            }
        }
    }
    
    func getTurnAddress(){
        Device.instance.getTurnServerValues(callback: { (turns) in
            self.turnAddress = turns
            self.placeCallFromServer()
        }) { (error) in
            
        }
    }
    
    func compareMac(mac1:String,mac2:String)->Bool{
        var macHexArray = mac1.components(separatedBy: ":")
        var macHex = macHexArray[5].hexToInt
        macHex = macHex - 1
        let macHexInStringAfterChange:String = macHex.hexString
        let finalMac = macHexArray[0] + macHexArray[1] + macHexArray[2] + macHexArray[3] + macHexArray[4] + macHexInStringAfterChange.uppercased()
        let mac = finalMac
        
        if (mac2.caseInsensitiveCompare(mac) == ComparisonResult.orderedSame){
            return true
        }else{
            return false
        }
    }
    
    
    func placeLoacalCall(localPort:String,localHost:String){
        DispatchQueue.main.async {
           // Toast(text:"Connecting local call").show()
        }
         guard let access = Token.instance.getAccessToken()else {return}
        isLocalCall = true
        signallingChannelDelegate = SignallingChannelDelegateFactory.instance.getSignallingChannelDelegate("local", access_token: access, medium_id: getDeviceId(), uri: "udp://" + localHost + ":" + localPort)
        if let phonertc = self.phoneRtc{
            phonertc.setSignallingDelegate(self.session_key, signallingChannelDelegate: signallingChannelDelegate!)
        }
        initCallSetup()
    }
    
    func placeCallFromServer(){
        DispatchQueue.main.async {
          //  Toast(text:"Connecting server call").show()
        }
        isLocalCall = false
        guard let access = Token.instance.getAccessToken()else {return}
        if var sessionConfig = self.sessionConfig{
            for item in self.turnAddress{
                if let url = item.url, let user = item.username, let credentials = item.credential{
                sessionConfig.turn.append(TurnConfig(
                    url:url,
                    username : user,
                    credential : credentials
                ))
                }
            }
        }else{
            if var sessionConfig = self.sessionConfig{
                sessionConfig.turn.append(TurnConfig(
                    url:"stun:stun.l.google.com:19302",
                    username : "",
                    credential : ""
                ))
            }
        }
        self.signallingChannelDelegate = SignallingChannelDelegateFactory.instance.getSignallingChannelDelegate("webSocket", access_token: access, medium_id: self.getDeviceId(), uri: "wss://pirtc.pioctave.com/rtc")
        if let phonertc = self.phoneRtc{
            phonertc.setSignallingDelegate(self.session_key, signallingChannelDelegate: signallingChannelDelegate!)
        }
        self.initCallSetup()
    }
 
    
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.remoteView
    }
    
//    func easyTipViewDidDismiss(_ tipView: EasyTipView) {
//        if tipView.text == SureConstantResources.VIDEO_CALL_SCRRENSHOT_TOOLTIP{
//            if !UserDefaults.standard.bool(forKey: SureConstantResources.VIDEO_CALL_PINCH_TO_ZOOM_TOOLTIP_KEY) {
//                UserDefaults.standard.set(true, forKey: SureConstantResources.VIDEO_CALL_PINCH_TO_ZOOM_TOOLTIP_KEY)
//                EasyTipView.show(forView: self.remoteView,
//                                 withinSuperview: self.view,
//                                 text: SureConstantResources.VIDEO_CALL_PINCH_TO_ZOOM_TOOLTIP,
//                                 delegate : self)
//            }
//        }
//    }
    
    //MARK: *------Custom Methods------*
    
    /// Double Tap gesture for zoom on video call screen
    /// zoom in/ zoom out
    /// - Parameter recognizer: taprecognizer object
    @objc func doubleTapGestureHandler(recognizer: UITapGestureRecognizer){
        if scrollView.zoomScale == 1 {
            scrollView.zoom(to: zoomRectForScale(scale: scrollView.maximumZoomScale, center: recognizer.location(in: recognizer.view)), animated: true)
        } else {
            scrollView.setZoomScale(1, animated: true)
        }
    }
    
    /// Scale to zoom on double tap gesture
    /// zoom on tapped point center position
    /// - Parameters:
    ///   - scale: scale value
    ///   - center: position to zoom
    /// - Returns: cgrect
    func zoomRectForScale(scale: CGFloat, center: CGPoint) -> CGRect {
        var zoomRect = CGRect.zero
        zoomRect.size.height = remoteView.frame.size.height / scale
        zoomRect.size.width  = remoteView.frame.size.width  / scale
        let newCenter = remoteView.convert(center, from: scrollView)
        zoomRect.origin.x = newCenter.x - (zoomRect.size.width / 2.0)
        zoomRect.origin.y = newCenter.y - (zoomRect.size.height / 2.0)
        return zoomRect
    }
    
    /// After taking snapshot of Live view save it to File Manager
    /// DeviceID_ROI_IMAGE.png is file name
    /// - Parameter image: Image captured from screenshot
    func savePictureForRoiBackground(_ image:UIImage){
       // uploadLastImage(image)
        do {
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let fileURL = documentsURL.appendingPathComponent("\(self.device_id)_ROI_IMAGE.png")
            if let pngImageData = UIImagePNGRepresentation(image) {
                try pngImageData.write(to: fileURL, options: [])
            }
        }catch {}
    }
    
    /// Upload Last image Captured By snapshot to server, Note: This feature is only
    /// avilable when Navigating from motion Setting View
    /// - Parameter image: Image captured from screenshot
//    func uploadLastImage(_ image:UIImage){
//        connection.requestPOSTURL("/device/\(self.device_id)/latest-image", shouldProgress: false, params: ["time":"\(getCurrentMillis())"], headers: ["" : ""], success: { (JSON, statusCode) in
//            let  imageData = UIImageJPEGRepresentation(image, 1.0)
//            self.connection.uploadImage("Motion",url: JSON["message"].stringValue, imageData: imageData!)
//            if self.navigationFrom == "ROI_MOTION"{
//                self.navigationFrom = "DEVICE_LIST"
//                //  SharedData.DeviceDataInNavigation.VIDEO_CALL_IMAGE = UIImage()
//                if self.session_key != ""{
//                    // self.showLoader(message: "Connecting...", delayTime: 5.0)
//                    self.hangupCall()
//                }
//            }
//        }) { (NSError) in
//        }
//    }
    
    /// Returns milli second time for uploading image to server
    /// as time params
    /// - Returns: current time in milli second
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    /// Check photos Library access permission before taking screenshots
    ///
    /// - Returns: true/false
    func checkPhotoLibraryPermission()->Bool {
        var returnStatus = false
        let status = PHPhotoLibrary.authorizationStatus()
        switch status {
        case .authorized:
            returnStatus = true
        case .denied, .restricted :
            returnStatus = false
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization() { (status) -> Void in
                switch status {
                case .authorized:
                    returnStatus = true
                case .denied, .restricted:
                    returnStatus = false
                case .notDetermined:
                    returnStatus = false
                }
            }
        }
        return returnStatus
    }
    
    /// Capture screenshots from Webrtc RTCEAGLVideoView
    ///
    /// - Parameter view: Remoteview as RTCEAGLVideoView
    /// - Returns: captured image
    func captureFrameFromRTCView(_ view:RTCEAGLVideoView) -> UIImage? {
        var localview: UIView?
        for v: UIView in view.subviews {
            if (v is GLKView) {
                localview = v
            }
        }
        if localview == nil {
            return nil
        }else {
            return (localview as! GLKView).snapshot
        }
    }
    
    /// After taking screenshot We are showing alert banner in which a small
    /// image is used to resize original image to thubnail this method is used.
    /// - Parameters:
    ///   - image: image to resize
    ///   - size: size for resizing
    /// - Returns: resized image
    func resizeImage(_ image: UIImage, size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    /// Pich to zoom feature on remoteview
    func pinchZoomViewSetup(){
        self.scrollView.delegate = self
        self.scrollView.maximumZoomScale = 3.0
        self.scrollView.bouncesZoom = false
        self.automaticallyAdjustsScrollViewInsets = false
        self.scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
        self.scrollView.flashScrollIndicators()
        self.remoteView.clipsToBounds = false
    }
    
    /// Get Last image/ROI background image. To show
    /// Image before call initialization
    /// - Parameter deviceID: deviceId to fetch ROI image from file manager
    func getROIImage(_ deviceID:String){
        let documentsURLEvent = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let filePathURLEvent = documentsURLEvent.appendingPathComponent("\(deviceID)_EVENT_RING_IMAGE.png").path
        if FileManager.default.fileExists(atPath: filePathURLEvent){
            let image = UIImage(contentsOfFile: filePathURLEvent)
            self.callImage.isHidden = false
            self.callImage.image = image
        }else{
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let filePath = documentsURL.appendingPathComponent("\(deviceID)_ROI_IMAGE.png").path
            if FileManager.default.fileExists(atPath: filePath){
                let image = UIImage(contentsOfFile: filePath)
                self.callImage.isHidden = false
                self.callImage.contentMode = .scaleAspectFill
                self.callImage.image = image
            }
        }
    }
    
    /// Method if notification observer post fired as roi image downloaded successfully
    @objc func changeCallTypeVideoToAudio(_ notification: Notification){
        DispatchQueue.main.async {
            if let value = notification.object as? Int{
                if value == 0{
                    self.callImage.contentMode = .center
                    if self.callImage.image == nil{
                        self.callImage.image = UIImage(named:"voice_call_only_white_text")
                    }
                    print("Audio Call connected")
                }else{
                    self.callImage.contentMode = .scaleAspectFill
                    self.callImage.image = UIImage(named:"")
                    print("Video Call connected")
                }
            }
        }
    }
    
    
    /// Notification observer method if call connected posted from Session.swift
    /// remove image from videocall view
    /// - Parameter notification: notification object
    @objc func removeImageAfterCallConnected(_ notification: Notification) {
        DispatchQueue.main.async {
            self.loaderView.isHidden = true
            self.scrollView.isScrollEnabled = true
            self.pinchZoomViewSetup()
            self.callImage.contentMode = .center
            self.callImage.image = UIImage()
        }
    }
    
    
    
    /// Notification observer if call not get connected because
    /// of few reason
    /// - Parameter notification: notification object
    @objc func callNotConnectedNotification(_ notification: Notification) {
        let state = UIApplication.shared.applicationState
        if state == .background {
            self.hangupCall()
        }else{
            if (notification.name.rawValue == "myNotification") {
                if  let data = notification.object as? [String: String]{
                    if data["signal"] == "EXIT"{
                        var reason = ""
                        if let msg = data["reason"]{
                            reason = msg
                        }else{
                            reason = "Unable to contact device, please try again."
                        }
                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Alert", message: reason, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
                                self.hangupCall()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }else if data["signal"] == "BUSY"{
                        var reason = ""
                        if let msg = data["reason"] {
                            reason = msg
                        } else {
                            reason = "Device is busy, please try again later."
                        }

                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Alert", message: reason, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
                                self.hangupCall()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }else if data["signal"] == "HANGUP"{
                        var reason = ""
                        if let msg = data["reason"]{
                            reason = msg
                        } else {
                            reason = "Call disconnected, please try again."
                        }

                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Alert", message: reason, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
                                self.hangupCall()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }else if data["signal"] == "OFFLINE"{
                        var reason = ""
                        if let msg = data["reason"] {
                            reason = msg
                        } else {
                            reason = "Device is not reachable, please try again."
                        }

                        DispatchQueue.main.async {
                            let alert = UIAlertController(title: "Alert", message: reason, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {(action: UIAlertAction!) in
                                self.hangupCall()
                            }))
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    
    
    /// Initiate video call process. Fetch device list, Initiate callkit feature
    /// Video or local call, Phonertc init
    func initCallSetup(){
        if let phonertc = self.phoneRtc{
            phonertc.crateSessionAddressList(session_key: session_key, turnAddressList: self.turnAddress.toJSON())
            phonertc.call(session_key: session_key, to: self.device_id, type: "device")
        }
    }
    
    
    /// Get unique id UUID of user device
    ///
    /// - Returns: uuid
    func getDeviceId()-> String{
        let appId = UserDefaults.standard
        let key = "MEDIUMID"
        if let deviceID = appId.object(forKey: key){
            return deviceID as! String
        }else{
            if let vendorKey:String = UIDevice().identifierForVendor?.uuidString{
                appId.set(vendorKey, forKey: key)
                appId.synchronize()
                return vendorKey
            }else{
                let uuid = UUID().uuidString
                appId.set(uuid, forKey: key)
                appId.synchronize()
                return uuid
            }
        }
    }
    
    //
    //    fileprivate func call() -> SureCall? {
    //            if let callMngr = callManager{
    //                if (callMngr.calls.count) > 0{
    //                    return callManager?.calls[0]
    //                }else{
    //                    return nil
    //                }
    //            }else{
    //                return nil
    //            }
    //    }
    
    /// Notification observer if device hangup
    /// from Session.swift
    /// - Parameter notification: notification object
    @objc func disconnectVideoCallFromCallKit(_ notification: Notification) {
        hangupCall()
    }
    
    func showAlert(title:String, message:String){
        DispatchQueue.main.async {
            let alertController = UIAlertController(
                title: title, message: message, preferredStyle: .alert)
            let defaultAction = UIAlertAction(
                title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func performEndCallAction(uuid: UUID) {
//        let endCallAction = CXEndCallAction(call: uuid)
//        let transaction = CXTransaction(action: endCallAction)
//        self.appDelegate.providerDelegate?.callKitCallController.request(transaction) { error in
//            if let error = error {
//                NSLog("EndCallAction transaction request failed: \(error.localizedDescription).")
//                return
//            }
//
//            NSLog("EndCallAction transaction request successful")
//        }
    }
    
    
    
    /// Disconnect call and go back to home screen
    func hangupCall(){
        
        if let phonertc = self.phoneRtc{
            phonertc.disconnect(session_key: self.session_key)
        }
        
        Utils.isCallPadbusy = false
        
        do {
            if let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first{
                try FileManager.default.removeItem(atPath: "\(documentsURL.path)/\("\(self.device_id)_EVENT_RING_IMAGE.png")")
            }
        }catch {}
        
        
        self.hide()
        
        self.dismiss(animated: false, completion: nil)

//        if let uuid = self.appDelegate.providerDelegate?.uuid{
//            self.performEndCallAction(uuid: uuid)
//        }

        Utils.isCallPadbusy = false
        UIApplication.shared.isIdleTimerDisabled = false
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func tapViewAction(){
        resetTimer()
        unHide()
    }
    
    @objc func hide(){
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.audioIn.transform = CGAffineTransform(translationX: self.view.frame.width,y: 0)
            self.audioOut.transform = CGAffineTransform(translationX: self.view.frame.width,y: 0)
            self.endCall.transform = CGAffineTransform(translationX: self.view.frame.width,y: 0)
        })
    }
    
    func unHide(){
        UIView.animate(withDuration: 0.4, animations: { () -> Void in
            self.audioIn.transform = CGAffineTransform(translationX: 0,y: 0)
            self.audioOut.transform = CGAffineTransform(translationX: 0,y: 0)
            self.endCall.transform = CGAffineTransform(translationX: 0,y: 0)
        })
    }
    
    
}


extension String {
    var firstUppercased: String {
        guard let first = first else { return "" }
        return String(first).uppercased() + dropFirst()
    }
    
    var URLEncodedString: String {
        let customAllowedSet = CharacterSet(charactersIn:":/?#[]@!$&'()*+,;=").inverted
        return addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
    }
    
    func boolValueFromString() -> Bool {
        return NSString(string: self).boolValue
    }
    
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+"]
        return Set(self).isSubset(of: nums)
    }
    
    
    var  isBlank:Bool {
        return self.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
    var hexToInt      : Int64    { return Int64(strtoul(self, nil, 16))    }
    var hexToDouble   : Double { return Double(strtoul(self, nil, 16)) }
    var hexToBinary   : String { return String(hexToInt, radix: 2)    }
    var intToHex      : String { return String(Int(), radix: 16)    }
    var intToBinary    : String { return String(Int(), radix: 2)     }
    var binaryToInt    : Int64    { return Int64(strtoul(self, nil, 2))     }
    var binaryToDouble : Double { return Double(strtoul(self, nil, 2))  }
    var binaryToHex   : String { return String(binaryToInt, radix: 16) }
}

extension Int64 {
    var binaryString: String { return String(self, radix: 2)  }
    var hexString  : String { return String(self, radix: 16) }
    var doubleValue : Double { return Double(self) }
}


class VectorICONS{
var camera: CAShapeLayer = {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 31.83, y: 22.86))
    path.addCurve(to: CGPoint(x: 25, y: 29.705), controlPoint1: CGPoint(x: 31.83, y: 26.64), controlPoint2: CGPoint(x: 28.772, y: 29.705))
    path.addCurve(to: CGPoint(x: 18.17, y: 22.86), controlPoint1: CGPoint(x: 21.228, y: 29.705), controlPoint2: CGPoint(x: 18.17, y: 26.639999999999997))
    path.addCurve(to: CGPoint(x: 25, y: 16.014), controlPoint1: CGPoint(x: 18.17, y: 19.080000000000002), controlPoint2: CGPoint(x: 21.228, y: 16.014))
    path.addCurve(to: CGPoint(x: 31.83, y: 22.86), controlPoint1: CGPoint(x: 28.772, y: 16.014), controlPoint2: CGPoint(x: 31.83, y: 19.079))
    path.move(to: CGPoint(x: 14.654999999999998, y: 22.86))
    path.addCurve(to: CGPoint(x: 25, y: 12.491999999999999), controlPoint1: CGPoint(x: 14.654999999999998, y: 17.134), controlPoint2: CGPoint(x: 19.287, y: 12.491999999999999))
    path.addCurve(to: CGPoint(x: 35.345, y: 22.86), controlPoint1: CGPoint(x: 30.713, y: 12.491999999999999), controlPoint2: CGPoint(x: 35.345, y: 17.134))
    path.addCurve(to: CGPoint(x: 25, y: 33.227), controlPoint1: CGPoint(x: 35.345, y: 28.586), controlPoint2: CGPoint(x: 30.713, y: 33.227))
    path.addCurve(to: CGPoint(x: 14.655, y: 22.859999999999996), controlPoint1: CGPoint(x: 19.287, y: 33.227), controlPoint2: CGPoint(x: 14.655, y: 28.586))
    path.close()
    path.move(to: CGPoint(x: 46.552, y: 15.982999999999997))
    path.addLine(to: CGPoint(x: 50, y: 15.982999999999997))
    path.addLine(to: CGPoint(x: 50, y: 34.24))
    path.addCurve(to: CGPoint(x: 42.258, y: 42), controlPoint1: CGPoint(x: 50, y: 38.526), controlPoint2: CGPoint(x: 46.534, y: 42))
    path.addLine(to: CGPoint(x: 9.281, y: 42))
    path.addLine(to: CGPoint(x: 9.281, y: 38.544))
    path.addLine(to: CGPoint(x: 41.64, y: 38.544))
    path.addCurve(to: CGPoint(x: 46.552, y: 33.620999999999995), controlPoint1: CGPoint(x: 44.353, y: 38.544), controlPoint2: CGPoint(x: 46.552, y: 36.339999999999996))
    path.addLine(to: CGPoint(x: 46.552, y: 15.983))
    path.close()
    path.move(to: CGPoint(x: 16.284, y: 7.95))
    path.addCurve(to: CGPoint(x: 13.732, y: 9.073), controlPoint1: CGPoint(x: 15.626, y: 8.663), controlPoint2: CGPoint(x: 14.700999999999999, y: 9.07))
    path.addLine(to: CGPoint(x: 3.4479999999999986, y: 9.102))
    path.addLine(to: CGPoint(x: 3.4479999999999986, y: 42))
    path.addLine(to: CGPoint(x: 0, y: 42))
    path.addLine(to: CGPoint(x: 0, y: 9.442))
    path.addCurve(to: CGPoint(x: 3.787, y: 5.647), controlPoint1: CGPoint(x: 0, y: 7.346), controlPoint2: CGPoint(x: 1.696, y: 5.647))
    path.addLine(to: CGPoint(x: 13.713000000000001, y: 5.647))
    path.addLine(to: CGPoint(x: 18.048, y: 0.862))
    path.addCurve(to: CGPoint(x: 19.996, y: 0), controlPoint1: CGPoint(x: 18.546999999999997, y: 0.31199999999999994), controlPoint2: CGPoint(x: 19.253999999999998, y: -0.0020000000000000018))
    path.addLine(to: CGPoint(x: 30.561999999999998, y: 0.02))
    path.addCurve(to: CGPoint(x: 32.483999999999995, y: 0.865), controlPoint1: CGPoint(x: 31.291999999999998, y: 0.02), controlPoint2: CGPoint(x: 31.988999999999997, y: 0.327))
    path.addLine(to: CGPoint(x: 36.895999999999994, y: 5.647))
    path.addLine(to: CGPoint(x: 50, y: 5.647))
    path.addLine(to: CGPoint(x: 50, y: 9.103))
    path.addLine(to: CGPoint(x: 36.864000000000004, y: 9.062))
    path.addCurve(to: CGPoint(x: 34.31400000000001, y: 7.938999999999999), controlPoint1: CGPoint(x: 35.895, y: 9.059), controlPoint2: CGPoint(x: 34.972, y: 8.652))
    path.addLine(to: CGPoint(x: 30.177000000000007, y: 3.453999999999999))
    path.addLine(to: CGPoint(x: 20.43200000000001, y: 3.453999999999999))
    path.addLine(to: CGPoint(x: 16.284, y: 7.95))
    path.close()
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.lineWidth = 1
    shapeLayer.path = path.cgPath
    shapeLayer.strokeStart = 0.0
    shapeLayer.strokeEnd = 1.0
    
    let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
    startAnimation.fromValue = 0.0
    startAnimation.toValue = 1.0
    
    let animation = CAAnimationGroup()
    animation.animations = [startAnimation]
    animation.duration = 1
    shapeLayer.add(animation, forKey: "MyAnimation")
    shapeLayer.transform = CATransform3DMakeScale(0.45,0.45,1.0)
    return shapeLayer
}()




var noSpeaker: CAShapeLayer = {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 9.377, y: 31.7))
    path.addCurve(to: CGPoint(x: 10.484, y: 31.773), controlPoint1: CGPoint(x: 9.699, y: 31.596999999999998), controlPoint2: CGPoint(x: 10.077, y: 31.57))
    path.addLine(to: CGPoint(x: 36.734, y: 43.893))
    path.addLine(to: CGPoint(x: 36.871, y: 44.078))
    path.addLine(to: CGPoint(x: 36.584, y: 47))
    path.addLine(to: CGPoint(x: 12.223, y: 36.12))
    path.addCurve(to: CGPoint(x: 10.471, y: 35.485), controlPoint1: CGPoint(x: 11.904, y: 35.96), controlPoint2: CGPoint(x: 11.176, y: 35.628))
    path.addCurve(to: CGPoint(x: 10.252, y: 35.444), controlPoint1: CGPoint(x: 10.397, y: 35.47), controlPoint2: CGPoint(x: 10.324, y: 35.455999999999996))
    path.addCurve(to: CGPoint(x: 9.969000000000001, y: 35.534000000000006), controlPoint1: CGPoint(x: 10.182, y: 35.519000000000005), controlPoint2: CGPoint(x: 10.09, y: 35.553000000000004))
    path.addCurve(to: CGPoint(x: 0.20300000000000118, y: 35.17400000000001), controlPoint1: CGPoint(x: 9.264000000000001, y: 35.42400000000001), controlPoint2: CGPoint(x: 0.3520000000000003, y: 35.48100000000001))
    path.addCurve(to: CGPoint(x: 0.03400000000000117, y: 30.599000000000007), controlPoint1: CGPoint(x: -0.09999999999999881, y: 34.547000000000004), controlPoint2: CGPoint(x: 0.07700000000000118, y: 32.507000000000005))
    path.addCurve(to: CGPoint(x: 0.04900000000000117, y: 22.012000000000008), controlPoint1: CGPoint(x: -0.045999999999998833, y: 26.98100000000001), controlPoint2: CGPoint(x: 0.04000000000000117, y: 22.41100000000001))
    path.addCurve(to: CGPoint(x: 0.17100000000000115, y: 12.212000000000007), controlPoint1: CGPoint(x: 0.13900000000000118, y: 18.102000000000007), controlPoint2: CGPoint(x: 0.2600000000000012, y: 12.330000000000007))
    path.addCurve(to: CGPoint(x: 9.723, y: 12.120000000000006), controlPoint1: CGPoint(x: -0.15399999999999886, y: 11.784000000000006), controlPoint2: CGPoint(x: 9.723, y: 12.120000000000006))
    path.addLine(to: CGPoint(x: 35.293, y: 0.319))
    path.addCurve(to: CGPoint(x: 40.915, y: 5.405), controlPoint1: CGPoint(x: 38.033, y: -0.937), controlPoint2: CGPoint(x: 40.915, y: 1.67))
    path.addLine(to: CGPoint(x: 40.915, y: 31.6))
    path.addCurve(to: CGPoint(x: 40.915, y: 32.992000000000004), controlPoint1: CGPoint(x: 41.096, y: 31.624000000000002), controlPoint2: CGPoint(x: 40.915, y: 36.83))
    path.addCurve(to: CGPoint(x: 40.774, y: 40.37200000000001), controlPoint1: CGPoint(x: 40.915, y: 30.731000000000005), controlPoint2: CGPoint(x: 41.18, y: 40.416000000000004))
    path.addCurve(to: CGPoint(x: 37.653, y: 31.782000000000007), controlPoint1: CGPoint(x: 39.232, y: 40.21000000000001), controlPoint2: CGPoint(x: 37.653, y: 38.327000000000005))
    path.addLine(to: CGPoint(x: 37.653, y: 5.47))
    path.addCurve(to: CGPoint(x: 36.361, y: 3.925), controlPoint1: CGPoint(x: 37.653, y: 4.343), controlPoint2: CGPoint(x: 37.19, y: 3.553))
    path.addLine(to: CGPoint(x: 10.047, y: 15.78))
    path.addCurve(to: CGPoint(x: 3.633000000000001, y: 15.825), controlPoint1: CGPoint(x: 10.047, y: 15.78), controlPoint2: CGPoint(x: 4.994000000000001, y: 15.774))
    path.addCurve(to: CGPoint(x: 3.395000000000001, y: 23.044999999999998), controlPoint1: CGPoint(x: 3.213000000000001, y: 15.84), controlPoint2: CGPoint(x: 3.397000000000001, y: 19.284))
    path.addCurve(to: CGPoint(x: 3.478000000000001, y: 31.711), controlPoint1: CGPoint(x: 3.392000000000001, y: 28.351999999999997), controlPoint2: CGPoint(x: 3.478000000000001, y: 33.894999999999996))
    path.addCurve(to: CGPoint(x: 8.218000000000002, y: 31.741), controlPoint1: CGPoint(x: 3.478000000000001, y: 31.651), controlPoint2: CGPoint(x: 6.0550000000000015, y: 31.779999999999998))
    path.addCurve(to: CGPoint(x: 9.377000000000002, y: 31.7), controlPoint1: CGPoint(x: 8.646000000000003, y: 31.733), controlPoint2: CGPoint(x: 9.042000000000002, y: 31.714))
    path.close()
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.lineWidth = 1
    shapeLayer.path = path.cgPath
    shapeLayer.strokeStart = 0.0
    shapeLayer.strokeEnd = 1.0
    
    let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
    startAnimation.fromValue = 0.0
    startAnimation.toValue = 1.0
    
    let animation = CAAnimationGroup()
    animation.animations = [startAnimation]
    animation.duration = 1
    shapeLayer.add(animation, forKey: "MyAnimation")
    shapeLayer.transform = CATransform3DMakeScale(0.45,0.45,1.0)
    return shapeLayer
}()




var speaker: CAShapeLayer = {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 40.342, y: 46.649))
    path.addLine(to: CGPoint(x: 40.452, y: 47))
    path.addLine(to: CGPoint(x: 36.352, y: 46.948))
    path.addLine(to: CGPoint(x: 12.345999999999997, y: 36.081))
    path.addCurve(to: CGPoint(x: 10.403999999999996, y: 35.446000000000005), controlPoint1: CGPoint(x: 12.029999999999996, y: 35.92100000000001), controlPoint2: CGPoint(x: 11.104999999999997, y: 35.589000000000006))
    path.addCurve(to: CGPoint(x: 10.186999999999996, y: 35.40500000000001), controlPoint1: CGPoint(x: 10.331999999999997, y: 35.431000000000004), controlPoint2: CGPoint(x: 10.258999999999997, y: 35.418000000000006))
    path.addCurve(to: CGPoint(x: 9.906999999999996, y: 35.49500000000001), controlPoint1: CGPoint(x: 10.117999999999995, y: 35.48000000000001), controlPoint2: CGPoint(x: 10.026999999999996, y: 35.51400000000001))
    path.addCurve(to: CGPoint(x: 0.2019999999999964, y: 35.13500000000001), controlPoint1: CGPoint(x: 9.204999999999997, y: 35.38500000000001), controlPoint2: CGPoint(x: 0.34899999999999665, y: 35.442000000000014))
    path.addCurve(to: CGPoint(x: 0.033999999999996394, y: 30.565000000000012), controlPoint1: CGPoint(x: -0.09900000000000359, y: 34.509000000000015), controlPoint2: CGPoint(x: 0.0769999999999964, y: 32.47100000000001))
    path.addCurve(to: CGPoint(x: 0.04799999999999639, y: 21.988000000000014), controlPoint1: CGPoint(x: -0.04600000000000361, y: 26.95100000000001), controlPoint2: CGPoint(x: 0.03899999999999639, y: 22.385000000000012))
    path.addCurve(to: CGPoint(x: 0.16999999999999638, y: 12.198000000000015), controlPoint1: CGPoint(x: 0.1379999999999964, y: 18.082000000000015), controlPoint2: CGPoint(x: 0.2579999999999964, y: 12.317000000000014))
    path.addCurve(to: CGPoint(x: 9.661999999999997, y: 12.107000000000015), controlPoint1: CGPoint(x: -0.15300000000000363, y: 11.771000000000015), controlPoint2: CGPoint(x: 9.661999999999997, y: 12.107000000000015))
    path.addLine(to: CGPoint(x: 35.069, y: 0.319))
    path.addCurve(to: CGPoint(x: 40.656000000000006, y: 5.399), controlPoint1: CGPoint(x: 37.792, y: -0.9359999999999999), controlPoint2: CGPoint(x: 40.656000000000006, y: 1.669))
    path.addLine(to: CGPoint(x: 40.656000000000006, y: 31.566000000000003))
    path.addCurve(to: CGPoint(x: 40.656000000000006, y: 32.956), controlPoint1: CGPoint(x: 40.836000000000006, y: 31.590000000000003), controlPoint2: CGPoint(x: 40.656000000000006, y: 36.791000000000004))
    path.addCurve(to: CGPoint(x: 40.516000000000005, y: 46.666000000000004), controlPoint1: CGPoint(x: 40.656000000000006, y: 30.698000000000004), controlPoint2: CGPoint(x: 40.92000000000001, y: 46.709))
    path.addLine(to: CGPoint(x: 40.342000000000006, y: 46.649))
    path.close()
    path.move(to: CGPoint(x: 37.61800000000001, y: 43.715))
    path.addCurve(to: CGPoint(x: 37.415000000000006, y: 31.748000000000005), controlPoint1: CGPoint(x: 37.07400000000001, y: 40.533), controlPoint2: CGPoint(x: 37.415000000000006, y: 35.39))
    path.addLine(to: CGPoint(x: 37.415000000000006, y: 5.464))
    path.addCurve(to: CGPoint(x: 36.13100000000001, y: 3.9200000000000004), controlPoint1: CGPoint(x: 37.415000000000006, y: 4.338000000000001), controlPoint2: CGPoint(x: 36.955000000000005, y: 3.5490000000000004))
    path.addLine(to: CGPoint(x: 9.984, y: 15.763))
    path.addCurve(to: CGPoint(x: 3.6100000000000003, y: 15.808), controlPoint1: CGPoint(x: 9.984, y: 15.763), controlPoint2: CGPoint(x: 4.962, y: 15.758))
    path.addCurve(to: CGPoint(x: 3.373, y: 23.02), controlPoint1: CGPoint(x: 3.1930000000000005, y: 15.824), controlPoint2: CGPoint(x: 3.3760000000000003, y: 19.264))
    path.addCurve(to: CGPoint(x: 3.4560000000000004, y: 31.677), controlPoint1: CGPoint(x: 3.37, y: 28.322), controlPoint2: CGPoint(x: 3.4560000000000004, y: 33.859))
    path.addCurve(to: CGPoint(x: 8.166, y: 31.707), controlPoint1: CGPoint(x: 3.4560000000000004, y: 31.617), controlPoint2: CGPoint(x: 6.016, y: 31.746))
    path.addCurve(to: CGPoint(x: 9.318, y: 31.666), controlPoint1: CGPoint(x: 8.591000000000001, y: 31.699), controlPoint2: CGPoint(x: 8.986, y: 31.68))
    path.addCurve(to: CGPoint(x: 10.418, y: 31.738), controlPoint1: CGPoint(x: 9.638, y: 31.562), controlPoint2: CGPoint(x: 10.014, y: 31.535))
    path.addLine(to: CGPoint(x: 37.617999999999995, y: 43.715))
    path.close()
    path.move(to: CGPoint(x: 45.77499999999999, y: 40.331))
    path.addCurve(to: CGPoint(x: 55.05099999999999, y: 23.107000000000003), controlPoint1: CGPoint(x: 51.29499999999999, y: 37.425000000000004), controlPoint2: CGPoint(x: 55.05099999999999, y: 29.709000000000003))
    path.addCurve(to: CGPoint(x: 47.64899999999999, y: 7.221000000000004), controlPoint1: CGPoint(x: 55.05099999999999, y: 17.282000000000004), controlPoint2: CGPoint(x: 52.127999999999986, y: 10.349000000000004))
    path.addLine(to: CGPoint(x: 50.472999999999985, y: 4.435000000000004))
    path.addCurve(to: CGPoint(x: 59, y: 23.107), controlPoint1: CGPoint(x: 55.653, y: 8.299), controlPoint2: CGPoint(x: 59, y: 16.208))
    path.addCurve(to: CGPoint(x: 48.67, y: 43.186), controlPoint1: CGPoint(x: 59, y: 30.777), controlPoint2: CGPoint(x: 54.863, y: 39.474000000000004))
    path.addLine(to: CGPoint(x: 45.775, y: 40.331))
    path.close()
    path.move(to: CGPoint(x: 45.734, y: 33.761))
    path.addCurve(to: CGPoint(x: 43.581, y: 30.015000000000004), controlPoint1: CGPoint(x: 43.916000000000004, y: 37.725), controlPoint2: CGPoint(x: 42.045, y: 34.411))
    path.addCurve(to: CGPoint(x: 45.013000000000005, y: 23.107000000000003), controlPoint1: CGPoint(x: 44.375, y: 27.739000000000004), controlPoint2: CGPoint(x: 45.013000000000005, y: 25.358000000000004))
    path.addCurve(to: CGPoint(x: 43.886, y: 17.635), controlPoint1: CGPoint(x: 45.013000000000005, y: 20.955000000000002), controlPoint2: CGPoint(x: 44.614000000000004, y: 19.841))
    path.addCurve(to: CGPoint(x: 45.895, y: 13.427000000000001), controlPoint1: CGPoint(x: 42.641000000000005, y: 13.868000000000002), controlPoint2: CGPoint(x: 44.404, y: 9.862000000000002))
    path.addCurve(to: CGPoint(x: 48.113, y: 17.682000000000002), controlPoint1: CGPoint(x: 46.480000000000004, y: 14.829), controlPoint2: CGPoint(x: 47.550000000000004, y: 16.251))
    path.addCurve(to: CGPoint(x: 49.163, y: 23.107000000000003), controlPoint1: CGPoint(x: 48.822, y: 19.483), controlPoint2: CGPoint(x: 49.163, y: 21.299000000000003))
    path.addCurve(to: CGPoint(x: 48.081999999999994, y: 29.190000000000005), controlPoint1: CGPoint(x: 49.163, y: 25.1), controlPoint2: CGPoint(x: 48.827, y: 27.163000000000004))
    path.addCurve(to: CGPoint(x: 45.733999999999995, y: 33.760000000000005), controlPoint1: CGPoint(x: 47.507999999999996, y: 30.751000000000005), controlPoint2: CGPoint(x: 46.407999999999994, y: 32.291000000000004))
    path.close()
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.lineWidth = 1
    shapeLayer.path = path.cgPath
    shapeLayer.strokeStart = 0.0
    shapeLayer.strokeEnd = 1.0
    
    let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
    startAnimation.fromValue = 0.0
    startAnimation.toValue = 1.0
    
    let animation = CAAnimationGroup()
    animation.animations = [startAnimation]
    animation.duration = 1
    shapeLayer.add(animation, forKey: "MyAnimation")
    shapeLayer.transform = CATransform3DMakeScale(0.45,0.45,1.0)
    return shapeLayer
}()



var muted: CAShapeLayer = {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 31.052, y: 27.238))
    path.addCurve(to: CGPoint(x: 36.003, y: 32.131), controlPoint1: CGPoint(x: 32.762, y: 28.927), controlPoint2: CGPoint(x: 34.426, y: 30.570999999999998))
    path.addCurve(to: CGPoint(x: 37.048, y: 28.094), controlPoint1: CGPoint(x: 36.508, y: 30.855), controlPoint2: CGPoint(x: 36.863, y: 29.502))
    path.addLine(to: CGPoint(x: 40.497, y: 28.094))
    path.addCurve(to: CGPoint(x: 38.617, y: 34.714), controlPoint1: CGPoint(x: 40.242, y: 30.445), controlPoint2: CGPoint(x: 39.592, y: 32.675000000000004))
    path.addCurve(to: CGPoint(x: 46.455, y: 42.42), controlPoint1: CGPoint(x: 41.9, y: 37.96), controlPoint2: CGPoint(x: 44.650999999999996, y: 40.674))
    path.addCurve(to: CGPoint(x: 44.155, y: 44.767), controlPoint1: CGPoint(x: 48.495, y: 44.394), controlPoint2: CGPoint(x: 44.155, y: 44.767))
    path.addCurve(to: CGPoint(x: 36.896, y: 37.652), controlPoint1: CGPoint(x: 44.155, y: 44.715), controlPoint2: CGPoint(x: 41.235, y: 41.862))
    path.addCurve(to: CGPoint(x: 31.547, y: 42.862), controlPoint1: CGPoint(x: 35.449, y: 39.708), controlPoint2: CGPoint(x: 33.633, y: 41.478))
    path.addLine(to: CGPoint(x: 30.192, y: 39.609))
    path.addCurve(to: CGPoint(x: 34.393, y: 35.225), controlPoint1: CGPoint(x: 31.841, y: 38.42), controlPoint2: CGPoint(x: 33.266, y: 36.933))
    path.addCurve(to: CGPoint(x: 30.267, y: 31.231), controlPoint1: CGPoint(x: 33.092, y: 33.965), controlPoint2: CGPoint(x: 31.707, y: 32.623000000000005))
    path.addCurve(to: CGPoint(x: 20.642, y: 37.536), controlPoint1: CGPoint(x: 28.604, y: 34.95), controlPoint2: CGPoint(x: 24.921, y: 37.536))
    path.addCurve(to: CGPoint(x: 10.336, y: 29.173000000000002), controlPoint1: CGPoint(x: 15.611999999999998, y: 37.536), controlPoint2: CGPoint(x: 11.405, y: 33.961))
    path.addCurve(to: CGPoint(x: 10.228, y: 28.211000000000002), controlPoint1: CGPoint(x: 10.266, y: 28.858), controlPoint2: CGPoint(x: 10.229000000000001, y: 28.534000000000002))
    path.addLine(to: CGPoint(x: 10.158999999999999, y: 11.809000000000001))
    path.addLine(to: CGPoint(x: 9.643999999999998, y: 11.312000000000001))
    path.addLine(to: CGPoint(x: 0, y: 2.012))
    path.addCurve(to: CGPoint(x: 3.765, y: 1.0050000000000001), controlPoint1: CGPoint(x: 0, y: 2.012), controlPoint2: CGPoint(x: 0.65, y: -1.742))
    path.addCurve(to: CGPoint(x: 10.618, y: 7.336), controlPoint1: CGPoint(x: 5.59, y: 2.616), controlPoint2: CGPoint(x: 7.945, y: 4.8))
    path.addCurve(to: CGPoint(x: 17.935000000000002, y: 2.1260000000000003), controlPoint1: CGPoint(x: 11.704, y: 4.297000000000001), controlPoint2: CGPoint(x: 14.569, y: 2.1260000000000003))
    path.addLine(to: CGPoint(x: 27.133000000000003, y: 2.503))
    path.addLine(to: CGPoint(x: 26.759000000000004, y: 5.651))
    path.addLine(to: CGPoint(x: 17.679000000000002, y: 5.45))
    path.addCurve(to: CGPoint(x: 13.409000000000002, y: 9.815000000000001), controlPoint1: CGPoint(x: 15.312000000000001, y: 5.45), controlPoint2: CGPoint(x: 13.397000000000002, y: 7.408))
    path.addLine(to: CGPoint(x: 13.410000000000002, y: 10.002))
    path.addCurve(to: CGPoint(x: 27.897000000000002, y: 24.127000000000002), controlPoint1: CGPoint(x: 17.899, y: 14.309000000000001), controlPoint2: CGPoint(x: 22.997, y: 19.301000000000002))
    path.addLine(to: CGPoint(x: 27.947000000000003, y: 12.300000000000002))
    path.addLine(to: CGPoint(x: 31.017000000000003, y: 12.300000000000002))
    path.addLine(to: CGPoint(x: 31.052000000000003, y: 27.238000000000003))
    path.close()
    path.move(to: CGPoint(x: 13.436, y: 14.97))
    path.addLine(to: CGPoint(x: 13.501, y: 27.632))
    path.addCurve(to: CGPoint(x: 13.626, y: 28.665000000000003), controlPoint1: CGPoint(x: 13.503, y: 27.98), controlPoint2: CGPoint(x: 13.54, y: 28.328000000000003))
    path.addCurve(to: CGPoint(x: 20.692, y: 34.211000000000006), controlPoint1: CGPoint(x: 14.446, y: 31.855000000000004), controlPoint2: CGPoint(x: 17.298, y: 34.211000000000006))
    path.addCurve(to: CGPoint(x: 27.432000000000002, y: 29.643000000000008), controlPoint1: CGPoint(x: 23.729, y: 34.211000000000006), controlPoint2: CGPoint(x: 26.332, y: 32.325))
    path.addCurve(to: CGPoint(x: 27.713, y: 28.76000000000001), controlPoint1: CGPoint(x: 27.55, y: 29.35600000000001), controlPoint2: CGPoint(x: 27.643, y: 29.061000000000007))
    path.addCurve(to: CGPoint(x: 13.436000000000002, y: 14.97000000000001), controlPoint1: CGPoint(x: 23.009, y: 24.210000000000008), controlPoint2: CGPoint(x: 17.938000000000002, y: 19.314000000000007))
    path.close()
    path.move(to: CGPoint(x: 0.81, y: 28.094))
    path.addLine(to: CGPoint(x: 4.186999999999999, y: 28.094))
    path.addCurve(to: CGPoint(x: 18.493, y: 42.561), controlPoint1: CGPoint(x: 5.1739999999999995, y: 35.614000000000004), controlPoint2: CGPoint(x: 11.029, y: 41.562))
    path.addLine(to: CGPoint(x: 24.078, y: 42.575))
    path.addCurve(to: CGPoint(x: 27.948, y: 46.52), controlPoint1: CGPoint(x: 26.216, y: 42.580000000000005), controlPoint2: CGPoint(x: 27.948, y: 44.345000000000006))
    path.addLine(to: CGPoint(x: 27.948, y: 51.737))
    path.addCurve(to: CGPoint(x: 26.705000000000002, y: 53), controlPoint1: CGPoint(x: 27.948, y: 52.434000000000005), controlPoint2: CGPoint(x: 27.391000000000002, y: 53))
    path.addLine(to: CGPoint(x: 13.387, y: 53))
    path.addLine(to: CGPoint(x: 13.387, y: 49.675))
    path.addLine(to: CGPoint(x: 24.678, y: 49.675))
    path.addLine(to: CGPoint(x: 24.678, y: 46.15))
    path.addLine(to: CGPoint(x: 19.249000000000002, y: 46.095))
    path.addCurve(to: CGPoint(x: 0.8100000000000023, y: 28.095), controlPoint1: CGPoint(x: 9.614000000000003, y: 45.413), controlPoint2: CGPoint(x: 1.8620000000000019, y: 37.783))
    path.close()
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.lineWidth = 1
    shapeLayer.path = path.cgPath
    shapeLayer.strokeStart = 0.0
    shapeLayer.strokeEnd = 1.0
    
    let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
    startAnimation.fromValue = 0.0
    startAnimation.toValue = 1.0
    
    let animation = CAAnimationGroup()
    animation.animations = [startAnimation]
    animation.duration = 1
    shapeLayer.add(animation, forKey: "MyAnimation")
    shapeLayer.transform = CATransform3DMakeScale(0.45,0.45,1.0)
    return shapeLayer
}()




var mic: CAShapeLayer = {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 30.446, y: 10))
    path.addLine(to: CGPoint(x: 30.482000000000003, y: 25.35))
    path.addCurve(to: CGPoint(x: 30.262000000000004, y: 27.040000000000003), controlPoint1: CGPoint(x: 30.483000000000004, y: 25.92), controlPoint2: CGPoint(x: 30.414, y: 26.490000000000002))
    path.addCurve(to: CGPoint(x: 19.989000000000004, y: 34.802), controlPoint1: CGPoint(x: 29.022000000000006, y: 31.514000000000003), controlPoint2: CGPoint(x: 24.894000000000005, y: 34.802))
    path.addCurve(to: CGPoint(x: 9.601000000000004, y: 26.582), controlPoint1: CGPoint(x: 14.918000000000005, y: 34.802), controlPoint2: CGPoint(x: 10.679000000000004, y: 31.289))
    path.addCurve(to: CGPoint(x: 9.492000000000004, y: 25.637), controlPoint1: CGPoint(x: 9.530000000000005, y: 26.272000000000002), controlPoint2: CGPoint(x: 9.493000000000004, y: 25.955000000000002))
    path.addLine(to: CGPoint(x: 9.415, y: 7.813))
    path.addCurve(to: CGPoint(x: 17.26, y: 0), controlPoint1: CGPoint(x: 9.397, y: 3.503), controlPoint2: CGPoint(x: 12.915, y: 0))
    path.addLine(to: CGPoint(x: 26.531000000000002, y: 0.372))
    path.addLine(to: CGPoint(x: 26.153000000000002, y: 3.465))
    path.addLine(to: CGPoint(x: 17.003, y: 3.267))
    path.addCurve(to: CGPoint(x: 12.699, y: 7.557), controlPoint1: CGPoint(x: 14.617, y: 3.267), controlPoint2: CGPoint(x: 12.686, y: 5.192))
    path.addLine(to: CGPoint(x: 12.791, y: 25.069000000000003))
    path.addCurve(to: CGPoint(x: 12.917, y: 26.084000000000003), controlPoint1: CGPoint(x: 12.793000000000001, y: 25.411), controlPoint2: CGPoint(x: 12.831, y: 25.753000000000004))
    path.addCurve(to: CGPoint(x: 20.039, y: 31.534000000000002), controlPoint1: CGPoint(x: 13.744, y: 29.220000000000002), controlPoint2: CGPoint(x: 16.617, y: 31.534000000000002))
    path.addCurve(to: CGPoint(x: 26.833000000000002, y: 27.046000000000003), controlPoint1: CGPoint(x: 23.099, y: 31.534000000000002), controlPoint2: CGPoint(x: 25.722, y: 29.682000000000002))
    path.addCurve(to: CGPoint(x: 27.288, y: 24.696), controlPoint1: CGPoint(x: 27.145000000000003, y: 26.303000000000004), controlPoint2: CGPoint(x: 27.284000000000002, y: 25.500000000000004))
    path.addLine(to: CGPoint(x: 27.351, y: 10))
    path.addLine(to: CGPoint(x: 30.445999999999998, y: 10))
    path.close()
    path.move(to: CGPoint(x: 0, y: 25.523))
    path.addLine(to: CGPoint(x: 3.403, y: 25.523))
    path.addCurve(to: CGPoint(x: 17.823, y: 39.74), controlPoint1: CGPoint(x: 4.399, y: 32.913), controlPoint2: CGPoint(x: 10.3, y: 38.757999999999996))
    path.addLine(to: CGPoint(x: 23.451, y: 39.754000000000005))
    path.addCurve(to: CGPoint(x: 27.351, y: 43.632000000000005), controlPoint1: CGPoint(x: 25.607, y: 39.760000000000005), controlPoint2: CGPoint(x: 27.351, y: 41.49400000000001))
    path.addLine(to: CGPoint(x: 27.351, y: 48.758))
    path.addCurve(to: CGPoint(x: 26.099, y: 50), controlPoint1: CGPoint(x: 27.351, y: 49.444), controlPoint2: CGPoint(x: 26.791, y: 50))
    path.addLine(to: CGPoint(x: 12.676, y: 50))
    path.addLine(to: CGPoint(x: 12.676, y: 46.733))
    path.addLine(to: CGPoint(x: 24.056, y: 46.733))
    path.addLine(to: CGPoint(x: 24.056, y: 43.266999999999996))
    path.addLine(to: CGPoint(x: 18.585, y: 43.212999999999994))
    path.addCurve(to: CGPoint(x: 0, y: 25.523), controlPoint1: CGPoint(x: 8.873, y: 42.543), controlPoint2: CGPoint(x: 1.06, y: 35.045))
    path.close()
    path.move(to: CGPoint(x: 36.524, y: 25.523))
    path.addLine(to: CGPoint(x: 40, y: 25.523))
    path.addCurve(to: CGPoint(x: 30.98, y: 40.036), controlPoint1: CGPoint(x: 39.324, y: 31.6), controlPoint2: CGPoint(x: 35.896, y: 36.855000000000004))
    path.addLine(to: CGPoint(x: 29.613, y: 36.839))
    path.addCurve(to: CGPoint(x: 36.524, y: 25.522999999999996), controlPoint1: CGPoint(x: 33.311, y: 34.239), controlPoint2: CGPoint(x: 35.896, y: 30.189))
    path.close()
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
    shapeLayer.lineWidth = 1
    shapeLayer.path = path.cgPath
    shapeLayer.strokeStart = 0.0
    shapeLayer.strokeEnd = 1.0
    
    let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
    startAnimation.fromValue = 0.0
    startAnimation.toValue = 1.0
    
    let animation = CAAnimationGroup()
    animation.animations = [startAnimation]
    animation.duration = 1
    shapeLayer.add(animation, forKey: "MyAnimation")
    shapeLayer.transform = CATransform3DMakeScale(0.45,0.45,1.0)
    return shapeLayer
}()




var endCall: CAShapeLayer = {
    let path = UIBezierPath()
    path.move(to: CGPoint(x: 39.161, y: 11.053))
    path.addLine(to: CGPoint(x: 37.779, y: 14.005))
    path.addCurve(to: CGPoint(x: 26.283, y: 12.447000000000001), controlPoint1: CGPoint(x: 37.779, y: 14.005), controlPoint2: CGPoint(x: 32.829, y: 12.397))
    path.addCurve(to: CGPoint(x: 14.324000000000002, y: 14.386000000000001), controlPoint1: CGPoint(x: 19.737000000000002, y: 12.497000000000002), controlPoint2: CGPoint(x: 14.324000000000002, y: 14.386000000000001))
    path.addLine(to: CGPoint(x: 13.104000000000001, y: 11.295000000000002))
    path.addCurve(to: CGPoint(x: 26.306, y: 9.475000000000001), controlPoint1: CGPoint(x: 13.104000000000001, y: 11.295000000000002), controlPoint2: CGPoint(x: 19.559, y: 9.527000000000001))
    path.addCurve(to: CGPoint(x: 39.161, y: 11.053), controlPoint1: CGPoint(x: 33.053, y: 9.423000000000002), controlPoint2: CGPoint(x: 39.161, y: 11.053))
    path.close()
    path.move(to: CGPoint(x: 4.771, y: 25.137))
    path.addCurve(to: CGPoint(x: 0.35799999999999965, y: 21.979), controlPoint1: CGPoint(x: 2.585, y: 25.886), controlPoint2: CGPoint(x: 0.3410000000000002, y: 24.28))
    path.addLine(to: CGPoint(x: 0.43399999999999966, y: 12.116999999999999))
    path.addCurve(to: CGPoint(x: 2.3339999999999996, y: 8.424), controlPoint1: CGPoint(x: 0.4449999999999997, y: 10.661999999999999), controlPoint2: CGPoint(x: 1.1499999999999997, y: 9.289))
    path.addCurve(to: CGPoint(x: 3.5269999999999997, y: 7.590999999999999), controlPoint1: CGPoint(x: 2.6839999999999997, y: 8.168), controlPoint2: CGPoint(x: 3.0839999999999996, y: 7.888))
    path.addCurve(to: CGPoint(x: 25.84, y: 0.43), controlPoint1: CGPoint(x: 6.967, y: 4.963), controlPoint2: CGPoint(x: 14.455, y: 0.517))
    path.addCurve(to: CGPoint(x: 49.794, y: 7.847), controlPoint1: CGPoint(x: 37.858, y: 0.338), controlPoint2: CGPoint(x: 46.264, y: 5.292))
    path.addCurve(to: CGPoint(x: 51.638, y: 11.511000000000001), controlPoint1: CGPoint(x: 50.964, y: 8.693), controlPoint2: CGPoint(x: 51.648999999999994, y: 10.055))
    path.addLine(to: CGPoint(x: 51.562, y: 21.373))
    path.addCurve(to: CGPoint(x: 47.099999999999994, y: 24.598000000000003), controlPoint1: CGPoint(x: 51.544999999999995, y: 23.673000000000002), controlPoint2: CGPoint(x: 49.275, y: 25.314))
    path.addLine(to: CGPoint(x: 35.669999999999995, y: 20.838))
    path.addLine(to: CGPoint(x: 36.654999999999994, y: 17.989))
    path.addLine(to: CGPoint(x: 47.29899999999999, y: 21.525000000000002))
    path.addCurve(to: CGPoint(x: 48.568999999999996, y: 20.610000000000003), controlPoint1: CGPoint(x: 47.916999999999994, y: 21.73), controlPoint2: CGPoint(x: 48.562999999999995, y: 21.265))
    path.addLine(to: CGPoint(x: 48.629999999999995, y: 12.535000000000004))
    path.addCurve(to: CGPoint(x: 47.675999999999995, y: 10.323000000000004), controlPoint1: CGPoint(x: 48.63699999999999, y: 11.691000000000004), controlPoint2: CGPoint(x: 48.28999999999999, y: 10.890000000000004))
    path.addCurve(to: CGPoint(x: 35.464, y: 4.373000000000004), controlPoint1: CGPoint(x: 45.873, y: 8.659000000000004), controlPoint2: CGPoint(x: 42.020999999999994, y: 5.848000000000004))
    path.addCurve(to: CGPoint(x: 26.381999999999998, y: 3.6120000000000037), controlPoint1: CGPoint(x: 32.887, y: 3.8830000000000036), controlPoint2: CGPoint(x: 29.878, y: 3.5850000000000035))
    path.addCurve(to: CGPoint(x: 4.7819999999999965, y: 10.539000000000003), controlPoint1: CGPoint(x: 14.287999999999998, y: 3.7040000000000037), controlPoint2: CGPoint(x: 7.607999999999997, y: 8.062000000000005))
    path.addCurve(to: CGPoint(x: 3.9949999999999966, y: 11.440000000000003), controlPoint1: CGPoint(x: 4.426999999999996, y: 10.908000000000003), controlPoint2: CGPoint(x: 4.168999999999997, y: 11.217000000000004))
    path.addCurve(to: CGPoint(x: 3.5059999999999967, y: 12.433000000000003), controlPoint1: CGPoint(x: 3.7649999999999966, y: 11.735000000000003), controlPoint2: CGPoint(x: 3.5909999999999966, y: 12.070000000000004))
    path.addCurve(to: CGPoint(x: 3.4259999999999966, y: 13.095000000000004), controlPoint1: CGPoint(x: 3.455999999999997, y: 12.648000000000003), controlPoint2: CGPoint(x: 3.427999999999997, y: 12.870000000000003))
    path.addLine(to: CGPoint(x: 3.3639999999999968, y: 21.17))
    path.addCurve(to: CGPoint(x: 4.6199999999999966, y: 22.066000000000003), controlPoint1: CGPoint(x: 3.358999999999997, y: 21.825000000000003), controlPoint2: CGPoint(x: 3.998999999999997, y: 22.28))
    path.addLine(to: CGPoint(x: 15.319999999999997, y: 18.367000000000004))
    path.addLine(to: CGPoint(x: 16.259999999999998, y: 21.201000000000004))
    path.addLine(to: CGPoint(x: 4.7719999999999985, y: 25.137000000000004))
    path.close()
    
    let shapeLayer = CAShapeLayer()
    shapeLayer.fillColor = UIColor.white.cgColor
    shapeLayer.strokeColor =  UIColor.white.cgColor
    shapeLayer.lineWidth = 1
    shapeLayer.path = path.cgPath
    shapeLayer.strokeStart = 0.0
    shapeLayer.strokeEnd = 1.0
    
    let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
    startAnimation.fromValue = 0.0
    startAnimation.toValue = 1.0
    
    let animation = CAAnimationGroup()
    animation.animations = [startAnimation]
    animation.duration = 1
    shapeLayer.add(animation, forKey: "MyAnimation")
    shapeLayer.transform = CATransform3DMakeScale(0.45,0.45,1.0)
    return shapeLayer
}()
    
    var deleteButton: CAShapeLayer = {
     
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 33.43, y: 5.744))
        path.addLine(to: CGPoint(x: 25.939999999999998, y: 5.744))
        path.addCurve(to: CGPoint(x: 19.471, y: 0), controlPoint1: CGPoint(x: 25.567, y: 2.516), controlPoint2: CGPoint(x: 22.81, y: 0))
        path.addCurve(to: CGPoint(x: 13.003, y: 5.744), controlPoint1: CGPoint(x: 16.133, y: 0), controlPoint2: CGPoint(x: 13.376999999999999, y: 2.516))
        path.addLine(to: CGPoint(x: 5.513, y: 5.744))
        path.addCurve(to: CGPoint(x: 0.014, y: 11.221), controlPoint1: CGPoint(x: 2.48, y: 5.744), controlPoint2: CGPoint(x: 0.014, y: 8.201))
        path.addLine(to: CGPoint(x: 0.014, y: 11.503))
        path.addCurve(to: CGPoint(x: 3.489, y: 16.591), controlPoint1: CGPoint(x: 0.014, y: 13.81), controlPoint2: CGPoint(x: 1.457, y: 15.786000000000001))
        path.addLine(to: CGPoint(x: 3.489, y: 42.466))
        path.addCurve(to: CGPoint(x: 8.986, y: 47.943), controlPoint1: CGPoint(x: 3.489, y: 45.486000000000004), controlPoint2: CGPoint(x: 5.955, y: 47.943))
        path.addLine(to: CGPoint(x: 29.956, y: 47.943))
        path.addCurve(to: CGPoint(x: 35.452, y: 42.465999999999994), controlPoint1: CGPoint(x: 32.986, y: 47.943), controlPoint2: CGPoint(x: 35.452, y: 45.486))
        path.addLine(to: CGPoint(x: 35.452, y: 16.59))
        path.addCurve(to: CGPoint(x: 38.927, y: 11.501999999999999), controlPoint1: CGPoint(x: 37.483999999999995, y: 15.785), controlPoint2: CGPoint(x: 38.927, y: 13.81))
        path.addLine(to: CGPoint(x: 38.927, y: 11.220999999999998))
        path.addCurve(to: CGPoint(x: 33.43, y: 5.7429999999999986), controlPoint1: CGPoint(x: 38.927, y: 8.200999999999999), controlPoint2: CGPoint(x: 36.461, y: 5.7429999999999986))
        path.close()
        path.move(to: CGPoint(x: 19.472, y: 2.598))
        path.addCurve(to: CGPoint(x: 23.307000000000002, y: 5.744), controlPoint1: CGPoint(x: 21.372, y: 2.598), controlPoint2: CGPoint(x: 22.956000000000003, y: 3.9539999999999997))
        path.addLine(to: CGPoint(x: 15.639000000000003, y: 5.744))
        path.addCurve(to: CGPoint(x: 19.472, y: 2.598), controlPoint1: CGPoint(x: 15.989000000000003, y: 3.9539999999999997), controlPoint2: CGPoint(x: 17.574, y: 2.598))
        path.close()
        path.move(to: CGPoint(x: 32.847, y: 42.466))
        path.addCurve(to: CGPoint(x: 29.957, y: 45.346000000000004), controlPoint1: CGPoint(x: 32.847, y: 44.054), controlPoint2: CGPoint(x: 31.551000000000002, y: 45.346000000000004))
        path.addLine(to: CGPoint(x: 8.986, y: 45.346000000000004))
        path.addCurve(to: CGPoint(x: 6.096, y: 42.466), controlPoint1: CGPoint(x: 7.393000000000001, y: 45.346000000000004), controlPoint2: CGPoint(x: 6.096, y: 44.054))
        path.addLine(to: CGPoint(x: 6.096, y: 16.98))
        path.addLine(to: CGPoint(x: 32.846000000000004, y: 16.98))
        path.addLine(to: CGPoint(x: 32.846000000000004, y: 42.466))
        path.close()
        path.move(to: CGPoint(x: 36.32000000000001, y: 11.503))
        path.addCurve(to: CGPoint(x: 33.43000000000001, y: 14.383), controlPoint1: CGPoint(x: 36.32000000000001, y: 13.09), controlPoint2: CGPoint(x: 35.02400000000001, y: 14.383))
        path.addLine(to: CGPoint(x: 5.512, y: 14.383))
        path.addCurve(to: CGPoint(x: 2.6219999999999994, y: 11.503), controlPoint1: CGPoint(x: 3.9189999999999996, y: 14.383), controlPoint2: CGPoint(x: 2.6219999999999994, y: 13.09))
        path.addLine(to: CGPoint(x: 2.6219999999999994, y: 11.221))
        path.addCurve(to: CGPoint(x: 5.512, y: 8.341000000000001), controlPoint1: CGPoint(x: 2.6219999999999994, y: 9.634), controlPoint2: CGPoint(x: 3.9189999999999996, y: 8.341000000000001))
        path.addLine(to: CGPoint(x: 33.431, y: 8.341000000000001))
        path.addCurve(to: CGPoint(x: 36.321, y: 11.221), controlPoint1: CGPoint(x: 35.024, y: 8.341000000000001), controlPoint2: CGPoint(x: 36.321, y: 9.634))
        path.addLine(to: CGPoint(x: 36.321, y: 11.503))
        path.close()
        path.move(to: CGPoint(x: 12.482, y: 42.024))
        path.addCurve(to: CGPoint(x: 13.786, y: 40.725), controlPoint1: CGPoint(x: 13.202, y: 42.024), controlPoint2: CGPoint(x: 13.786, y: 41.442))
        path.addLine(to: CGPoint(x: 13.786, y: 26.101))
        path.addCurve(to: CGPoint(x: 12.482, y: 24.802), controlPoint1: CGPoint(x: 13.786, y: 25.384), controlPoint2: CGPoint(x: 13.202, y: 24.802))
        path.addCurve(to: CGPoint(x: 11.178999999999998, y: 26.101), controlPoint1: CGPoint(x: 11.761999999999999, y: 24.802), controlPoint2: CGPoint(x: 11.178999999999998, y: 25.384))
        path.addLine(to: CGPoint(x: 11.178999999999998, y: 40.725))
        path.addCurve(to: CGPoint(x: 12.482, y: 42.024), controlPoint1: CGPoint(x: 11.178999999999998, y: 41.442), controlPoint2: CGPoint(x: 11.761999999999999, y: 42.024))
        path.close()
        path.move(to: CGPoint(x: 19.471, y: 42.024))
        path.addCurve(to: CGPoint(x: 20.775, y: 40.725), controlPoint1: CGPoint(x: 20.191, y: 42.024), controlPoint2: CGPoint(x: 20.775, y: 41.442))
        path.addLine(to: CGPoint(x: 20.775, y: 26.101))
        path.addCurve(to: CGPoint(x: 19.471, y: 24.802), controlPoint1: CGPoint(x: 20.775, y: 25.384), controlPoint2: CGPoint(x: 20.191, y: 24.802))
        path.addCurve(to: CGPoint(x: 18.168, y: 26.101), controlPoint1: CGPoint(x: 18.751, y: 24.802), controlPoint2: CGPoint(x: 18.168, y: 25.384))
        path.addLine(to: CGPoint(x: 18.168, y: 40.725))
        path.addCurve(to: CGPoint(x: 19.471, y: 42.024), controlPoint1: CGPoint(x: 18.168, y: 41.442), controlPoint2: CGPoint(x: 18.752, y: 42.024))
        path.close()
        path.move(to: CGPoint(x: 26.46, y: 42.024))
        path.addCurve(to: CGPoint(x: 27.764, y: 40.725), controlPoint1: CGPoint(x: 27.18, y: 42.024), controlPoint2: CGPoint(x: 27.764, y: 41.442))
        path.addLine(to: CGPoint(x: 27.764, y: 26.101))
        path.addCurve(to: CGPoint(x: 26.461, y: 24.802), controlPoint1: CGPoint(x: 27.764, y: 25.384), controlPoint2: CGPoint(x: 27.18, y: 24.802))
        path.addCurve(to: CGPoint(x: 25.157, y: 26.101), controlPoint1: CGPoint(x: 25.741, y: 24.802), controlPoint2: CGPoint(x: 25.157, y: 25.384))
        path.addLine(to: CGPoint(x: 25.157, y: 40.725))
        path.addCurve(to: CGPoint(x: 26.461, y: 42.024), controlPoint1: CGPoint(x: 25.157, y: 41.442), controlPoint2: CGPoint(x: 25.741, y: 42.024))
        path.close()
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
        shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.path = path.cgPath
        shapeLayer.strokeStart = 0.0
        shapeLayer.strokeEnd = 1.0
        
        let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
        startAnimation.fromValue = 0.0
        startAnimation.toValue = 1.0
        
        let animation = CAAnimationGroup()
        animation.animations = [startAnimation]
        animation.duration = 1
        shapeLayer.add(animation, forKey: "MyAnimation")
        shapeLayer.transform = CATransform3DMakeScale(0.55,0.55,1.0)
        return shapeLayer
    }()
    
    var save: CAShapeLayer = {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 29.688, y: 15.625))
        path.addCurve(to: CGPoint(x: 31.25, y: 14.062), controlPoint1: CGPoint(x: 30.549999999999997, y: 15.625), controlPoint2: CGPoint(x: 31.25, y: 14.925))
        path.addLine(to: CGPoint(x: 31.25, y: 7.811999999999999))
        path.addCurve(to: CGPoint(x: 29.687, y: 6.249999999999999), controlPoint1: CGPoint(x: 31.25, y: 6.949999999999999), controlPoint2: CGPoint(x: 30.55, y: 6.249999999999999))
        path.addCurve(to: CGPoint(x: 28.125, y: 7.812999999999999), controlPoint1: CGPoint(x: 28.825000000000003, y: 6.249999999999999), controlPoint2: CGPoint(x: 28.125, y: 6.949999999999999))
        path.addLine(to: CGPoint(x: 28.125, y: 14.062999999999999))
        path.addCurve(to: CGPoint(x: 29.688, y: 15.624999999999998), controlPoint1: CGPoint(x: 28.125, y: 14.924999999999999), controlPoint2: CGPoint(x: 28.825, y: 15.624999999999998))
        path.close()
        path.move(to: CGPoint(x: 43.75, y: 0))
        path.addLine(to: CGPoint(x: 6.25, y: 0))
        path.addCurve(to: CGPoint(x: 0, y: 6.25), controlPoint1: CGPoint(x: 2.798, y: 0), controlPoint2: CGPoint(x: 0, y: 2.798))
        path.addLine(to: CGPoint(x: 0, y: 43.75))
        path.addCurve(to: CGPoint(x: 6.25, y: 50), controlPoint1: CGPoint(x: 0, y: 47.202), controlPoint2: CGPoint(x: 2.798, y: 50))
        path.addLine(to: CGPoint(x: 43.75, y: 50))
        path.addCurve(to: CGPoint(x: 50, y: 43.75), controlPoint1: CGPoint(x: 47.202, y: 50), controlPoint2: CGPoint(x: 50, y: 47.202))
        path.addLine(to: CGPoint(x: 50, y: 6.25))
        path.addCurve(to: CGPoint(x: 43.75, y: 0), controlPoint1: CGPoint(x: 50, y: 2.798), controlPoint2: CGPoint(x: 47.202, y: 0))
        path.close()
        path.move(to: CGPoint(x: 12.5, y: 3.125))
        path.addLine(to: CGPoint(x: 37.5, y: 3.125))
        path.addLine(to: CGPoint(x: 37.5, y: 17.188000000000002))
        path.addCurve(to: CGPoint(x: 35.937, y: 18.750000000000004), controlPoint1: CGPoint(x: 37.5, y: 18.05), controlPoint2: CGPoint(x: 36.8, y: 18.750000000000004))
        path.addLine(to: CGPoint(x: 14.064, y: 18.750000000000004))
        path.addCurve(to: CGPoint(x: 12.501, y: 17.187000000000005), controlPoint1: CGPoint(x: 13.201, y: 18.750000000000004), controlPoint2: CGPoint(x: 12.501, y: 18.050000000000004))
        path.addLine(to: CGPoint(x: 12.501, y: 3.126))
        path.close()
        path.move(to: CGPoint(x: 46.875, y: 43.75))
        path.addCurve(to: CGPoint(x: 43.75, y: 46.875), controlPoint1: CGPoint(x: 46.875, y: 45.477), controlPoint2: CGPoint(x: 45.477, y: 46.875))
        path.addLine(to: CGPoint(x: 6.25, y: 46.875))
        path.addCurve(to: CGPoint(x: 3.125, y: 43.75), controlPoint1: CGPoint(x: 4.523, y: 46.875), controlPoint2: CGPoint(x: 3.125, y: 45.477))
        path.addLine(to: CGPoint(x: 3.125, y: 6.25))
        path.addCurve(to: CGPoint(x: 6.25, y: 3.125), controlPoint1: CGPoint(x: 3.125, y: 4.523), controlPoint2: CGPoint(x: 4.523, y: 3.125))
        path.addLine(to: CGPoint(x: 9.375, y: 3.125))
        path.addLine(to: CGPoint(x: 9.375, y: 18.75))
        path.addCurve(to: CGPoint(x: 12.5, y: 21.875), controlPoint1: CGPoint(x: 9.375, y: 20.475), controlPoint2: CGPoint(x: 10.773, y: 21.875))
        path.addLine(to: CGPoint(x: 37.5, y: 21.875))
        path.addCurve(to: CGPoint(x: 40.625, y: 18.75), controlPoint1: CGPoint(x: 39.227, y: 21.875), controlPoint2: CGPoint(x: 40.625, y: 20.475))
        path.addLine(to: CGPoint(x: 40.625, y: 3.125))
        path.addLine(to: CGPoint(x: 43.75, y: 3.125))
        path.addCurve(to: CGPoint(x: 46.875, y: 6.25), controlPoint1: CGPoint(x: 45.477, y: 3.125), controlPoint2: CGPoint(x: 46.875, y: 4.525))
        path.addLine(to: CGPoint(x: 46.875, y: 43.75))
        path.close()
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
        shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.path = path.cgPath
        shapeLayer.strokeStart = 0.0
        shapeLayer.strokeEnd = 1.0
        
        let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
        startAnimation.fromValue = 0.0
        startAnimation.toValue = 1.0
        
        let animation = CAAnimationGroup()
        animation.animations = [startAnimation]
        animation.duration = 1
        shapeLayer.add(animation, forKey: "MyAnimation")
        shapeLayer.transform = CATransform3DMakeScale(0.42,0.42,1.0)
        return shapeLayer
    }()
    
    
    var plus: CAShapeLayer = {
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 48.875, y: 23.375))
        path.addLine(to: CGPoint(x: 27.625, y: 23.375))
        path.addLine(to: CGPoint(x: 27.625, y: 2.125))
        path.addCurve(to: CGPoint(x: 25.5, y: 0), controlPoint1: CGPoint(x: 27.625, y: 0.951), controlPoint2: CGPoint(x: 26.674, y: 0))
        path.addCurve(to: CGPoint(x: 23.375, y: 2.125), controlPoint1: CGPoint(x: 24.326, y: 0), controlPoint2: CGPoint(x: 23.375, y: 0.951))
        path.addLine(to: CGPoint(x: 23.375, y: 23.375))
        path.addLine(to: CGPoint(x: 2.125, y: 23.375))
        path.addCurve(to: CGPoint(x: 0, y: 25.5), controlPoint1: CGPoint(x: 0.951, y: 23.375), controlPoint2: CGPoint(x: 0, y: 24.326))
        path.addCurve(to: CGPoint(x: 2.125, y: 27.625), controlPoint1: CGPoint(x: 0, y: 26.674), controlPoint2: CGPoint(x: 0.951, y: 27.625))
        path.addLine(to: CGPoint(x: 23.375, y: 27.625))
        path.addLine(to: CGPoint(x: 23.375, y: 48.875))
        path.addCurve(to: CGPoint(x: 25.5, y: 51), controlPoint1: CGPoint(x: 23.375, y: 50.049), controlPoint2: CGPoint(x: 24.326, y: 51))
        path.addCurve(to: CGPoint(x: 27.625, y: 48.875), controlPoint1: CGPoint(x: 26.673000000000002, y: 51), controlPoint2: CGPoint(x: 27.625, y: 50.049))
        path.addLine(to: CGPoint(x: 27.625, y: 27.625))
        path.addLine(to: CGPoint(x: 48.875, y: 27.625))
        path.addCurve(to: CGPoint(x: 51, y: 25.5), controlPoint1: CGPoint(x: 50.048, y: 27.625), controlPoint2: CGPoint(x: 51, y: 26.674))
        path.addCurve(to: CGPoint(x: 48.875, y: 23.375), controlPoint1: CGPoint(x: 51, y: 24.326), controlPoint2: CGPoint(x: 50.049, y: 23.375))
        path.close()
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.fillColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
        shapeLayer.strokeColor =  UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8).cgColor
        shapeLayer.lineWidth = 1
        shapeLayer.path = path.cgPath
        shapeLayer.strokeStart = 0.0
        shapeLayer.strokeEnd = 1.0
        
        let startAnimation = CABasicAnimation(keyPath: "strokeEnd")
        startAnimation.fromValue = 0.0
        startAnimation.toValue = 1.0
        
        let animation = CAAnimationGroup()
        animation.animations = [startAnimation]
        animation.duration = 1
        shapeLayer.add(animation, forKey: "MyAnimation")
        shapeLayer.transform = CATransform3DMakeScale(0.45,0.45,1.0)
        return shapeLayer
    }()
    
    
  
    
}
