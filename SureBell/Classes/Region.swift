//
//  Region.swift
//  SureBell
//
//  Created by PiOctave on 26/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import UIKit

public struct Anchor{
    let point:CGPoint
    let type:PointView.PointType
}


open class Region:UIBezierPath{
    let MAX_ANCHORS = 4
    var _pointViewArray = [PointView]()
    var _shapeLayer = CAShapeLayer()
    var context:UIView!
    var backgroundImage:UIImageView!
    var primary = UIColor.lightGray.withAlphaComponent(0.7)
    
    
    convenience init(region:Region) {
        self.init(anchors: region._pointViewArray.map {
            return Anchor(point: $0.center, type: $0.type)
        }, context: region.context, bgImage: region.backgroundImage);
    }
    
    init  (anchors: [Anchor],context:UIView, bgImage:UIImageView){
        super.init()
        self.context = context
        self.backgroundImage = bgImage
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeColor(_:)), name:NSNotification.Name(rawValue: "UPDATE_ROI_COLOR"), object: nil)
        
        for anchor in anchors{
            let point = PointView(pointType: anchor.type, pointColor: primary)
            let this = self
            point.center = anchor.point
            point.dragCallBack = { [unowned self] (pointView: PointView, touch:UITouch) -> Void in
                self.onDragCallChangeShape(point, touch: touch, pointView: pointView,region: this)
            }
            if anchor.type == PointView.PointType.temp{
                // Add new point only on drag of small points
                point.touchEndCallback = { [unowned self] (pointView: PointView) -> Void in
                    if(self._pointViewArray.count <= self.MAX_ANCHORS){
                        self.addDots(point, this: this)
                    }
                    this.changeshape()
                }
            }
            _pointViewArray.append(point);
        }
    }
    
    func getXWithinBound(dx deltaX:CGFloat, dy deltaY:CGFloat, center:CGPoint)-> CGPoint{
        var cx = center.x, cy = center.y
        if((cx + deltaX) < context.frame.width && (cx + deltaX) > 0){
            cx = center.x + deltaX
        }
        if((cy + deltaY) < context.frame.height && (cy + deltaY) > 0){
            cy = center.y + deltaY
        }
        
        return CGPoint(x: cx, y:cy);
    }
    
    func onDragCallChangeShape(_ point: PointView, touch: UITouch, pointView:PointView, region: Region){
        
        // On drag of big point only shape change happens
        if point.type == PointView.PointType.permanent && self._pointViewArray.count < self.MAX_ANCHORS{
            if let index = self._pointViewArray.index(of: point){
                var prevIndex = index - 1
                var nextIndex = index + 1
                if prevIndex < 0{
                    prevIndex = self._pointViewArray.count - 1
                }
                if nextIndex >= self._pointViewArray.count{
                    nextIndex = 0
                }
                let dx = (touch.location(in: pointView.superview).x - touch.previousLocation(in: pointView.superview).x)
                let dy = (touch.location(in: pointView.superview).y - touch.previousLocation(in: pointView.superview).y)
                
                self._pointViewArray[prevIndex].center = getXWithinBound(dx: dx/2, dy: dy/2, center: self._pointViewArray[prevIndex].center)
                self._pointViewArray[nextIndex].center = getXWithinBound(dx: dx/2, dy: dy/2, center: self._pointViewArray[nextIndex].center)
            }
        }
        region.changeshape()
    }
    
    func addDots(_ point:PointView, this:Region){
        if var index = self._pointViewArray.index(of: point){
            let currentPointUpdate = PointView(pointType : PointView.PointType.permanent, pointColor: primary)
            currentPointUpdate.center = point.center
            self._pointViewArray.remove(at: index)
            point.removeFromSuperview()
            self._pointViewArray.insert(currentPointUpdate, at: index)
            currentPointUpdate.dragCallBack = { [unowned self] (pointView: PointView, touch:UITouch) -> Void in
                self.onDragCallChangeShape(currentPointUpdate, touch: touch, pointView: pointView, region: this)
            }
            index = self._pointViewArray.index(of: currentPointUpdate)!
            let prevIndex = index == 0 ? self._pointViewArray.count - 1 : index - 1
            print(index, prevIndex)
            let newPreviousPoint = PointView(pointType : PointView.PointType.temp, pointColor: primary)
            newPreviousPoint.center = self.getMidPoint(self._pointViewArray[prevIndex].center, point2: point.center)
            self._pointViewArray.insert(newPreviousPoint, at: index)
            index = self._pointViewArray.index(of: currentPointUpdate)!
            let nextIndex = (index+1) % (self._pointViewArray.count)
            print(index, nextIndex)
            let newNextPoint = PointView(pointType : PointView.PointType.temp, pointColor: primary)
            newNextPoint.center = self.getMidPoint(currentPointUpdate.center, point2: self._pointViewArray[nextIndex].center)
            self._pointViewArray.insert(newNextPoint, at: nextIndex)
            newNextPoint.dragCallBack = { [unowned self] (pointView: PointView, touch:UITouch) -> Void in
                self.onDragCallChangeShape(newNextPoint, touch: touch, pointView: pointView, region: this)
            }
            newPreviousPoint.dragCallBack = { [unowned self] (pointView: PointView, touch:UITouch) -> Void in
                self.onDragCallChangeShape(newPreviousPoint, touch: touch, pointView: pointView, region: this)
            }
            newNextPoint.touchEndCallback = { [unowned self] (pointView: PointView) -> Void in
                this.changeshape()
                if(self._pointViewArray.count <= self.MAX_ANCHORS){
                    self.addDots(point, this: this)
                }
                print("n")
            }
            newPreviousPoint.touchEndCallback = { [unowned self] (pointView: PointView) -> Void in
                this.changeshape()
                if(self._pointViewArray.count <= self.MAX_ANCHORS){
                    self.addDots(point, this: this)
                }
                print("p")
            }
        }else{
            return;
        }
        this.changeshape()
        self.remove()
        self.draw()
    }
    
    
    
    
    func getMidPoint(_ point1:CGPoint, point2:CGPoint) -> CGPoint{
        return CGPoint(x: (point1.x + point2.x) / 2, y: (point1.y + point2.y) / 2)
    }
    
    @objc func changeColor(_ notification: Notification) {
        if let place = notification.userInfo?["place"] as? Int{
            if place == 0{
                if let value = notification.userInfo?["value"] as? Int{
                    if value == 1{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
                        }
                        primary = UIColor.lightGray.withAlphaComponent(0.7)
                        _shapeLayer.strokeColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
                        _shapeLayer.fillColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
                    }else if value == 2{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.yellow.withAlphaComponent(0.7)
                        }
                        primary = UIColor.yellow.withAlphaComponent(0.7)
                        _shapeLayer.strokeColor = UIColor.yellow.withAlphaComponent(0.7).cgColor
                        _shapeLayer.fillColor = UIColor.yellow.withAlphaComponent(0.7).cgColor
                    }else{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.red.withAlphaComponent(0.7)
                        }
                        primary = UIColor.red.withAlphaComponent(0.7)
                        _shapeLayer.strokeColor = UIColor.red.withAlphaComponent(0.7).cgColor
                        _shapeLayer.fillColor = UIColor.red.withAlphaComponent(0.7).cgColor
                    }
                }
            }else if place == 1{
                if let value = notification.userInfo?["value"] as? Int{
                    if value == 1{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.lightGray.withAlphaComponent(0.7)
                        }
                        primary = UIColor.lightGray.withAlphaComponent(0.7)
                        _shapeLayer.strokeColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
                        _shapeLayer.fillColor = UIColor.lightGray.withAlphaComponent(0.7).cgColor
                    }else if value == 2{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.yellow.withAlphaComponent(0.7)
                        }
                        primary = UIColor.yellow.withAlphaComponent(0.7)
                        _shapeLayer.strokeColor = UIColor.yellow.withAlphaComponent(0.7).cgColor
                        _shapeLayer.fillColor = UIColor.yellow.withAlphaComponent(0.7).cgColor
                    }else{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.red.withAlphaComponent(0.7)
                        }
                        primary = UIColor.red.withAlphaComponent(0.7)
                        _shapeLayer.strokeColor = UIColor.red.withAlphaComponent(0.7).cgColor
                        _shapeLayer.fillColor = UIColor.red.withAlphaComponent(0.7).cgColor
                    }
                }
                
            }else {
                if let value = notification.userInfo?["value"] as? Int{
                    if value == 1{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.lightGray.withAlphaComponent(0.9)
                        }
                        primary = UIColor.lightGray.withAlphaComponent(0.9)
                        _shapeLayer.strokeColor = UIColor.lightGray.withAlphaComponent(0.9).cgColor
                        _shapeLayer.fillColor = UIColor.lightGray.withAlphaComponent(0.9).cgColor
                    }else if value == 2{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.yellow.withAlphaComponent(0.9)
                        }
                        primary = UIColor.yellow.withAlphaComponent(0.9)
                        _shapeLayer.strokeColor = UIColor.yellow.withAlphaComponent(0.9).cgColor
                        _shapeLayer.fillColor = UIColor.yellow.withAlphaComponent(0.9).cgColor
                    }else{
                        for point in _pointViewArray{
                            point.backgroundColor = UIColor.red.withAlphaComponent(0.9)
                        }
                        primary = UIColor.red.withAlphaComponent(0.9)
                        _shapeLayer.strokeColor = UIColor.red.withAlphaComponent(0.9).cgColor
                        _shapeLayer.fillColor = UIColor.red.withAlphaComponent(0.9).cgColor
                    }
                }
                
            }
        }
    }
    
    func draw(){
        for pointView in _pointViewArray{
            context.addSubview(pointView)
            print(pointView.center)
        }
        if let pontViewFirst = _pointViewArray.first{
            self.move(to: pontViewFirst.center)
            addline()
            _shapeLayer = CAShapeLayer()
            _shapeLayer.strokeColor = primary.cgColor
            _shapeLayer.fillColor = primary.cgColor
            _shapeLayer.lineWidth = 1
            _shapeLayer.path = self.cgPath
            _shapeLayer.lineCap = kCALineCapRound
            _shapeLayer.sublayers?.removeAll()
            context.layer.addSublayer(_shapeLayer)
        }
    }
    
    func remove(){
        for pointView in _pointViewArray{
            pointView.removeFromSuperview()
        }
        self.removeAllPoints()
        // _pointViewArray.removeAll()
        _shapeLayer.removeFromSuperlayer()
    }
    
    func changeshape(){
        self.removeAllPoints()
        self.move(to: _pointViewArray.first!.center)
        addline()
        _shapeLayer.path = self.cgPath
    }
    
    func coordinatesOfPoint() -> AnyObject{
        var json:AnyObject
        let screenWidth = self.context.frame.size.width
        let screenHeight = self.context.frame.size.height
        var anchorArr = [[String:AnyObject]]()
        let resolutionObj = ["x":screenWidth,"y":screenHeight]
        print(resolutionObj)
        var minX = 1000000.0
        var minY = 1000000.0
        var maxX = 0.0
        var maxY = 0.0
        
        if !_pointViewArray.isEmpty{
            for i in 0..<_pointViewArray.count{
                if(Double(_pointViewArray[i].center.y) < minY){
                    minY = Double(_pointViewArray[i].center.y)
                }
                if(Double(_pointViewArray[i].center.x) < minX){
                    minX = Double(_pointViewArray[i].center.x)
                }
                if(Double(_pointViewArray[i].center.y) > maxY){
                    maxY = Double(_pointViewArray[i].center.y)
                }
                if(Double(_pointViewArray[i].center.x) > maxX){
                    maxX = Double(_pointViewArray[i].center.x)
                }
                if _pointViewArray[i].type == PointView.PointType.permanent{
                    print("savecoor",_pointViewArray[i].center.x,_pointViewArray[i].center.y)
                    anchorArr.append(["x":_pointViewArray[i].center.x as AnyObject,"y":_pointViewArray[i].center.y as AnyObject,"type":0 as AnyObject])
                }else if _pointViewArray[i].type == PointView.PointType.temp{
                    print("savecoor",_pointViewArray[i].center.x,_pointViewArray[i].center.y)
                    anchorArr.append(["x":_pointViewArray[i].center.x as AnyObject,"y":_pointViewArray[i].center.y as AnyObject,"type":1 as AnyObject])
                }
            }
        }
        json = ["anchors":anchorArr,"resolution":resolutionObj,"bounds":[["x":minX, "y":minY],["x":maxX, "y":maxY]]] as AnyObject
        return json
    }
    
    func addline(){
        for point in _pointViewArray{
            self.addLine(to: point.center)
        }
        self.close()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
