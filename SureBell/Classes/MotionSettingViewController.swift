//
//  MotionSettingViewController.swift
//  SureBell
//
//  Created by PiOctave on 26/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import UIKit

 struct Anchors {
    var x:Double?
    var y:Double?
    var type:Int?
}
 class RegionModel{
    
    var resolution_x:Int?
    var resolution_y:Int?
    var anchors = [Anchors]()
    
    required init(resolution_x:Int?,resolution_y:Int?,anchors:[Anchors]?){
        if let res_x = resolution_x, let res_y = resolution_y, let anchors = anchors{
            self.resolution_x = res_x
            self.resolution_y = res_y
            self.anchors = anchors
        }
    }
}


open class MotionSettingViewController: UIViewController {

    var regionContainerView: UIView!
    var motionLabel: UILabel!
    var backgroundImage: UIImageView!
    
    var _camera: UIButton?
    var isTouchingRegion:Region?
    var isTouchingRegionCopy:Region?
    var regions = [Region]()
    var tempx:CGFloat!
    var tempy:CGFloat!
    var z = [Region]()
    var motionSensitivity = 2
    public var device:Device?
    var resionSave:RegionModel?
    var loaderView: Loader!
    var deviceId = ""
    var deviceName = ""
    
    convenience public init(_ device: Device){
        self.init()
        self.device = device
    }
    
    public var camera : UIButton{
        if let currentImage = _camera{
            return currentImage
        }
        let button = UIButton()
        button.frame = CGRect(x: 60, y: 200, width: 200, height: 150)
        button.isUserInteractionEnabled = true
        button.contentMode = .scaleAspectFit
        button.backgroundColor = UIColor.groupTableViewBackground
        button.layer.cornerRadius = 19.0
        button.addTarget(self, action:#selector(self.cameraA), for: .touchUpInside)
        _camera = button
        return button
    }
    
    @objc func cameraA(sender: UIButton!) {
        if let device = self.device{
            self.showLoader()
            Device.instance.getTurnServerValues(callback: { (turnList) in
                DispatchQueue.main.async {
                    self.hideLoader()
                    let live = LiveViewController(turnList, device: device, sessionKey: Utils.randomStringWithLength(15), callFrom: "Motion")
                    self.present(live, animated: false, completion: nil)
                }
            }) { (error) in
                DispatchQueue.main.async {
                    self.hideLoader()
                }
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                    self.showAlert(title: "Alert", message: des)
                }else{
                    guard let error = error as? Error else{return}
                    print(error)
                    self.showAlert(title: "Alert", message: error.localizedDescription)
                }
            }
        }else{
            print("Device Object not found")
        }
    }
    
    var _save: UIButton?
    public var save : UIButton{
        if let currentImage = _save{
            return currentImage
        }
        let button = UIButton()
        button.frame = CGRect(x: 60, y: 200, width: 200, height: 150)
        button.isUserInteractionEnabled = true
        button.contentMode = .scaleAspectFit
        button.backgroundColor = UIColor.groupTableViewBackground
        button.layer.cornerRadius = 19.0
        button.addTarget(self, action:#selector(self.saveA), for: .touchUpInside)
        _save = button
        return button
    }
    
    @objc func saveA(sender: UIButton!) {
        if let device = self.device{
        var regionsArr = [AnyObject]()
        for reg in regions{
            regionsArr.append(reg.coordinatesOfPoint())
        }
        let json = ["motionSettings":regionsArr]
        if let deviceID = device._id{
            DispatchQueue.main.async {
                self.showLoader()
            }
            DeviceHttps.instance.motionSettings(id: deviceID, motionSettings: json, callback: { (device) in
                DispatchQueue.main.async {
                    self.hideLoader()
                    let alertController = UIAlertController(
                        title: "Alert", message: "Setting saved successfully.", preferredStyle: .alert)
                    let defaultAction = UIAlertAction(
                        title: "Back", style: .default, handler: { action in
                        self.dismiss(animated: false, completion: nil)
                    })
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }) { (error) in
                DispatchQueue.main.async {
                    self.hideLoader()
                }
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    print(des)
                    self.showAlert(title: "Alert", message: des)
                }else{
                    guard let error = error as? Error else{return}
                    print(error)
                    self.showAlert(title: "Alert", message: error.localizedDescription)
                }
            }
        }
        }
    }
    
    
    var _back: UIButton?
    public var back : UIButton{
        if let currentImage = _back{
            return currentImage
        }
        let button = UIButton()
        button.frame = CGRect(x: 60, y: 200, width: 200, height: 150)
        button.isUserInteractionEnabled = true
        button.contentMode = .scaleAspectFit
        button.backgroundColor = UIColor.groupTableViewBackground
        button.layer.cornerRadius = 3.0
        button.setTitle("Back", for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.6).cgColor
        button.setTitleColor(UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.75), for: .normal)
        button.addTarget(self, action:#selector(self.backA), for: .touchUpInside)
        _back = button
        return button
    }
    
    
    @objc func backA(sender: UIButton!) {
        self.dismiss(animated: false, completion: nil)
    }
    
    var _trash: UIButton?
    public var trash : UIButton{
        if let currentImage = _trash{
            return currentImage
        }
        let button = UIButton()
        button.frame = CGRect(x: 60, y: 200, width: 200, height: 150)
        button.isUserInteractionEnabled = true
        button.contentMode = .scaleAspectFit
        button.backgroundColor = UIColor.groupTableViewBackground
        button.layer.cornerRadius = 19.0
        button.addTarget(self, action:#selector(self.trashA), for: .touchUpInside)
        _trash = button
        return button
    }
    
    
    @objc func trashA(sender: UIButton!) {
        clearmotionWindow()
    }
    
    var _add: UIButton?
    public var add : UIButton{
        if let currentImage = _add{
            return currentImage
        }
        let button = UIButton()
        button.frame = CGRect(x: 60, y: 200, width: 200, height: 150)
        button.isUserInteractionEnabled = true
        button.contentMode = .scaleAspectFit
        button.backgroundColor = UIColor.groupTableViewBackground
        button.layer.cornerRadius = 19.0
        button.addTarget(self, action:#selector(self.addA), for: .touchUpInside)
        _add = button
        return button
    }
    
    @objc func addA(sender: UIButton!) {
        addmotionWindow()
    }
    
    
    var customSC:UISegmentedControl!
    
    override open  func viewDidLoad() {
        super.viewDidLoad()

        self.backgroundImage = UIImageView()
        self.backgroundImage.frame = self.view.bounds
        self.view.addSubview(self.backgroundImage)
        
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        backgroundImage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        backgroundImage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        backgroundImage.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        backgroundImage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        
        self.regionContainerView = UIView()
        self.regionContainerView.frame = self.view.bounds
        self.view.addSubview(self.regionContainerView)
        
        regionContainerView.translatesAutoresizingMaskIntoConstraints = false
        regionContainerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        regionContainerView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        regionContainerView.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        regionContainerView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        
        self.view.addSubview(back)
        
        back.translatesAutoresizingMaskIntoConstraints = false
        back.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        back.widthAnchor.constraint(equalToConstant: 65.0).isActive = true
        back.heightAnchor.constraint(equalToConstant: 33.0).isActive = true
        back.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        back.transform = CGAffineTransform(translationX: 20.0, y: 20.0)
       
        let vectors = VectorICONS()
        vectors.save.frame.origin.x = self.save.bounds.origin.x + 12
        vectors.save.frame.origin.y = self.save.bounds.origin.y + 12
        self.save.layer.addSublayer(vectors.save)
        
        vectors.camera.frame.origin.x = self.camera.bounds.origin.x + 11
        vectors.camera.frame.origin.y = self.camera.bounds.origin.y + 12.5
        self.camera.layer.addSublayer(vectors.camera)
        
        vectors.plus.frame.origin.x = self.add.bounds.origin.x + 10.5
        vectors.plus.frame.origin.y = self.add.bounds.origin.y + 10.5
        self.add.layer.addSublayer(vectors.plus)
        
        vectors.deleteButton.frame.origin.x = self.trash.bounds.origin.x + 12
        vectors.deleteButton.frame.origin.y = self.trash.bounds.origin.y + 8.5
        self.trash.layer.addSublayer(vectors.deleteButton)
        
        let stackView   = UIStackView()
        stackView.axis  = UILayoutConstraintAxis.vertical
        stackView.distribution  = UIStackViewDistribution.fillEqually
        stackView.alignment = UIStackViewAlignment.fill
        stackView.spacing   = 14.0
        
        
        
        stackView.addArrangedSubview(camera)
        stackView.addArrangedSubview(save)
        stackView.addArrangedSubview(trash)
        stackView.addArrangedSubview(add)
        
        self.view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        stackView.widthAnchor.constraint(equalToConstant: 45.0).isActive = true
        stackView.heightAnchor.constraint(equalToConstant: 220.0).isActive = true
        stackView.transform = CGAffineTransform(translationX: -20.0, y: -30.0)
     
        let items = ["LOW", "MID", "HIGH"]
        customSC = UISegmentedControl(items: items)
        customSC.selectedSegmentIndex = 0
       
        customSC.layer.cornerRadius = 5.0
        customSC.backgroundColor = UIColor.groupTableViewBackground
        customSC.tintColor = UIColor(red: 63/255, green: 14/255, blue: 79/255, alpha: 1.0).withAlphaComponent(0.8)
    
        customSC.addTarget(self, action: #selector(change(sender:)), for: .valueChanged)
    
        self.view.addSubview(customSC)
        
        customSC.translatesAutoresizingMaskIntoConstraints = false
        
        customSC.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        customSC.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        customSC.widthAnchor.constraint(equalToConstant: 180.0).isActive = true
        customSC.heightAnchor.constraint(equalToConstant: 33.0).isActive = true
        customSC.transform = CGAffineTransform(translationX: 15.0, y: -20.0)
        
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
        self.parseMotionLevel()
        self.parseMotionSetting()
        
        self.loaderView = Loader()
        self.view.addSubview(self.loaderView)

        loaderView.translatesAutoresizingMaskIntoConstraints = false
        loaderView.heightAnchor.constraint(equalToConstant: 50.0).isActive = true
        loaderView.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        loaderView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        loaderView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
     
        self.loaderView.tintColor = UIColor.white
        self.loaderView.isHidden = true
        
    }

    func showLoader(){
         self.loaderView.startAnimating()
         self.loaderView.isHidden = false
    }
    
    func hideLoader(){
        self.loaderView.stopAnimating()
        self.loaderView.isHidden = true
    }
    
    @objc func change(sender: UISegmentedControl) {
        print("Changing Color to ")
        switch sender.selectedSegmentIndex {
        case 0:
            let info = ["place":0,"value":1]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ROI_COLOR"), object: nil,userInfo:info)
            self.motionSensitivity = 1
           
        case 1:
            let info = ["place":0,"value":2]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ROI_COLOR"), object: nil,userInfo:info)
            self.motionSensitivity = 2
        default:
            let info = ["place":0,"value":3]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ROI_COLOR"), object: nil,userInfo:info)
            self.motionSensitivity = 3
        }
        if let device = self.device, let id = device._id{
            DispatchQueue.main.async {
                self.showLoader()
            }
        Device.instance.motionSensitivity(id: id, motionSensitivity: self.motionSensitivity, callback: { (device) in
            DispatchQueue.main.async {
                self.hideLoader()
            }
        }) { (error) in
            DispatchQueue.main.async {
                self.hideLoader()
            }
            if let error =  error as? APIError{
                guard  let des = error.error_description else{return}
                print(des)
                self.showAlert(title: "Alert", message: des)
            }else{
                guard let error = error as? Error else{return}
                print(error)
                self.showAlert(title: "Alert", message: error.localizedDescription)
            }
        }
        }
    }
    
    override open func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
    }
    
    
    
    override open func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
            UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        self.tabBarController?.tabBar.isHidden = false
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.landscapeRight
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return UIInterfaceOrientation.landscapeRight
    }
  
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            if touch.view != self.regionContainerView {
                return
            }
            let position :CGPoint = touch.location(in: self.regionContainerView)
            let location:CGPoint = (touch as! UITouch).location(in: self.regionContainerView)
            for j in 0  ..< regions.count {
                if (regions[j]._shapeLayer.path?.contains(location))!{
                    isTouchingRegion = regions[j]
                    let info = ["place":2,"value":self.motionSensitivity]
                    NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ROI_COLOR"), object: nil,userInfo:info)
                    z = (regions.map { return Region( region: $0 ) })
                    isTouchingRegionCopy = z[j]
                    tempx = position.x
                    tempy = position.y
                }
            }
        }
    }
    
    override open func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let position :CGPoint = touch.location(in: self.regionContainerView)
            let location:CGPoint = (touch as! UITouch).location(in: self.regionContainerView)
            if isTouchingRegion != nil && isTouchingRegionCopy != nil{
                if position.x <= 0{
                    print(position.x,position.y)
                }
                if isTouchingRegion!._shapeLayer.path!.contains(location)
                {
                    let dx = position.x - self.tempx
                    let dy = position.y - self.tempy
                    switch (getDirectionForOutOfBounds(dx, dy: dy)) {
                    case 0:
                        for i in 0  ..< isTouchingRegion!._pointViewArray.count  {
                            isTouchingRegion!._pointViewArray[i].center.x = isTouchingRegionCopy!._pointViewArray[i].center.x + dx
                            isTouchingRegion!._pointViewArray[i].center.y = isTouchingRegionCopy!._pointViewArray[i].center.y + dy
                        }
                        break;
                    case 2:
                        for i in 0  ..< isTouchingRegion!._pointViewArray.count  {
                            isTouchingRegion!._pointViewArray[i].center.y = isTouchingRegionCopy!._pointViewArray[i].center.y + dy
                        }
                        break;
                    case 3:
                        for i in 0  ..< isTouchingRegion!._pointViewArray.count  {
                            isTouchingRegion!._pointViewArray[i].center.x = isTouchingRegionCopy!._pointViewArray[i].center.x + dx
                        }
                        break;
                    default: break
                    }
                }
                isTouchingRegion!.changeshape()
            }
        }
    }
    
    
    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isTouchingRegion = nil
        isTouchingRegionCopy = nil
    }
    
    func parseMotionLevel()  {
        if let device = self.device{
            if let id = device._id{
                self.deviceId = id
            }
            if let name = device.name{
                self.deviceName = name
            }
                if let settings = device.settings{
                    if let motionSensitivityValue = settings.motionSensitivity{
                        self.motionSensitivity = motionSensitivityValue
                        DispatchQueue.main.async {
                            if self.motionSensitivity == 1{
                                self.customSC.selectedSegmentIndex = 0
                            }else if self.motionSensitivity == 2{
                                self.customSC.selectedSegmentIndex = 1
                            }else{
                                self.customSC.selectedSegmentIndex = 2
                            }
                            let info = ["place":2,"value":self.motionSensitivity]
                            NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ROI_COLOR"), object: nil,userInfo:info)
                        }
                    }
                }
        }
    }
    
    func parseMotionSetting(){
        if let device = self.device{
            if let imgUrlString = device.lastImage{
                if let imgUrl = URL(string: imgUrlString){
                    DispatchQueue.global().async{
                        do {
                             let data = try Data(contentsOf: imgUrl)
                                DispatchQueue.main.async {
                                    self.backgroundImage.image = UIImage(data: data)
                                }
                        }catch {}
                    }
                }
            }
                if let settings = device.settings?.motionSettings{
                    var anchors = [Anchors]()
                    for setting in settings{
                        for anchor in setting.anchors!{
                            anchors.append(Anchors(x: anchor.x!, y: anchor.y!, type: anchor.type!))
                        }
                        resionSave = RegionModel(resolution_x: setting.resolution?.x!, resolution_y: setting.resolution?.y!,anchors: anchors)
                        setViewonWindow()
                    }
                }
        }
    }
    
    func setViewonWindow(){
        var anchorArray = [Anchor]()
        let frameHeight = self.view.frame.size.height
        let frameWidth = self.view.frame.size.width
        
        for anchor in (resionSave?.anchors)!{
            let x_co:Double = Double(anchor.x!) * (Double(frameHeight) / Double(resionSave!.resolution_x!))
            let y_co:Double = Double(anchor.y!) * (Double(frameWidth) / Double(resionSave!.resolution_y!))
            print("res",frameHeight,frameWidth)
            print("coor",x_co,y_co,resionSave!.resolution_x!,resionSave!.resolution_y!,anchor.x!,anchor.y!)
            anchorArray.append(Anchor(point: CGPoint(x: x_co , y: y_co), type: getType(anchor.type!)))
        }
        let newRegion = Region(anchors: anchorArray, context: self.regionContainerView, bgImage: self.backgroundImage)
        regions.append(newRegion)
        newRegion.draw()
    }
    
    func getType(_ val:Int)-> PointView.PointType{
        if val == 1{
            return PointView.PointType.temp
        }else{
            return PointView.PointType.permanent
        }
    }
    
    func getDirectionForOutOfBounds(_ dx:CGFloat, dy:CGFloat) -> Int{
        var isX = false
        var isY = false
        for i in 0  ..< isTouchingRegion!._pointViewArray.count  {
            let isOutOfBoundInX = (isTouchingRegionCopy!._pointViewArray[i].center.x + dx) > self.regionContainerView.frame.width || (isTouchingRegionCopy!._pointViewArray[i].center.x + dx) < 0;
            let isOutOfBoundInY = (isTouchingRegionCopy!._pointViewArray[i].center.y + dy) > self.regionContainerView.frame.height || (isTouchingRegionCopy!._pointViewArray[i].center.y + dy) < 0;
            if (isOutOfBoundInX && isOutOfBoundInY) {
                return 1;
            }
            if (isOutOfBoundInX) {
                isX = true;
            } else if (isOutOfBoundInY) {
                isY = true;
            }
        }
        if (isX && isY){
            return 1;
        }else if(isX){
            return 2;
        }else if(isY){
            return 3;
        }
        return 0;
    }
    
    func isOutOfBound(_ x:CGFloat, y:CGFloat) -> Bool{
        return x <= 0 || y <= 0 || x >= self.regionContainerView.frame.width || y >= self.regionContainerView.frame.height
    }
    
    
    func clearmotionWindow(){
        if regions.count > 0{
            let region = regions.popLast()
            region!.remove()
        }
    }
    
    func addmotionWindow(){
        if regions.count < 1{
            var anchorArray = [Anchor]()
            anchorArray.append(Anchor(point: CGPoint(x: 150 , y: 150), type: PointView.PointType.permanent))
            anchorArray.append(Anchor(point: CGPoint(x: 300 , y: 150), type: PointView.PointType.permanent))
            anchorArray.append(Anchor(point: CGPoint(x: 300 , y: 300), type: PointView.PointType.permanent))
            anchorArray.append(Anchor(point: CGPoint(x: 150 , y: 300), type: PointView.PointType.permanent))
            let newRegion = Region(anchors: anchorArray, context: self.regionContainerView, bgImage: self.backgroundImage)
            regions.append(newRegion)
            newRegion.draw()
            let info = ["place":2,"value":self.motionSensitivity]
            NotificationCenter.default.post(name: Notification.Name(rawValue: "UPDATE_ROI_COLOR"), object: nil,userInfo:info)
        }else{
            let alertController = UIAlertController(
                title: "Alert", message: "Only one ROI is supported yet.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(
                title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func showAlert(title:String, message:String){
        DispatchQueue.main.async {
            let alertController = UIAlertController(
                title: title, message: message, preferredStyle: .alert)
            let defaultAction = UIAlertAction(
                title: "OK", style: .default, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }

}
