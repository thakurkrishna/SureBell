//
//  PhoneRTCPlugin.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import AVFoundation
import WebRTC
import SwiftyJSON


@objc(PhoneRTCPlugin)
public class PhoneRTCPlugin : NSObject {
    public var sessions = [String: Session]()
    public var peerConnectionFactory: RTCPeerConnectionFactory?
    public var videoConfig: VideoConfig?
    public var videoCapturer: RTCDeviceType?
    public var videoSource: RTCAVFoundationVideoSource?
    public var localVideoView: RTCEAGLVideoView?
    public var remoteView: RTCEAGLVideoView?
    public var localView: RTCCameraPreviewView?
    public var remoteVideoViews = [VideoTrackViewPair]()
    public var camera: String?
    public var localVideoTrack: RTCVideoTrack?
    public var localAudioTrack: RTCAudioTrack?
    public var accessToken:String?
    
    public func pluginInitialize(_ accessToken:String,remoteView:RTCEAGLVideoView?,localView:RTCCameraPreviewView?) {
        self.sessions = [:];
        self.remoteVideoViews = [];
        self.accessToken = accessToken
        self.remoteView = remoteView
        self.localView = localView
        peerConnectionFactory = RTCPeerConnectionFactory()
        RTCPeerConnectionFactory.initialize()
    }
    
    
    public func setVideoViews(_ remoteView:RTCEAGLVideoView?,localView:RTCCameraPreviewView?){
        DispatchQueue.main.async {
            self.remoteView = remoteView
            self.localView = localView
        }
    }
    
    
    public func setSignallingDelegate(_ session_key:String, signallingChannelDelegate:SignallingChannel){
        if self.sessions.isEmpty{
            DispatchQueue.main.async {
                print("⚠️ Session not available")
            }
        }else{
            if sessions[session_key] != nil{
                DispatchQueue.global().async() {
                    if let session = self.sessions[session_key] {
                        session.setSignallingDelegate(signallingChannelDelegate: signallingChannelDelegate)
                    }
                }
            }
        }
    }
    
    
    public func sosCall(_ session_key:String){
        if self.sessions.isEmpty{
            DispatchQueue.main.async {
                print("⚠️ Session not available")
            }
        }else{
            if sessions[session_key] != nil{
                DispatchQueue.global().async() {
                    if let session = self.sessions[session_key] {
                        session.sosCall()
                    }
                }
            }
        }
    }
    

    public func createSessionObject(_ session_key:String?,config: SessionConfig,isLocal:Bool,isSDESSupported:Bool, callInItType: String) {
        if let sessionKey = session_key{
            // create a session and initialize it.
            let config = config
            if let peerConnectionFactory = self.peerConnectionFactory{
                let session = Session(plugin: self,
                                      peerConnectionFactory: peerConnectionFactory,
                                      config: config,
                                      sessionKey: sessionKey, isLocal: isLocal,isSDESSupported:isSDESSupported, callInItType: callInItType)
                sessions[sessionKey] = session
            }else{
                print("PHONE_RTC_ERROR: peerConnectionFactory nil")
            }
        }else{
            print("PHONE_RTC_ERROR: SESSION KEY NIL")
        }
    }
    
    
    public func crateSessionAddressList(session_key:String,turnAddressList:Any?){
        if self.sessions.isEmpty{
            DispatchQueue.main.async {
                print("⚠️ Session not available")
            }
        }else{
            if sessions[session_key] != nil{
                DispatchQueue.global().async() {
                    if let session = self.sessions[session_key] {
                        session.setTurnAddressList(turnAddressList: turnAddressList)
                    }
                }
            }
        }
    }
    
    
    public func sendMediaSignal(session_key:String,config: SessionConfig){
        print(sessions)
        if self.sessions.isEmpty{
            DispatchQueue.main.async {
                print("⚠️ Session not available")
            }
        }else{
            if sessions[session_key] != nil{
                DispatchQueue.global().async() {
                    if let session = self.sessions[session_key] {
                        session.config = config
                        session.sendMediaSignal()
                    }
                }
            }
        }
    }
    
    
    public func call(session_key:String, to:String, type:String) {
        DispatchQueue.main.async {
            if self.sessions.isEmpty{
                print("⚠️ call Session not available")
            }else{
                if self.sessions[session_key] != nil{
                    if let session = self.sessions[session_key] {
                        session.call(to,type: type)
                    }
                }
            }
        }
    }
    
    
    public func receiveMessage(session_key:String, message:String?) {
        if let msg = message {
            if self.sessions.isEmpty{
                DispatchQueue.main.async {
                    print("⚠️ Session not available")
                }
            }else{
                if sessions[session_key] != nil{
                    if let session = sessions[session_key] {
                        DispatchQueue.global().async() {
                            session.receiveMessage(msg)
                        }
                    }
                }
            }
        }
    }
    
    
    public func renegotiate(session_key:String,config: SessionConfig) {
        DispatchQueue.main.async {
            if self.sessions.isEmpty{
                print("⚠️ Session not available")
            }else{
                if self.sessions[session_key] != nil{
                    if let session = self.sessions[session_key] {
                        session.config = config
                        session.createOrUpdateStream()
                    }
                }
            }
        }
    }
    

    public func muteAudio(session_key:String, isAudio:Bool){
        print("sessionKey",session_key)
        DispatchQueue.global().async() {
            if self.sessions.isEmpty{
                DispatchQueue.main.async {
                    print("⚠️ Session not available")
                }
            }else{
                if self.sessions[session_key] != nil{
                    if let session = self.sessions[session_key]{
                        session.muteAudio(isAudio: isAudio)
                    }
                }
            }
        }
    }
    
    
    public func disconnect(session_key:String) {
        print(sessions)
        print("PHONERTC DISSCONNECT EMPTY")
        DispatchQueue.global().async() {
            if self.sessions.isEmpty{
                DispatchQueue.main.async {
                    print("⚠️ Session not available")
                }
            }else{
                if self.sessions[session_key] != nil{
                    if let session = self.sessions[session_key]{
                        session.disconnect(true)
                    }
                }
            }
        }
    }
    
    public func initLocalAudioTrack() {
        if let peerConnectionFactory = self.peerConnectionFactory{
            localAudioTrack = peerConnectionFactory.audioTrack(withTrackId: "ARDAMSa0")
        }else{
            print("PHONE_RTC_ERROR: peerConnectionFactory nil")
        }
    }
    
    
    public func addRemoteVideoTrack(_ videoTrack: RTCVideoTrack) {
        if let remote = self.remoteView{
            DispatchQueue.main.async {
                videoTrack.add(remote)
                self.remoteVideoViews.append(VideoTrackViewPair(videoView: remote, videoTrack: videoTrack))
                self.refreshVideoContainer()
            }
        }else{
            DispatchQueue.main.async {
                print("⚠️ Remote View not available")
            }
            return
        }
    }
    
    
    public func removeRemoteVideoTrack(_ videoTrack: RTCVideoTrack) {
        DispatchQueue.main.async {
            for i in 0 ..< self.remoteVideoViews.count {
                let pair = self.remoteVideoViews[i]
                if pair.videoTrack == videoTrack {
                    pair.videoView.isHidden = true
                    pair.videoView.removeFromSuperview()
                    self.remoteVideoViews.remove(at: i)
                    self.refreshVideoContainer()
                    return
                }
            }
        }
    }
    
    
    public func refreshVideoContainer() {
        if let remote = self.remoteView{
            var videoViewIndex = 0
            for _ in 0 ..< self.remoteVideoViews.count  {
                let pair = self.remoteVideoViews[videoViewIndex]
                pair.videoView.frame = remote.bounds
                pair.videoView.contentMode = UIViewContentMode.scaleAspectFill
                videoViewIndex = videoViewIndex + 1
            }
        }else{
            DispatchQueue.main.async {
                print("⚠️ Remote View not available")
            }
            return
        }
    }
    
    
    public func onSessionDisconnect(_ sessionKey: String) {
        print("Session removed")
        if let local = self.localVideoView{
            sessions.removeValue(forKey: sessionKey)
            if sessions.count == 0 {
                local.isHidden = true
                local.removeFromSuperview()
                self.localVideoView = nil
                self.localVideoTrack = nil
                self.localAudioTrack = nil
                self.videoSource = nil
                self.videoCapturer = nil
            }
        }else{
            DispatchQueue.main.async {
                //  Toast(text:"Local View not available").show()
            }
        }
    }
    
}
public struct VideoTrackViewPair {
    var videoView: RTCEAGLVideoView
    var videoTrack: RTCVideoTrack
}

