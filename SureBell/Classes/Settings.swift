//
//  File.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import ObjectMapper


/// This Settings class is an instance of Device module, which helps user to do settings for their device.
/// Using this user can do settings for **chime**, **motionSensitivity**, **cloudUpload**, **cloudUploadStatus**,
/// **video**, **audio**, **motionSettings**, **cloudPlan** and **timeZone** of their device.
///
public class Settings:Mappable {
    public var chime : Bool?
    public var motionSensitivity : Int?
    public var cloudUpload : Bool?
    public var cloudUploadStatus : Bool?
    public var video : Video?
    public var audio : Audio?
    public var motionSettings : Array<MotionSettings>?
    public var cloudPlan : CloudPlan?
    public var timeZone : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self.chime <- map["chime"]
        self.motionSensitivity <- map["motionSensitivity"]
        self.cloudUpload <- map["cloudUpload"]
        self.cloudUploadStatus <- map["cloudUploadStatus"]
        self.video <- map["video"]
        self.audio <- map["audio"]
        self.motionSettings <- map["motionSettings"]
        self.cloudPlan <- map["cloudPlan"]
        self.timeZone <- map["timeZone"]
    }
    
}

