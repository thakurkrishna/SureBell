//
//  DataGramSignallingChannelDelegate.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import WebRTC
import UIKit
import CocoaAsyncSocket
import SwiftyJSON

class DataGramSignallingChannelDelegate:NSObject,SignallingChannel,GCDAsyncUdpSocketDelegate {
    
    func sendEndOfCandidate(_ callId: String,  to: String, type: String, i: Int,turnAddressList:Any?) {
        print("%%%%%%%%%%%%%%%%call id", callId)
        let json = ["id": callId,
                    "to":["id":to,"type":type],
                    "type": "CALL",
                    "from": ["signal": "end_of_candidate","seqNo":i,"timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        print("ENDOFCANDIDATE",json)
        sendMessage(json as AnyObject)
    }
    
    
    func sendMediaSignal(_ callId:String,  to: String, type: String, i:Int,turnAddressList:Any?) {
        let jsonSignal = ["id": callId,
                          "to": ["id":to,"type":type],
                          "type": "CALL",
                          "from": ["signal": "start_media","seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        print("start_media",jsonSignal)
        sendMessage(jsonSignal as AnyObject)
    }
    
    
    func sendCandidate(_ id: String,  to: String, type: String, candidate: RTCIceCandidate?, i: Int,turnAddressList:Any?) {
        if candidate == nil {
            sendEndOfCandidate(id,i: i, turnAddressList:turnAddressList)
            return
        }
        
        var candidateDict = NSDictionary()
        candidateDict = [
            "candidate":candidate!.sdp ,
            "sdpMid":candidate!.sdpMid!,
            "sdpMLineIndex": Int64(candidate!.sdpMLineIndex)
        ]
        let json = ["id":id,
                    "to":["id":to,"type":type],
                    "type":"CALL",
                    "from":["candidate":candidateDict,"seqNo":i,
                            "timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios"
            ]
            ] as [String : Any]
        print("SENDCANDIDATE", json)
        sendMessage(json as AnyObject)
    }
    
    
    func sendSdp(_ id: String,  to: String, type: String, sdp: RTCSessionDescription,turnAddressList:Any?) {
        let json = ["id":"\(id)",
            "to":["id":to,"type":type],
            "type":"CALL",
            "from":["sdp":
                [   "sdp":sdp.sdp,
                    "type":RTCSessionDescription.string(for: sdp.type)],"timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        print("SENDSDPMESSAGSDP",json)
        sendMessage(json as AnyObject)
        
    }
    
    
    fileprivate var rtcSignalhandler:RTCSignalHandler!
    fileprivate var isReady = false
    fileprivate var disconnected = false
    fileprivate var messageQueue = [AnyObject]()
    var ip = ""
    var port:NSNumber = -1
    var isCallDisconnected = false
    
    var socket:GCDAsyncUdpSocket!
    
    init(access_token:String,medium_id:String,uri:String,rtcSignalhandler:RTCSignalHandler){
        super.init()
        let url = URL(fileURLWithPath: uri)//udp://192.168.20.52:15000
        if let urlHost = url.host{
            self.ip = urlHost
        }
        if let urlPort = (url as NSURL).port{
            self.port = urlPort
        }
        self.rtcSignalhandler = rtcSignalhandler
        socket = GCDAsyncUdpSocket(delegate: self, delegateQueue: DispatchQueue.main)
        do {
            try socket.connect(toHost: ip, onPort: UInt16(port.int16Value))
        }catch{
            
        }
    }
    
    init(access_token:String,medium_id:String,uri:String){
        //self.init(access_token: access_token,medium_id: medium_id,uri:uri)
        super.init()
        if let url = URL(string: uri){
            if let urlHost = url.host{
                self.ip = urlHost
            }
            if let urlPort = (url as NSURL).port{
                self.port = urlPort
            }
        }
        socket = GCDAsyncUdpSocket(delegate: self, delegateQueue: DispatchQueue.main)
        do {
            try socket.connect(toHost: ip, onPort: UInt16(port.int16Value))
        }catch{
            
        }
        
    }
    
    
    
    func sendMessage(_ message: AnyObject) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: message, options: .prettyPrinted)
            print("SOCKET SENDING MSG \(Utils.getCurrentMillis())",message)
            if (isReady){
                socket.send(jsonData, withTimeout: 2, tag: 0)
            } else {
                messageQueue.append(jsonData as AnyObject)
            }
        }catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    func purgeMessage(){
        for message in self.messageQueue{
            if let msg = message as? String{
                let data = msg.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                if let dt = data{
                    socket.send(dt, withTimeout: 2, tag: 0)
                }
            }
        }
        self.messageQueue.removeAll()
    }
    
    func sendSDP(_ id: String, sdp: RTCSessionDescription,turnAddressList:Any?) {
        let json = [
            "id":id,
            "type":"CALL",
            "from":[
                "sdp": [
                    "sdp":sdp.sdp,
                    "timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios",
                    "type":RTCSessionDescription.string(for: sdp.type)
                ]
            ]
            ] as [String : Any]
        print("SENDSDPMESSAGESDP",json)
        sendMessage(json as AnyObject)
    }
    
    func sendCandidate(_ id: String, candidate: RTCIceCandidate?, i:Int,turnAddressList:Any?) {
        if candidate == nil {
            sendEndOfCandidate(id,i: i, turnAddressList:turnAddressList)
            return
        }
        
        var candidateDict = NSDictionary()
        candidateDict = [
            "candidate":candidate!.sdp ,
            "sdpMid":candidate!.sdpMid!,
            "sdpMLineIndex": Int64(candidate!.sdpMLineIndex)
        ]
        let json = ["id":id,
                    "type":"CALL",
                    "from":["candidate":candidateDict,"seqNo":i,"timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios"
            ]
            ] as [String : Any]
        print("SENDCANDIDATE", json)
        sendMessage(json as AnyObject)
    }
    
    func sendSDP(_ to: String, type: String, sdp: RTCSessionDescription,turnAddressList:Any?) {
        let randomCallID = Utils.randomStringWithLength(15)
        let json = ["id":"\(randomCallID)",
            "to":
                ["id":to,"type":type],
            "type":"CALL",
            "from":["sdp":
                [   "sdp":sdp.sdp,
                    "type":RTCSessionDescription.string(for: sdp.type)],"timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        print("SENDSDPMESSAGSDP",json)
        sendMessage(json as AnyObject)
    }
    
    func sosCall(_ callId:String,  to: String, type: String){
        let json = ["id": callId,
                    "to": ["id":to,"type":type],
                    "type": "CALL",
                    "from": ["signal": "sos","timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        sendMessage(json as AnyObject)
    }
    
    func hangupCall(_ callId:String,to: String, type: String){
        disconnected = true
        let json = ["id": callId,
                    "to": ["id":to,"type":type],
                    "type": "CALL",
                    "from": ["signal": "hangup","timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        sendMessage(json as AnyObject)
    }
    
    func sendEndOfCandidate(_ callId:String, i:Int,turnAddressList:Any?){
        print("%%%%%%%%%%%%%%%%call id", callId)
        let json = ["id": callId,
                    "type": "CALL",
                    "from": ["signal": "end_of_candidate","seqNo":i,"timestamp":getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        print("ENDOFCANDIDATE",json)
        sendMessage(json as AnyObject)
    }
    
    func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    
    func setHandller(_ signalHandller:RTCSignalHandler){
        self.rtcSignalhandler = signalHandller
    }
    
    deinit {
        self.closeSocket()
    }
    
    func closeSocket(){
        self.purgeMessage()
        self.disconnected = true
        self.socket.close()
    }
    
    
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didReceive data: Data, fromAddress address: Data, withFilterContext filterContext: Any?) {
        print("incoming message: \(data) \(getCurrentMillis())");
        if !isCallDisconnected{
            do {
                let convertedJsonData = try JSONSerialization.jsonObject(with: data, options:JSONSerialization.ReadingOptions(rawValue: 0))
                guard let JSONDictionary: NSDictionary = convertedJsonData as? NSDictionary else {
                    print("Not a Dictionary")
                    // put in function
                    return
                }
                print("JSONDictionary! \(JSONDictionary)")
                do {
                    if let type = JSONDictionary.object(forKey: "type") as? String{
                        if(type.uppercased() == "REGISTER"){
                            isReady = true;
                            purgeMessage();
                            return
                        }else {
                            
                        }
                    }
                    
                    let jsonString = String(data: data, encoding: String.Encoding.utf8)
                    if let dataString = jsonString{
                        rtcSignalhandler.receiveMessage(dataString)
                    }
                }
            }
            catch let JSONError as NSError {
                print("\(JSONError)")
            }
        }
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didConnectToAddress address: Data) {
        print("didConnectToAddress");
        do{
            try self.socket.beginReceiving()
        }catch{}
        isReady = true;
        purgeMessage();
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotConnect error: Error?) {
        print("didNotConnect \(String(describing: error))")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didSendDataWithTag tag: Int) {
        print("didSendDataWithTag")
    }
    
    func udpSocket(_ sock: GCDAsyncUdpSocket, didNotSendDataWithTag tag: Int, dueToError error: Error?) {
        print("didNotSendDataWithTag")
    }
    
    
}
