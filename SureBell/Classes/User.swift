//
//  User.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 17/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import ObjectMapper
import Alamofire
import AlamofireObjectMapper

/// User Description
public class User :Mappable{
    
    public static var instance = User()
    
    public init(){}
    
    public var  _id: String?
    public var  email: String?
    public var  firstname: String?
    public var  lastname: String?
    public var  phone: String?
    public var  type: String?
    public var  profilePicture: String?
    public var  phoneVerified: Bool?
    public var  emailVerified: Bool?
    public var  createdAt = Date()
    public var  updatedAt = Date()
    public var  countryDialCode: String?
    public var  sosRegistered:Bool?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self._id <- map["_id"]
        self.email <- map["email"]
        self.firstname <- map["firstname"]
        self.lastname <- map["lastname"]
        self.phone <- map["phone"]
        self.type <- map["type"]
        self.profilePicture <- map["profilePicture"]
        self.phoneVerified  <- map["phoneVerified"]
        self.emailVerified  <- map["emailVerified"]
        self.createdAt <- map["createdAt"]
        self.updatedAt <- map["updatedAt"]
        self.countryDialCode <- map["countryDialCode"]
        self.sosRegistered <- map["sosRegistered"]
    }
    
    /// Calls back create() function to register the user in the server.
    ///
    /// **Listing 1**
    ///
    ///     let firstName:ABC, lastName:EFG, email:abc@gmail.com, phone:9480235678, password:abcdefg (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5ad97a6d0af9d2001a4bafc7",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"9876598765",
    ///     "type":"USER",
    ///     "profilePicture":"default",
    ///     "phoneVerified":false,
    ///     "emailVerified":false,
    ///     "createdAt":"2018-04-20T05:28:14.041Z",
    ///     "updatedAt":"2018-04-20T05:28:14.041Z"
    ///     "countryDialCode":"+91"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"password\",\"firstname\",\"lastname\",\"phone\", \"countryDialCode\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - firstName: firstname of the user
    ///   - lastName: lastname of the user
    ///   - email: email id of the user
    ///   - phone: phone number of the user
    ///   - password: password with min 6 characters
    ///   - countryDialCode: respective country dial code
    ///   - callback: @escaping (Calls back create() function and register the user details in the server)
    ///   - newUser: User is registered successfully
    ///   - failure: @escaping (If the user details are missing then the call back fails)
    ///   - error: User not created on server
    public func create(firstName:String,lastName:String, email:String, phone:String,password:String, countryDialCode:String, callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.create(firstName: firstName, lastName: lastName, email: email, phone: phone, password: password, countryDialCode: countryDialCode, callback: callback, failure: failure)
    }
    
    /// Calls back updateNotificationToken() function for particular device(IOS/Android).
    ///
    /// **Listing 1**
    ///
    ///     let token:ABC, device:ios (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5a60b9dbe4329f0010afaa28",
    ///     "email":"niteesh@pioctave.com",
    ///     "firstname":"Niteesh",
    ///     "lastname":"Kumar",
    ///     "phone":"+919916644441",
    ///     "type":"VENDOR",
    ///     "profilePicture":"profilePicture1523612625038",
    ///     "phoneVerified":true,
    ///     "emailVerified":true,
    ///     "createdAt":"2018-01-18T15:14:35.446Z",
    ///     "updatedAt":"2018-04-24T13:51:02.397Z",
    ///     "countryDialCode":"+91"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"token\",\"device\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - token: any string of characters
    ///   - device: device ios/ android
    ///   - callback: @escaping (Calls back updateNotificationToken() function for particular device)
    ///   - newUser: successfully updated notification token
    ///   - failure: @escaping (If the token is missing then the call back fails)
    ///   - error: NotificationToken is not updated
    public func updateNotificationToken(token:String, device:String, callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.updateNotificationToken(token: token, device: device, callback: callback, failure: failure)
    }
    
    /// Calls back deleteNotificationToken() function for particular device(IOS/Android).
    ///
    /// **Listing 1**
    ///
    ///     let token:ABC (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5a60b9dbe4329f0010afaa28",
    ///     "email":"niteesh@pioctave.com",
    ///     "firstname":"Niteesh",
    ///     "lastname":"Kumar",
    ///     "phone":"+919916644441",
    ///     "type":"VENDOR",
    ///     "profilePicture":"profilePicture1523612625038",
    ///     "phoneVerified":true,
    ///     "emailVerified":true,
    ///     "createdAt":"2018-01-18T15:14:35.446Z",
    ///     "updatedAt":"2018-04-24T13:51:02.397Z",
    ///     "countryDialCode":"+91"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"token\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - token: any string of characters
    ///   - callback: @escaping (Calls back deleteNotificationToken() function for particular device)
    ///   - newUser: NotificationToken is deleted successfully
    ///   - failure: @escaping (If the token is missing then the call back fails)
    ///   - error: NotificationToken is not deleted
    public func deleteNotificationToken(token: String, callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.deleteNotificationToken(token: token, callback: callback, failure: failure)
    }
    
    /// Calls back registerVendor() function and it register's the vendor.
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 1**
    ///
    ///     {
    ///     "_id":"5ad97a6d0af9d2001a4bafc7",
    ///     "email":"sample@gmail.com",
    ///     "firstname":"sample",
    ///     "lastname":"test",
    ///     "phone":"9876598765",
    ///     "type":"VENDOR",
    ///     "profilePicture":"default",
    ///     "phoneVerified":true,
    ///     "emailVerified":true,
    ///     "createdAt":"2018-04-20T05:28:14.041Z",
    ///     "updatedAt":"2018-04-20T09:03:39.655Z"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "code": 401,
    ///     "error": "invalid_token",
    ///     "error_description": "The access token provided is invalid."
    ///     }
    ///
    /// - Parameters:
    ///   - callback: @escaping (Calls back registerVendor() function and vendor will be registered for user)
    ///   - newUser: Vendor registered successfully
    ///   - failure: @escaping (If the vendor is not registered for the user then the call back fails)
    ///   - error: Vendor not registered on server
    public func registerVendor(callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.registerVendor(callback: callback, failure: failure)
    }
    
    /// Calls back getMe() function if user is already registered.
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 1**
    ///
    ///     {
    ///     "_id":"5a60b9dbe4329f0010afaa28",
    ///     "email":"niteesh@pioctave.com",
    ///     "firstname":"Niteesh",
    ///     "lastname":"Kumar",
    ///     "phone":"+919916644441",
    ///     "type":"VENDOR",
    ///     "profilePicture":"https:storage.googleapis.com/piocave-user/5a60b9dbe4329f0010afaa28/profilePicture-profilePicture1523612625038",
    ///     "phoneVerified":true,
    ///     "emailVerified":true,
    ///     "createdAt":"2018-01-18T15:14:35.446Z",
    ///     "updatedAt":"2018-04-13T09:43:45.334Z",
    ///     "countryDialCode":"+91",
    ///     "sosRegistered":false
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "code": 401,
    ///     "error": "invalid_token",
    ///     "error_description": "The access token provided is invalid."
    ///     }
    ///
    /// - Parameters:
    ///   - callback: @escaping (Calls back getMe() function if user is already registered)
    ///   - newUser: server will response back successfully the detalis of registered user
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: user is not registered on server
    public func getMe(callback:@escaping (_ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.getMe(callback: callback, failure: failure)
    }
    
    /// Calls back updateMe() function and updates the user details.
    ///
    /// **Listing 1**
    ///
    ///     let email:abcd@gmail.com, firstname:abc, lastname:def, phone:+919876543210 (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id":"5a60b9dbe4329f0010afaa28",
    ///     "email":"niteesh@pioctave.com",
    ///     "firstname":"Niteesh",
    ///     "lastname":"Kumar",
    ///     "phone":"+919916644441",
    ///     "countryDialCode":"+91",
    ///     "type":"VENDOR",
    ///     "profilePicture":"profilePicture1523612625038",
    ///     "phoneVerified":true,
    ///     "emailVerified":true
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 401,
    ///     "error": "invalid_token",
    ///     "error_description": "The access token provided is invalid."
    ///     }
    ///
    /// - Parameters:
    ///   - userUpdate: any of the user details to update(firstname, lastname, email, phone, profile picture)
    ///   - callback: @escaping (Calls back updateMe() function and updates the user details)
    ///   - newUser: successfully updated the user details
    ///   - failure: @escaping (If the user is not registered and tries to update their details then the call back fails)
    ///   - error: user is not updated on server
    public func updateMe(userUpdate: User, callback:@escaping ( _ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.updateMe(userUpdate: userUpdate, callback: callback, failure: failure)
    }
    
    /// Calls back resetPassword() function and resets the password of the user.
    ///
    /// **Listing 1**
    ///
    ///     let email:abcd@gmail.com, password_reset_code:12345, password:123@ab (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"your password is reset"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_request",
    ///     "error_description": "missing token or code, the request is not valid"
    ///     }
    ///
    /// - Parameters:
    ///   - parameters: password_reset_code(One_time_password(OTP)) received by user from their email/phone number, email of the user, password with min 6 characters length
    ///   - callback: @escaping (Calls back resetPassword() function and resets the user password)
    ///   - newUser: Password reset is successful
    ///   - failure: @escaping (If the user is not registered and trying to reset the password then the call back fails)
    ///   - error: failed to reset the password
    public func resetPassword(parameters: Parameters, callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.resetPassword(parameters: parameters, callback: callback, failure: failure)
    }
    
    /// Calls back updatePassword() function and updates the password of the user.
    ///
    /// **Listing 1**
    ///
    ///     let currentPassword:123456, newPassword:abcdef (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "_id": "5a60b9dbe4329f0010afaa28",
    ///     "email": "niteesh@pioctave.com",
    ///     "firstname": "Niteesh",
    ///     "lastname": "Kumar",
    ///     "phone": "+919916644441",
    ///     "type": "VENDOR",
    ///     "profilePicture": "profilePicture1523612625038",
    ///     "phoneVerified": true,
    ///     "emailVerified": true,
    ///     "createdAt": "2018-01-18T15:14:35.446Z",
    ///     "updatedAt": "2018-05-02T10:34:17.510Z",
    ///     "countryDialCode": "+91"
    ///     }
    ///
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"currentPassword\",\"newPassword\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - parameters: currentPassword of the user, newPassword of the user
    ///   - callback: @escaping (Calls back updatePassword() function and updates the user password)
    ///   - newUser: updated user password successfully
    ///   - failure: @escaping (If the user is not registered and trying to update the password then the call back fails)
    ///   - error: failed to update the password
    public func updatePassword(parameters: Parameters, callback:@escaping ( _ newUser:User)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.updatePassword(parameters: parameters, callback: callback, failure: failure)
    }
    
    //    /// Calls back resendVerificationCode() function and again it sends the verification code to user email.
    //    ///
    //    /// When called function become successful, server gives the response as like below.
    //    ///
    //    /// **Listing 1**
    //    ///
    //    ///     {
    //    ///     "status":"success",
    //    ///     "message":"Please check your e-mail for verification code"
    //    ///     }
    //    ///
    //    /// when called function fails server gives the response as like below
    //    ///
    //    /// **Listing 2**
    //    ///
    //    ///     {
    //    ///     "code": 401,
    //    ///     "error": "invalid_token",
    //    ///     "error_description": "The access token provided is invalid."
    //    ///     }
    //    ///
    //    /// - Parameters:
    //    ///   - callback: @escaping (Calls back resendVerificationCode() function and again it sends the verification code to user email)
    //    ///   - newUser: code sent to email successfully
    //    ///   - failure: @escaping (If the user is not registered then the call back fails)
    //    ///   - error: failed to send the email verification code
    //    public func resendVerificationCode(callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
    //        UserHttp.instance.resendVerificationCode(callback: callback, failure: failure)
    //    }
    
    /// Calls back resendPhoneVerificationCode() function and sends back the verification code to user email again.
    ///
    /// **Listing 1**
    ///
    ///     let id:5a60b9dbe4329f0010afaa28 (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"Please check your sms for verification Code"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"id\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id: id of the user from server
    ///   - callback: @escaping (Calls back resendPhoneVerificationCode() function and sends back the verification code to user email again)
    ///   - newUser: code sent to phone successfully
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: failed to send the phone verification code
    public func resendPhoneVerificationCode(id:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.resendPhoneVerificationCode(id: id, callback: callback, failure: failure)
    }
    
    /// Calls back resendEmailVerificationCode() function and sends back the verification code to user email again.
    ///
    /// **Listing 1**
    ///
    ///     let id:5a60b9dbe4329f0010afaa28 (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"Please check your e-mail for verification code"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"id\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id: id of the user from server
    ///   - callback: @escaping (Calls back resendEmailVerificationCode() function and sends back the verification code to user email again)
    ///   - newUser: code sent to email successfully
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: failed to send the email verification code
    public func resendEmailVerification(id:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.resendEmailVerification(id:id,callback: callback, failure: failure)
    }
    
    /// Calls back generateLoginOtp() function and sends the OTP to user mobile number.
    ///
    /// **Listing 1**
    ///
    ///     let phone:9876543210 (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"Please check your sms for otp"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"phone\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - phone: mobile number of the user
    ///   - callback: @escaping (Calls back generateLoginOtp() function and sends the OTP to user mobile number)
    ///   - newUser: OTP sent successfully
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: failed to send the OTP code
    public func generateLoginOtp(phone:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.generateLoginOtp(phone: phone, callback: callback, failure: failure)
    }
    
    /// Calls back verifyEmail() function and verifies the user account.
    ///
    /// **Listing 1**
    ///
    ///     let id:5a60b9dbe4329f0010afaa28, code:12345 (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"account successfully verified"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"id\",\"code\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id: id of the user from the server
    ///   - code: code sent to email of the user
    ///   - callback: @escaping (Calls back verifyEmail() function and verifies the user account)
    ///   - newUser: user email account verified successfully
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: failed to verify the user account
    public func verifyEmail( id:String,  code:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.verifyEmail(id: id, code: code, callback: callback, failure: failure)
    }
    
    /// Calls back verifyPhone() function and verifies the user phone number.
    ///
    /// **Listing 1**
    ///
    ///     let id:5a60b9dbe4329f0010afaa28, code:12345 (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"account successfully verified"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"id\",\"code\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - id: id of the user from the server
    ///   - code: code(OTP) sent to phone number of the user
    ///   - callback: @escaping (Calls back verifyPhone() function and verifies the user phone number)
    ///   - newUser: phone number verified successfully
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: failed to verify the user phone number
    public func verifyPhone(id:String,  code:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.verifyPhone(id: id, code: code, callback: callback, failure: failure)
    }
    
    /// Calls back forgotPassword() function and sends the password reset link to user email.
    ///
    /// **Listing 1**
    ///
    ///     let userName:abcd@gmail.com/9876543210 (param passed by user to server)
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"we have sent reset link to you, please check your mail"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 3**
    ///
    ///     {
    ///     "code": 400,
    ///     "error": "invalid_params",
    ///     "error_description": "missing necessary params{\"query\":[],\"body\":[\"userName\"],\"params\":[]}"
    ///     }
    ///
    /// - Parameters:
    ///   - userName: email id of the user/ mobile number of the user
    ///   - callback: @escaping (Calls back forgotPassword() function and sends the password reset link to email id of the user)
    ///   - newUser: we have sent reset link to you, please check your mail
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: failed to send the password reset link
    public func forgotPassword(userName:String,callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.forgotPassword(userName: userName, callback: callback, failure: failure)
    }
    
    /// Calls back logoutFromOtherDevices() function and logs out the user email from all other devices other than the using one.
    ///
    /// When called function become successful, server gives the response as like below.
    ///
    /// **Listing 1**
    ///
    ///     {
    ///     "status":"success",
    ///     "message":"You are logged out of all other devices"
    ///     }
    ///
    /// when called function fails server gives the response as like below
    ///
    /// **Listing 2**
    ///
    ///     {
    ///     "code": 401,
    ///     "error": "invalid_token",
    ///     "error_description": "The access token provided is invalid."
    ///     }
    ///
    /// - Parameters:
    ///   - callback: @escaping (Calls back logoutFromOtherDevices() function and logs out user email from all other devices other than the using one)
    ///   - newUser:You are logged out of all other devices successfully
    ///   - failure: @escaping (If the user is not registered then the call back fails)
    ///   - error: failed to logout from other devices
    public func logoutFromOtherDevices(callback:@escaping ( _ newUser:StatusResponse)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.logoutFromOtherDevices(callback: callback, failure: failure)
    }
    
    /// <#Description#>
    ///
    /// - Parameters:
    ///   - id: <#id description#>
    ///   - imageData: <#imageData description#>
    ///   - timeinms: <#timeinms description#>
    ///   - callback: <#callback description#>
    ///   - failure: <#failure description#>
    public func uploadProfileImage(imageData: Data, callback:@escaping (_ newDevice:Bool)->Void, failure:@escaping (_ error:Any)->Void) -> Void {
        UserHttp.instance.uploadProfileImage(imageData: imageData, callback: callback, failure: failure)
    }
    
}

