//
//  Event.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 17/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import ObjectMapper


/// This Event class is an instance of Device module, where parameters of this class will get maps with the JSON object.
/// This class includes **device id**, **name**, **type**, **createdAt**, **createdBy**, **image**, **allowUsers**, **comments**, **like**, **status**, **videosize**,
/// **video**, **version**, **imageSize** and **urlSummary** as member variables.
///
public class Event: Mappable {
    public var _id : String?
    public var name : String?
    public var type : String?
    public var createdAt : String?
    public var createdBy : CreatedBy?
    public var image : String?
    public var allowedUsers : [String]?
    public var comments : [String]?
    public var like : [String]?
    public var status : String?
    public var videoSize : [Int]?
    public var video : String?
    public var __v : Int?
    public var imageSize : Int?
    public var urlSummary : String?
    
    required convenience public init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        self._id <- map["_id"]
        self.name <- map["name"]
        self.type <- map["type"]
        self.createdAt <- map["createdAt"]
        self.createdBy <- map["createdBy"]
        self.image <- map["image"]
        self.allowedUsers <- map["allowedUsers"]
        self.comments <- map["comments"]
        self.like <- map["like"]
        self.status <- map["status"]
        self.videoSize <- map["videoSize"]
        self.video <- map["video"]
        self.__v <- map["__v"]
        self.imageSize <- map["imageSize"]
        self.urlSummary <- map["urlSummary"]
    }
}
