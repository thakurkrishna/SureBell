//
//  WebSocketSignallingChannelDelegate.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation
import WebRTC
import Starscream
import SwiftyJSON

class WebSocketSignallingChannelDelegate:NSObject,WebSocketDelegate, SignallingChannel{
    func websocketDidConnect(socket: WebSocketClient) {
        
        
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
         let encodedString : Data = (error?.localizedDescription as! NSString).data(using: String.Encoding.utf8.rawValue)!
        // var finalJSON = try! JSON(data: encodedString)
        print(error)
//                if error?.localizedDescription == 401{
//                    connection.updateSureTokens({ (Bool) in
//                        if Bool{
//                            self.url = ""
//                            self.url = self.uri + "?access_token=" + SharedData.UserDetails.USER_ACCESS_TOKEN + "&medium=client&mediumId=" + self.medium_id
//                            self.reconnect()
//                        }
//                    })
//                }else{
//                    isReady = false;
//                    if !disconnected{
//                        reconnect()
//                    }
//                }
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        if !isCallDisconnected{
            do {
                if let encodedText = text.data(using: String.Encoding.utf8){
                    let data = try JSONSerialization.jsonObject(
                        with: encodedText,
                        options: JSONSerialization.ReadingOptions())
                    if let type = (data as AnyObject).object(forKey: "type") as? String{
                        if(type.uppercased() == "REGISTER"){
                            print("⚠️ Message recived from server","Message recived" + text)
                            isReady = true;
                            purgeMessage();
                            return
                        }
                    }
                }
                rtcSignalhandler.receiveMessage(text)
            } catch let error as NSError {
                print("⚠️######RECEIVE MESSAGE##########",error.description)
            }
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }
    
    func websocketDidDisconnect(socket: WebSocket, error: Error?) {
        print("⚠️ websocketDidDisconnect : ", error as Any)
        // let encodedString : Data = (error?.localizedDescription as! NSString).data(using: String.Encoding.utf8.rawValue)!
        // var finalJSON = JSON(data: encodedString)
        //        if error?.localizedDescription == 401{
        //            connection.updateSureTokens({ (Bool) in
        //                if Bool{
        //                    self.url = ""
        //                    self.url = self.uri + "?access_token=" + SharedData.UserDetails.USER_ACCESS_TOKEN + "&medium=client&mediumId=" + self.medium_id
        //                    self.reconnect()
        //                }
        //            })
        //        }else{
        //            isReady = false;
        //            if !disconnected{
        //                reconnect()
        //            }
        //        }
    }
    
    
    func websocketDidConnect(socket: WebSocket) {
        print("⚠️ websocket is connected")
    }
    
    //    func websocketDidDisconnect(socket: WebSocket, error: Error?) {
    //        print("⚠️ websocketDidDisconnect : ", error as Any)
    //        // let encodedString : Data = (error?.localizedDescription as! NSString).data(using: String.Encoding.utf8.rawValue)!
    //        // var finalJSON = JSON(data: encodedString)
    //        //        if error?.localizedDescription == 401{
    //        //            connection.updateSureTokens({ (Bool) in
    //        //                if Bool{
    //        //                    self.url = ""
    //        //                    self.url = self.uri + "?access_token=" + SharedData.UserDetails.USER_ACCESS_TOKEN + "&medium=client&mediumId=" + self.medium_id
    //        //                    self.reconnect()
    //        //                }
    //        //            })
    //        //        }else{
    //        //            isReady = false;
    //        //            if !disconnected{
    //        //                reconnect()
    //        //            }
    //        //        }
    //    }
    
    func websocketDidReceiveMessage(socket: WebSocket, text: String) {
        //NSLog("Message recived from server","Message recived" + text)
        if !isCallDisconnected{
            do {
                if let encodedText = text.data(using: String.Encoding.utf8){
                    let data = try JSONSerialization.jsonObject(
                        with: encodedText,
                        options: JSONSerialization.ReadingOptions())
                    if let type = (data as AnyObject).object(forKey: "type") as? String{
                        if(type.uppercased() == "REGISTER"){
                            print("⚠️ Message recived from server","Message recived" + text)
                            isReady = true;
                            purgeMessage();
                            return
                        }
                    }
                }
                rtcSignalhandler.receiveMessage(text)
            } catch let error as NSError {
                print("⚠️######RECEIVE MESSAGE##########",error.description)
            }
        }
    }
    
    func websocketDidReceiveData(socket: WebSocket, data: Data) {
        print("⚠️ Received data: \(data.count)")
    }
    
    
    
    fileprivate var socket:WebSocket!
    fileprivate var url:String?
    fileprivate var uri:String!
    fileprivate var medium_id:String!
    fileprivate var rtcSignalhandler:RTCSignalHandler!
    fileprivate var isReady = false
    fileprivate var disconnected = false
    fileprivate var messageQueue = [AnyObject]()
    //var connection = Connection()
    var isCallDisconnected = false
    var callID = ""
    
    
    
    init(access_token:String,medium_id:String,uri:String,rtcSignalhandler:RTCSignalHandler){
        super.init()
        self.url =  uri + "?access_token=" + access_token + "&medium=client&mediumId=" + medium_id
        self.uri = uri
        self.medium_id = medium_id
        self.rtcSignalhandler = rtcSignalhandler
        reconnect()
    }
    
    init(access_token:String,medium_id:String,uri:String){
        super.init()
        self.url = uri + "?access_token=" + access_token + "&medium=client&mediumId=" + medium_id
        self.medium_id = medium_id
        self.uri = uri
        NotificationCenter.default.addObserver(self, selector: #selector(self.invalidateTimers(_:)), name:NSNotification.Name(rawValue: "DISCONNECT"), object: nil)
        reconnect()
    }
    
    func setHandller(_ signalHandller:RTCSignalHandler){
        self.rtcSignalhandler = signalHandller
    }
    
    // MARK: Websocket Delegate Methods.
    
    
    
    
    
    @objc func invalidateTimers(_ notification: Notification){
        self.isCallDisconnected = true
    }
    
    
    func closeSocket(){
        self.purgeMessage()
        self.disconnected = true
        self.socket.disconnect()
    }
    
    
    func sendMediaSignal(_ callId:String,  to: String, type: String, i:Int,turnAddressList:Any?) {
        if let turn = turnAddressList{
            let jsonSignal = ["id": callId,
                              "to": ["id":to,"type":type],
                              "type": "CALL",
                              "from": ["signal": "start_media","seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn]
                ] as [String : Any]
            sendMessage(jsonSignal as AnyObject)
        }else{
            let jsonSignal = ["id": callId,
                              "to": ["id":to,"type":type],
                              "type": "CALL",
                              "from": ["signal": "start_media","seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
                ] as [String : Any]
            sendMessage(jsonSignal as AnyObject)
        }
    }
    
    
    func sendMessage(_ message: AnyObject) {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: message, options: .prettyPrinted)
            print("⚠️ SOCKET SENDING MSG \(Utils.getCurrentMillis())",message)
            if (isReady){
                print("⚠️ SOCKET Sent MSG \(Utils.getCurrentMillis())",message)
                self.socket.write(data: jsonData)
            } else {
                DispatchQueue.main.async {
                    self.messageQueue.append(jsonData as AnyObject)
                }
            }
        }catch let error as NSError {
            print("⚠️ WebSocket sent failed",error.localizedDescription)
        }
    }
    
    
    func purgeMessage(){
        for message in self.messageQueue{
            // print("SENDING DATA TO SOCKET PURGE",message)
            if let data = message as? Data{
                let json = JSON(data)
                print("⚠️ SOCKET Sent MSG",json)
                self.socket.write(data: data)
            }
        }
        self.messageQueue.removeAll()
    }
    
    
    func sendSDP(_ id: String, sdp: RTCSessionDescription,turnAddressList:Any?) {
        print("⚠️ SENDING SDP \(Utils.getCurrentMillis())")
        if let turn = turnAddressList{
            let json = [
                "id":id,
                "type":"CALL",
                "from":[
                    "sdp": [
                        "sdp":sdp.sdp,
                        "timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn,
                        "type":RTCSessionDescription.string(for: sdp.type)
                    ]
                ]
                ] as [String : Any]
            //Toast(text:"sending SDP to server").show()
            sendMessage(json as AnyObject)
        }else{
            let json = [
                "id":id,
                "type":"CALL",
                "from":[
                    "sdp": [
                        "sdp":sdp.sdp,
                        "timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios",
                        "type":RTCSessionDescription.string(for: sdp.type)
                    ]
                ]
                ] as [String : Any]
            //Toast(text:"sending SDP to server").show()
            sendMessage(json as AnyObject)
        }
    }
    
    
    func sendCandidate(_ id: String, candidate: RTCIceCandidate?, i:Int,turnAddressList:Any?) {
        print("⚠️ SENDING CANDIDATE \(Utils.getCurrentMillis())")
        if candidate == nil {
            sendEndOfCandidate(id,i: i,turnAddressList:turnAddressList)
            return
        }
        
        if let turn = turnAddressList{
            var candidateDict = NSDictionary()
            candidateDict = [
                "candidate":candidate!.sdp ,
                "sdpMid":candidate!.sdpMid!,
                "sdpMLineIndex": Int64(candidate!.sdpMLineIndex)
            ]
            let json = ["id":id,
                        "type":"CALL",
                        "from":["candidate":candidateDict,"seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn
                ]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }else{
            var candidateDict = NSDictionary()
            candidateDict = [
                "candidate":candidate!.sdp ,
                "sdpMid":candidate!.sdpMid!,
                "sdpMLineIndex": Int64(candidate!.sdpMLineIndex)
            ]
            let json = ["id":id,
                        "type":"CALL",
                        "from":["candidate":candidateDict,"seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"
                ]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }
    }
    
    func sendEndOfCandidate(_ callId:String,  to: String, type: String, i:Int,turnAddressList:Any?){
        print("⚠️ SENDING EOD \(Utils.getCurrentMillis())")
        if let turn = turnAddressList{
            let json = ["id": callId,
                        "to": ["id":to,"type":type],
                        "type": "CALL",
                        "from": ["signal": "end_of_candidate","seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }else{
            let json = ["id": callId,
                        "to": ["id":to,"type":type],
                        "type": "CALL",
                        "from": ["signal": "end_of_candidate","seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }
        //print(“%%%%%%%%%%%%%%%%call id”, callId)
    }
    
    func sendCandidate(_ id: String,  to: String, type: String, candidate: RTCIceCandidate?, i:Int,turnAddressList:Any?) {
        print(" SENDING CANDIDATE \(Utils.getCurrentMillis())")
        if candidate == nil {
            sendEndOfCandidate(id,i: i,turnAddressList:turnAddressList)
            return
        }
        if let turn = turnAddressList{
            var candidateDict = NSDictionary()
            candidateDict = [
                "candidate":candidate!.sdp ,
                "sdpMid":candidate!.sdpMid!,
                "sdpMLineIndex": Int64(candidate!.sdpMLineIndex)
            ]
            let json = ["id":id,
                        "to": ["id":to,"type":type],
                        "type":"CALL",
                        "from":["candidate":candidateDict,"seqNo":i,
                                "timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn
                ]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }else{
            var candidateDict = NSDictionary()
            candidateDict = [
                "candidate":candidate!.sdp ,
                "sdpMid":candidate!.sdpMid!,
                "sdpMLineIndex": Int64(candidate!.sdpMLineIndex)
            ]
            let json = ["id":id,
                        "to": ["id":to,"type":type],
                        "type":"CALL",
                        "from":["candidate":candidateDict,"seqNo":i,
                                "timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"
                ]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }
    }
    
    func sendSDP(_ to: String, type: String, sdp: RTCSessionDescription,turnAddressList:Any?) {
        print("⚠️ SENDING SDP \(Utils.getCurrentMillis())")
        if let turn = turnAddressList{
            let json = ["to":
                ["id":to,"type":type],
                        "type":"CALL",
                        "from":["sdp":
                            [   "sdp":sdp.sdp,
                                "type":RTCSessionDescription.string(for: sdp.type)],"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }else{
            let json = ["to":
                ["id":to,"type":type],
                        "type":"CALL",
                        "from":["sdp":
                            [   "sdp":sdp.sdp,
                                "type":RTCSessionDescription.string(for: sdp.type)],"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }
    }
    
    
    func sendSdp(_ id : String,  to: String, type: String, sdp: RTCSessionDescription,turnAddressList:Any?){
        print("⚠️ SENDING SDP \(Utils.getCurrentMillis())")
        if let turn = turnAddressList{
            let json = [
                "id":id,
                "to": ["id":to,"type":type],
                "type":"CALL",
                "from":["sdp":
                    [   "sdp":sdp.sdp,
                        "type":RTCSessionDescription.string(for: sdp.type)],"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }else{
            let json = [
                "id":id,
                "to": ["id":to,"type":type],
                "type":"CALL",
                "from":["sdp":
                    [   "sdp":sdp.sdp,
                        "type":RTCSessionDescription.string(for: sdp.type)],"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }
    }
    
    func sosCall(_ callId:String,  to: String, type: String){
        let json = ["id": callId,
                    "to": ["id":to,"type":type],
                    "type": "CALL",
                    "from": ["signal": "sos","timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        sendMessage(json as AnyObject)
    }
    
    func hangupCall(_ callId:String,to: String, type: String){
        disconnected = true
        let json = ["id": callId,
                    "to": ["id":to,"type":type],
                    "type": "CALL",
                    "from": ["signal": "hangup","timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
            ] as [String : Any]
        sendMessage(json as AnyObject)
    }
    
    
    deinit {
        self.closeSocket()
    }
    
    func sendEndOfCandidate(_ callId:String, i:Int,turnAddressList:Any?){
        print("⚠️ SENDING EOD \(Utils.getCurrentMillis())")
        if let turn = turnAddressList{
            let json = ["id": callId,
                        "type": "CALL",
                        "from": ["signal": "end_of_candidate","seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios","iceServers":turn]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }else{
            let json = ["id": callId,
                        "type": "CALL",
                        "from": ["signal": "end_of_candidate","seqNo":i,"timestamp":Utils.getCurrentMillis(),"version": Utils.appVersion,"app":"ios"]
                ] as [String : Any]
            sendMessage(json as AnyObject)
        }
    }
    
    func reconnect(){
        if self.socket != nil{
            self.socket.disconnect()
        }
        
        if let stringURL = self.url{
            if let url = URL(string: stringURL){
                self.socket = WebSocket(url: url, protocols: nil)
                self.socket.delegate = self
                self.socket.connect()
            }
        }
    }
    
}
