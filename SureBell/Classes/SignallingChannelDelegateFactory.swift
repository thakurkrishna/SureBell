//
//  SignallingChannelDelegateFactory.swift
//  sureBellFramework-iOS
//
//  Created by PiOctave on 16/04/18.
//

//    Copyright (C) 2016-2018 PiOctave Solutions (Pvt) Ltd. (http://www.pioctave.com/)

//    All information contained herein is, and remains
//    the property of PiOctave Solutions (Pvt) Ltd and its suppliers
//    if any.  The intellectual and technical concepts contained
//    herein are proprietary to PiOctave Solutions (Pvt) Ltd
//    and are protected by trade secret or copyright law.
//    Dissemination of this information or reproduction of this material
//    is strictly forbidden unless prior written permission is obtained
//    from PiOctave Solutions (Pvt) Ltd.

//    THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
//    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
//    IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
//    ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
//    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
//    DAMAGES. HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
//    STRICT LIABILITY, OR TORT ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
//    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import Foundation


public class SignallingChannelDelegateFactory {
    
    public static var instance = SignallingChannelDelegateFactory()
    
    public func getSignallingChannelDelegate(_ type:String,access_token:String, medium_id: String, uri: String) ->SignallingChannel{
        switch type {
        case "local":
            return DataGramSignallingChannelDelegate(access_token: access_token, medium_id:medium_id, uri:  uri)
        default:
            return WebSocketSignallingChannelDelegate(access_token: access_token, medium_id:medium_id, uri:  uri)
        }
    }
    
}
