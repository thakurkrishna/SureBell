
Pod::Spec.new do |s|
  s.name             = 'SureBell'
  s.version          = '1.0.11'
  s.summary          = 'SureBell framework for iOS.'

  s.description      = <<-DESC
    SureBell framework for iOS.
                       DESC

  s.homepage         = 'https://gitlab.com/thakurkrishna/SureBell'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Krishna Thakur' => 'krishna@pioctave.com' }
  s.source           = { :git => 'https://gitlab.com/thakurkrishna/SureBell.git' }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'
#s.requires_arc = true
  s.source_files = 'SureBell/Classes/**/*'
  
  # s.resource_bundles = {
  #   'SureBell' => ['SureBell/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
s.dependency 'Alamofire','4.7.1'
s.dependency 'CocoaAsyncSocket','7.6.3'
s.dependency 'WebRTC','63.11.20455'
s.dependency 'Starscream','3.0.4'
s.dependency 'SwiftyJSON'
s.dependency 'AlamofireObjectMapper','5.0.0'
end