//
//  SelectDeviceController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell


class SelectDeviceController: UIViewController {
    
    @IBOutlet weak var selectdevicelbl: UILabel!

    var timer:Timer?
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var isSecond = false
    
    
    @IBAction func next(_ sender: Any) {
        pingToAp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
//            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
//        })
//        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.pingNow(_:)), name:NSNotification.Name(rawValue: SureConstantResources.NotificationType.APP_IS_ACTIVE), object: nil)

    }
    
//    @objc func update() {
//        //print(GenralFunctions.fetchSSIDInfo())
//        let wifiInfo = GenralFunctions.fetchSSIDInfo()
//        if wifiInfo.count > 0{
//            if wifiInfo[0].contains("SureBell"){
//                self.timer?.invalidate()
//                //AppDelegate.shared.showPushNotification("\(wifiInfo[0])", details: "Connected. Go back to Sûre app.", id: "com.pioctave.localNotification.WifiSetup",media: "",userInfo: ["":""])
//            }
//        }
//    }

//    @objc func pingNow(_ notification: Notification){
//        pingToAp()
//        viewWillAppear(false)
//        isSecond = true
//    }

    
    func pingToAp(){
        let wifiInfo = GenralFunctions.fetchSSIDInfo()
        if wifiInfo.count > 0{
            if wifiInfo[0].contains("SureBell"){
                self.timer?.invalidate()
                Device.instance.getPingResponse(ipAddress: SureConstantResources.AP_DEFAULT_IP, callback: { (ping) in
                    DispatchQueue.main.async{
                        self.timer?.invalidate()
                        Device.instance.getSSIDList(ipAddress: SureConstantResources.AP_DEFAULT_IP, callback: { (ssids) in
                            DispatchQueue.main.async{
                                self.timer?.invalidate()
                                let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
                                let wifiList: WifiListViewController = storyboard.instantiateViewController(withIdentifier: "WifiListViewController") as! WifiListViewController
                                self.navigationController?.pushViewController(wifiList, animated: false)
                            }

                        }, { (error) in
                            
                        })
                    }
                }, { (error) in
                    
                })
            }
    }
    }
    
    func tryAgain(){
        if isSecond{
            isSecond = false
            DispatchQueue.main.async(execute: {
                let refreshAlert = UIAlertController(title: SureConstantResources.ALERT_TITLE, message: SureConstantResources.BASIC_ERROR_MSG, preferredStyle: UIAlertControllerStyle.alert)
                refreshAlert.addAction(UIAlertAction(title: "Try again", style: .default, handler: { (action: UIAlertAction!) in
                    self.pingToAp()
                }))
                refreshAlert.addAction(UIAlertAction(title: SureConstantResources.CANCEL_LBL, style: .cancel, handler: { (action: UIAlertAction!) in
                    
                }))
                self.present(refreshAlert, animated: true, completion: nil)
            })
        }
    }


}
