//
//  ForgotPasswordViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/14/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class ForgotPasswordViewController: UIViewController, UITextFieldDelegate, UIPopoverPresentationControllerDelegate, CountrySelected {
    
    @IBAction func login(_ sender: Any) {
        let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
        let login: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        self.navigationController?.pushViewController(login, animated: true)
    }
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var sendCode: UIButton!
    @IBOutlet weak var login: UIButton!
    
    @IBAction func sendCode(_ sender: Any) {
        sendCodeAction()
    }
    
    var selectedCountryCode = "+91"
    let leftTextUsername = UILabel()
    let leftViewUsername = UIView()

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.userName.backgroundColor = UIColor.white
        
        if textField == self.userName{
            if let text = textField.text{
                if text.isNumeric{
                    if text.count >= 5{
                        self.showCountryCodeView()
                    }else{
                        self.hideCountryCode()
                    }
                }else{
                    self.hideCountryCode()
                }
            }else{
                self.hideCountryCode()
            }
        }
        
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string) as NSString
        let nsString = NSString(string: textField.text!)
        let newText = nsString.replacingCharacters(in: range, with: string)
        guard range.location == 0 else {
            if newText.count <= 50{
                return true
            }else{
                return  newText.count <= 50 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
            }
        }
        return  newText.count <= 50 && newString.rangeOfCharacter(from: NSCharacterSet.whitespacesAndNewlines).location != 0
    }
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.userName.backgroundColor = UIColor.white
        if textField == self.userName{
            // dropDown.show()
        }
    }
    
    
    //Auto phone and wmail selection
    func showCountryCodeView(){
        
        leftTextUsername.font = UIFont(name: "ClanOT-News", size: 15)
        leftTextUsername.textColor = UIColor.gray
        leftTextUsername.textAlignment = .center
        leftViewUsername.addSubview(leftTextUsername)
        
        
        leftViewUsername.frame = CGRect(x: 0, y: 1, width: 80, height: userName.frame.height)
        leftTextUsername.frame = CGRect(x: 0, y: 1, width: 80, height: userName.frame.height)
        userName.leftViewMode = .always
        userName.leftView = leftViewUsername
        if selectedCountryCode.contains("+"){
            
            leftTextUsername.text = IsoCountryCodes.searchByCallingCode(calllingCode: selectedCountryCode).flag + selectedCountryCode
        }else{
            leftTextUsername.text = "Country Code"
        }
        leftTextUsername.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapFunction(_:)))
        leftTextUsername.addGestureRecognizer(tap)
    }
    
    func hideCountryCode(){
        leftTextUsername.text = ""
        self.leftTextUsername.removeFromSuperview()
        self.leftTextUsername.removeFromSuperview()
        userName.leftViewMode = .never
        leftTextUsername.isUserInteractionEnabled = false
    }
    
    @objc func tapFunction(_ sender:UITapGestureRecognizer) {
        self.dismissKeyboard()
        let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
        guard let settingsViewController = storyboard.instantiateViewController(withIdentifier: "CountrySelectionTableViewController") as? CountrySelectionTableViewController else {
            return
        }
        settingsViewController.delegate = self
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "close"),style: .done, target: self, action: #selector(dismissSettings))
        
        settingsViewController.navigationItem.rightBarButtonItem = barButtonItem
        settingsViewController.title = "Select a Country"
        
        let navigationController = UINavigationController(rootViewController: settingsViewController)
        navigationController.navigationBar.tintColor = UIColor.darkGray
        navigationController.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.darkGray];
        navigationController.modalPresentationStyle = .popover
        navigationController.popoverPresentationController?.delegate = self
        navigationController.preferredContentSize = CGSize(width: self.view.bounds.size.width - 10, height: self.view.bounds.size.height - 60)
        self.present(navigationController, animated: true, completion: nil)
        navigationController.popoverPresentationController?.sourceView = leftTextUsername
        navigationController.popoverPresentationController?.backgroundColor = UIColor.white
        navigationController.popoverPresentationController?.sourceRect = leftTextUsername.bounds
    }
    
    
    func sendData(countryCode: String, counrtyName: String, countryFlag: String) {
        self.leftTextUsername.text = countryFlag + " " + countryCode
        self.selectedCountryCode = countryCode
    }
    
    @objc
    func dismissSettings() {
        self.dismiss(animated: true, completion: nil)
        //TODO
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sendCode.setTitleColor(UIColor.white, for: UIControlState())
        login.setTitleColor(UIColor.darkGray, for: UIControlState())
        
//        userName.tintColor = UIColor.darkGray
//        userName.selectedTitleColor = UIColor.darkGray
//        userName.selectedLineColor = UIColor.darkGray
//        userName.textColor = UIColor.black
//        userName.lineColor = UIColor.lightGray.withAlphaComponent(0.3)
//        userName.placeholder = "Email or Phone Number"
//        self.view.addSubview(userName)
        
        sendCode.backgroundColor = UIColor.black
        login.backgroundColor = UIColor.white
        
        self.userName.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }

    func checkEmptyValidation() -> Bool{
        var message:String = ""
        if userName!.text == "" || (userName!.text?.isEmpty)!{
            message = SureConstantResources.USERNAME_REQUIRED
            displayMyAlertMessage(userMessage: message)
            return false
        }else{
            return true
        }
    }
    
    func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        if !emailTest.evaluate(with: testStr){
            displayMyAlertMessage(userMessage: SureConstantResources.INVALID_EMAIL)
        }
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhone(_ testStr:String) -> Bool {
        let PHONE_REGEX = "^[1-9]{1}[0-9]+$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: testStr)
        if !result{
            displayMyAlertMessage(userMessage: SureConstantResources.INVALID_PHONE)
        }
        return  result
    }

    func sendCodeAction(){
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
        if checkEmptyValidation(){
            if  userName.text!.isNumeric ? isValidPhone(userName.text!) :
                isValidEmail(userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)){
                var user = ""
                if userName.text!.isNumeric{
                    user = self.selectedCountryCode + userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                }else{
                    user = userName.text!.trimmingCharacters(in: CharacterSet.whitespaces)
                }
                User.instance.forgotPassword(userName: user, callback: { (StatusResponse) in
                    print(StatusResponse)
                    let userMessage = "Check your Inbox for Reset Code"
                    
                    let alert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Verify", style: UIAlertActionStyle.destructive, handler: { action in
                        DispatchQueue.main.async {
                            User.instance.email = user
                            let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
                            let reset: ResetPasswordViewController = storyboard.instantiateViewController(withIdentifier: SureConstantResources.RESET_PASSWORD_VIEW_CONTROLLER) as! ResetPasswordViewController
                            self.navigationController?.pushViewController(reset, animated: false)
                        }
                        
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }, failure: { (error) in
                    if let error =  error as? APIError{
                        guard  let des = error.error_description else{return}
                        self.displayMyAlertMessage(userMessage: des)
                    }else if let error =  error as? Error{
                        guard let error = error as? Error else{return}
                        self.displayMyAlertMessage(userMessage: error as! String)
                    }
                })
            }
        }
        }else{
                self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }


}
