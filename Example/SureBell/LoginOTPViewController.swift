//
//  LoginOTPViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/14/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class LoginOTPViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var firstnum: UITextField!
    @IBOutlet weak var secondnum: UITextField!
    @IBOutlet weak var thirdnum: UITextField!
    
    @IBOutlet weak var forthnum: UITextField!
    @IBOutlet weak var fifthnum: UITextField!
    @IBOutlet weak var verify: UIButton!
    
    
    var otp = ""
    var phone:String?
    var loginWithOTP = false
    var phverify = false
    static var countryCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        verify.setTitleColor(UIColor.white, for: UIControlState())
        
        verify.backgroundColor = UIColor.black
        
        self.firstnum.borderStyle = UITextBorderStyle.roundedRect
        self.firstnum.autocorrectionType = UITextAutocorrectionType.no
        self.firstnum.keyboardType = UIKeyboardType.numberPad
        self.firstnum.returnKeyType = UIReturnKeyType.done
        self.firstnum.textAlignment = .center
        self.firstnum.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        self.firstnum.delegate = self

        self.secondnum.borderStyle = UITextBorderStyle.roundedRect
        self.secondnum.autocorrectionType = UITextAutocorrectionType.no
        self.secondnum.keyboardType = UIKeyboardType.numberPad
        self.secondnum.returnKeyType = UIReturnKeyType.done
        self.secondnum.textAlignment = .center
        self.secondnum.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        self.secondnum.delegate = self
        
        self.thirdnum.borderStyle = UITextBorderStyle.roundedRect
        self.thirdnum.autocorrectionType = UITextAutocorrectionType.no
        self.thirdnum.keyboardType = UIKeyboardType.numberPad
        self.thirdnum.returnKeyType = UIReturnKeyType.done
        self.thirdnum.textAlignment = .center
        self.thirdnum.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        self.thirdnum.delegate = self

        self.forthnum.borderStyle = UITextBorderStyle.roundedRect
        self.forthnum.autocorrectionType = UITextAutocorrectionType.no
        self.forthnum.keyboardType = UIKeyboardType.numberPad
        self.forthnum.returnKeyType = UIReturnKeyType.done
        self.forthnum.textAlignment = .center
        self.forthnum.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        self.forthnum.delegate = self

        self.fifthnum.borderStyle = UITextBorderStyle.roundedRect
        self.fifthnum.autocorrectionType = UITextAutocorrectionType.no
        self.fifthnum.keyboardType = UIKeyboardType.numberPad
        self.fifthnum.returnKeyType = UIReturnKeyType.done
        self.fifthnum.textAlignment = .center
        self.fifthnum.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        self.fifthnum.delegate = self

        self.firstnum.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        self.secondnum.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        self.thirdnum.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        self.forthnum.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
        self.fifthnum.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField){
        let text = textField.text
        if text?.utf16.count==1{
            switch textField{
            case self.firstnum:
                self.secondnum.becomeFirstResponder()
            case self.secondnum:
                self.thirdnum.becomeFirstResponder()
            case self.thirdnum:
                self.forthnum.becomeFirstResponder()
            case self.forthnum:
                self.fifthnum.becomeFirstResponder()
            case self.fifthnum:
                if loginWithOTP {
                    self.loginwithOTPAction()
                }else {
                    if LoginOTPViewController.countryCode != "" {
                        self.phverifyAction()
                    }else {
                        self.emailverifyAction()
                    }
                }
            default:
                break
            }
        }else{
            
        }
    }
    
    func loginwithOTPAction() {
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
        if firstnum.text != "" && secondnum.text != "" && thirdnum.text != "" && forthnum.text != "" && fifthnum.text != ""{
            self.otp = firstnum.text! + secondnum.text! + thirdnum.text! + forthnum.text! + fifthnum.text!
            Token.instance.getToken(userName: User.instance.phone!, password: self.otp, callback: { (token) in
                
            User.instance.getMe(callback: { (user) in
                DispatchQueue.main.async {
                    User.instance.firstname = user.firstname
                    User.instance.lastname = user.lastname
                    User.instance.email = user.email
                    User.instance.phone = user.phone
                    User.instance.profilePicture = user.profilePicture
                    User.instance.countryDialCode = user.countryDialCode
                    User.instance._id = user._id
                    
                    let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
                    let appHome: AppHomeViewController = storyboard.instantiateViewController(withIdentifier: "AppHomeViewController") as! AppHomeViewController
                    self.navigationController?.pushViewController(appHome, animated: true)
                
                }
                }, failure: { (error) in
                    
                    })
                
                }, failure: { (error) in
                if let error =  error as? APIError{
                    guard  let des = error.error_description else{return}
                    self.displayMyAlertMessage(userMessage: des)
                }else if let error =  error as? Error{
                    guard let error = error as? Error else{return}
                    self.displayMyAlertMessage(userMessage: error as! String)
                }
            })
        }else{
            self.displayMyAlertMessage(userMessage: "Please enter the OTP")
            }
        }else{
                self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }
    
    func phverifyAction() {
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
            if firstnum.text != "" && secondnum.text != "" && thirdnum.text != "" && forthnum.text != "" && fifthnum.text != ""{
                self.otp = firstnum.text! + secondnum.text! + thirdnum.text! + forthnum.text! + fifthnum.text!
                User.instance.verifyPhone(id: User.instance._id!, code: self.otp, callback: { (sr) in
                let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
                let appHome: AppHomeViewController = storyboard.instantiateViewController(withIdentifier: "AppHomeViewController") as! AppHomeViewController
                self.navigationController?.pushViewController(appHome, animated: true)

                }, failure: { (error) in
                    print("failed to verify")
                })
                            }else{
                self.displayMyAlertMessage(userMessage: "Please enter the OTP")
            }
        }else{
            self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }
    
    func emailverifyAction() {
        if  SharedData.AppSetting.IS_INTERNET_CONNECTED{
            if firstnum.text != "" && secondnum.text != "" && thirdnum.text != "" && forthnum.text != "" && fifthnum.text != ""{
                self.otp = firstnum.text! + secondnum.text! + thirdnum.text! + forthnum.text! + fifthnum.text!
                User.instance.verifyEmail(id: User.instance._id!, code: self.otp, callback: { (sr) in
                    let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
                    let appHome: AppHomeViewController = storyboard.instantiateViewController(withIdentifier: "AppHomeViewController") as! AppHomeViewController
                    self.navigationController?.pushViewController(appHome, animated: true)
                    
                }, failure: { (error) in
                    print("failed to verify")
                })
            }else{
                self.displayMyAlertMessage(userMessage: "Please enter the OTP")
            }
        }else{
            self.displayMyAlertMessage(userMessage: "No Internet Connection")
        }
    }

    @IBAction func verify(_ sender: Any) {
        if loginWithOTP{
            self.loginwithOTPAction()
        }else if phverify{
            self.phverifyAction()
        }else {
            self.emailverifyAction()
        }
    }
}
