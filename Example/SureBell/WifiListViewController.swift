//
//  WifiListViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class WifiListViewController:UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var cell:WifiListTableViewCell!
    var networks = [Networks]()
    var refreshControl:UIRefreshControl?
    var selected_channel = ""
    var selected_ssid = ""
    var selected_security = ""
    var selected_encryption = ""
    var localAddress = SureConstantResources.AP_DEFAULT_IP

    @IBAction func backAction(_ sender: AnyObject) {
        navigationController?.popToRootViewController(animated: false)
    }
    
    //MARK: *------UIViewController delegate methods------*
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        headerView.layer.masksToBounds = false
        //        headerView.layer.shadowColor = UIColor.black.cgColor
        //        headerView.layer.shadowOpacity = 0.5
        //        headerView.layer.shadowOffset = CGSize(width: -1, height: 1)
        //        headerView.layer.shadowRadius = 1
        //
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UIApplication.shared.isIdleTimerDisabled = false
        tableView.tableFooterView = UIView()
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.tintColor = UIColor.black
        self.refreshControl!.attributedTitle = NSAttributedString(string: "Pull to refresh", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
        self.refreshControl!.addTarget(self, action: #selector(self.refreshControllerAction(_:)), for: UIControlEvents.valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl!)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        //fetchList()
    }
    
    @objc func refreshControllerAction(_ sender:AnyObject){
        //fetchList()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return networks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! WifiListTableViewCell
        if networks.count > 0{
            cell.wifiName.text = networks[indexPath.row].ssid!
            if networks[indexPath.row].security! != "OPEN"{
                
            }else{
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if networks[indexPath.row].security == "OPEN"{
            Device.instance.pairNewDevice(ipAddress: SureConstantResources.AP_DEFAULT_IP, channel: SSID.instance.channel!, ssid: SSID.instance.ssid!, encryption: SSID.instance.encryption!, password: "l2dtech1921681001", security: SSID.instance.security!, location: [21.28236, 19.28262], chime: true, name: "d364", status: { (Int) in
                print("Pairing Successfull")
            })
        }else{
            print(self.networks)
            self.selected_ssid = self.networks[indexPath.row].ssid!
            self.selected_channel = self.networks[indexPath.row].channel!
            self.selected_security = self.networks[indexPath.row].security!
            self.selected_encryption = self.networks[indexPath.row].encryption!
            self.performSegue(withIdentifier: "next", sender: self)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "next") {
            let destinationVC:WifiPasswordViewController = segue.destination as! WifiPasswordViewController
            destinationVC.selected_ssid = self.selected_ssid
            destinationVC.deviceAddress = self.localAddress
            destinationVC.selected_encryption = self.selected_encryption
            destinationVC.selected_security = self.selected_security
            destinationVC.selected_channel = self.selected_channel
        }
    }

    
    

}
