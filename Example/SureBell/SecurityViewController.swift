//
//  SecurityViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class SecurityViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var items = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headerView.layer.shadowColor = UIColor.darkGray.withAlphaComponent(0.50).cgColor
        headerView.layer.masksToBounds = false
        headerView.layer.shadowOffset = CGSize(width: 0.0 , height: 1.0)
        headerView.layer.shadowOpacity = 1.0
        headerView.layer.shadowRadius = 2.0
        headerView.layer.cornerRadius = 2.0
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        
        items.append("Logout from other devices")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.tableFooterView = UIView()
    }

    @objc func backAction(sender: UIBarButtonItem) {
        // custom actions here
        navigationController?.popViewController(animated: true)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Security"
    }
    
    func settingsAction(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SecurityTableViewCell
        cell.titleLabel.text = self.items[indexPath.row]
        if indexPath.row == 0{
            cell.titleImage.image = UIImage(named:"drawer_logout")
        }else {
            
        }
        return cell
    }
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let message = "Are you sure, you want to logout from other devices?"
            
            let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
            myAlert.addAction(cancelAction)
            self.present(myAlert, animated: true, completion: nil)
            
            myAlert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive, handler: { action in
                DispatchQueue.main.async {
                    User.instance.logoutFromOtherDevices(callback: { (sr) in
                        self.displayMyAlertMessage(userMessage: "Successfully logged out from other devices")
                    }, failure: { (error) in
                        
                    })
                }
            }))
        }
    }
}
