//
//  DrawerTableViewCell.swift
//  SureBell_Example
//
//  Created by piOctave on 5/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class DrawerTableViewCell:UITableViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var notificationTimer: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    
    @IBOutlet weak var drawerTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
