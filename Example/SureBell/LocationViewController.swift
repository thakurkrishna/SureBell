//
//  LocationViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell

class LocationViewController: UIViewController {
    @IBOutlet weak var location: UITextField!
    
    @IBAction func done(_ sender: Any) {
        if location.text != "" {
            done.addTarget(self, action: #selector(doneAction), for: .touchUpInside)
        }else {
            self.displayMyAlertMessage(userMessage: "Location is empty")
        }

    }
    @IBOutlet weak var done: UIButton!
    
    func displayMyAlertMessage(userMessage:String){
        let myAlert = UIAlertController(title: "Alert", message: userMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        myAlert.addAction(okAction)
        self.present(myAlert, animated: true, completion: nil)
    }
    
    @objc func doneAction() {
        let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
        let controller: SelectDeviceController = storyboard.instantiateViewController(withIdentifier: "SelectDeviceController") as! SelectDeviceController
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

}
