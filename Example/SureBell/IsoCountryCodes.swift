//
//  IsoCountryCodes.swift
//  IsoCountryCodes
//
//  Created by Krishna Thakur on 8/10/17.
//  Copyright © 2017 PiOctave. All rights reserved.
//

class IsoCountryCodes {
    
    class func find( key:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter(  { $0.alpha2 == key.uppercased() || $0.alpha3 == key.uppercased() || $0.numeric == key } )
        return country[0]
    }
    
    class func searchByName( name:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter( { $0.name == name } )
        
        return (!country.isEmpty) ? country[0] : IsoCountryInfo(name: "", numeric: "", alpha2: "", alpha3: "", calling: "", currency: "", continent: "")
    }
    
    class func searchByCurrency( currency:String ) -> [IsoCountryInfo] {
        let country = IsoCountries.allCountries.filter(  { $0.currency == currency } )
        return country
    }
    
    class func searchByCallingCode( calllingCode:String ) -> IsoCountryInfo {
        var country = IsoCountries.allCountries.filter( { $0.calling == calllingCode } )
        return country[0]
    }
    
    class func searchByAnyKey(searchKey:String) -> [IsoCountryInfo]{
        var filteredList = [IsoCountryInfo]()
        if searchKey.isEmpty {
            filteredList = IsoCountries.allCountries
        } else {
            if Int64(searchKey) != nil{
                filteredList = IsoCountries.allCountries.filter({(country) in
                    let stringMatch = country.calling.lowercased().range(of: searchKey.lowercased())
                    return stringMatch != nil ? true : false
                })
            }else{
                filteredList = IsoCountries.allCountries.filter({(country) in
                    let stringMatch = country.name.lowercased().range(of: searchKey.lowercased())
                    return stringMatch != nil ? true : false
                })
            }
        }
        return filteredList
    }
}

