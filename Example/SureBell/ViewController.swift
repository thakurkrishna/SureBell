//
//  ViewController.swift
//  SureBell
//
//  Created by Krishna Thakur on 04/18/2018.
//  Copyright (c) 2018 Krishna Thakur. All rights reserved.
//

import UIKit
import SureBell

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        Token.instance.getToken(vendorKey: "vendor id", userName: "user email", password: "password", callback: { (token) in
            print(token.access_token)
            print(token.refresh_token)
        }) { (error) in
            let error = error as! APIError
            print(error.code)
            print(error.error_description)
        }

//        Device.instance.inviteUser(id: "device ID", user: "user email", callback: { (device) in
//
//        }) { (error) in
//
//        }
    }
}

