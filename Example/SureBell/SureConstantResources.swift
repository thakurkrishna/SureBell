//
//  SureConstantResources.swift
//  PiCloud
//
//  Created by Krishna Thakur on 18/08/16.
//  Copyright © 2016 PiOctave. All rights reserved.
//

import Foundation
import UIKit

class SureConstantResources{
    static var button_radius:CGFloat = 2.0
    static var button_shadow:CGFloat = 2.0
    
    //Google KEYS
    static var GOOGLE_API_KEY = "AIzaSyDt7hcYM-hYsmSyA3ShbF77Ff2WHYNFUco"
    static var INSTA_BUG_KEY = "eb951452d149abb99a6437831fa69b44"
    static var VENDOR_KEY = "MIIEowIBAAKCAQEAvyv7AVwAw/2wSXupcftnIk95VdG/yR8nTbTOwyADlyhEvbmHQbmMmQoCqUUuQ9QzW5oVGbL/ZTz9oo79iXp6s7GSUQDD3F7xDz1JM1bWuHPbq6OjblK,d58i8we71SCrNImIN+iRKWe53aaHvJhHBqem/eIVvDaBNIDnchiv5i13OVbuGG4pfggW6DNmsF4YVXYDcMSZmXon9TGp8D2GAMWQ+25wQAitfebWkowhTOzUm7eOfGze2kU36Jby0ZQ585RZUoxrA7LUrmZHk9rCOaq5ds9XUrVTi9t,y7kd6vnaZ6i3XevGnmhpE9ibBUEtMbJ5/6HRuSxPDAMzdj1fJxuwIDAQABAoIBAQCRLGzok11heH0u0GHU1P106LVTnnzt1mKad/ur4dvYJhRNn14/YxCKBzgOIyMHD1YQ8emkInIgBzj1AXE3vXMAkpByQwp5hv+4eunjusZEPHdM2,G97j/Gp2p69ByHtM9OjkKLcmkJ8GMNFzFs3pbRTu9ZTR8EVr+3h0w7MNQjD858XSGqn84RWhlm8Z8bCsvo5AJqm8IlvBYm3/wPNAFEs9Uib9niZ4XzgcqlKuMM/KZSdvJujIhSE3FbeHq/5MhroumbvpAVI4XIkYHqkmH0sxIvwFjBS,t2kP62YvDI831DzTFlbOlEZIo4Ua74tXL7Xj8szbiKLApJDJz0/LVZpBAoGBAPKicywt2dolaraFYtGoZthbz/uhJ7cPqA6KL19Yj5xjKButJLrEuyJy8dSPpCX6dyFFYHEY6o+EJY3pM9co7yI7xe1xAF617JUJiSOPBjb4PWJFH2N,tEuS6wl0sfg2VDb+Q6/YIfJLUHZ7pmkAFrgFEwK01hv0qmnMTi9ZXOyb3AoGBAMmz0fM2veoBperdld1iWzYIGI8LePE79VoiWxEhTB4W386mrDrHM3b/hlMIEHHvKAHqwlbfimMODw38+BBYTNiRlXTD2DAGmjIKxiMcwSvXVaeAep,tYANTsNMGZLCIdJ4AN0n1QkqV62PHhsrUp71T1zFNftoBnUD6lna01cIZdAoGAAQK+OhPi0W7Qd/wsQjVSYFpZjnHIaZHOIGlg8yO6TWt4Uk0xVvnU0brq2PjUNThK4L+aPD5fU8O4KFQtXBsWi+z10RxkZQCbAK/ue1BwNTBpiQxCq,jZdGsjQYHT7/XB+gyrGYNr+MdpiWTQd6WBcrjKsHsyXSFyyEgKl1Xm9jFcCgYAmmOd7MCWFaw1R9VchRxoHK+NvNBXPq9aWie59R67mLyBizhg1Iztg8SLGLsve0ZIJU+AeeaZksCMHXTom/o6t/qXn8zYicpD9NHyX6X+5oPo+FGLh,6SQ3mn+58FK2sHeHlJ2L52g4jFqJZtQowL6QtnyEE9O7nImJg2254HkI7QKBgB0oa7IGTbcnpoVDZ3Rn2iqygFkK/jReSehlb0dw8wTX6WMAcdLqxKoBKBRHRA0ZOwK8t+rW5ZB05uNW5Qf2Szk4dAmD9sUvbxXT/mWYg82eclANcBA,C8uSb1qmOOMOOWmMfgihRkmRKTrMyAjxtrw2+nYEHorVzOSPiN9DFr4k6"
    // Application Connection values
    static var SERVER_ADDRESS = ""
    static var BASE_URL = "/api"
    
    //Webrtc url's
    static var SIGNALING_SERVER = ""
    static var TURN_SERVER = "turn:global.turn.twilio.com:3478?transport=udp"
    
    //Device pair url's
    static var AP_PAIR_ADDRESS = "/fcgi-bin/v1/pair"
    static var CHIME_STATUS_UPDATE = "/fcgi-bin/v1/setchime"
    static var AP_UPDATE_ADDRESS = "/fcgi-bin/v1/update-wifi"
    static var AP_CREATE_ADDRESS = "/fcgi-bin/v1/create"
    static var AP_NETWORK_LIST_ADDRESS = "/fcgi-bin/v1/networks"
    static var AP_PING_ADDRESS = "/fcgi-bin/v1/ping"
    static var AP_DEFAULT_IP = "http://192.168.101.1"
    static var SUREBELL_IP = "192.168.101.1"
    // static var EVENT_SHARE_URL = "https://securewithsure.com/project/surebell/?_vurl="
    
    //master server method names
    static var AUTH_TOKEN = "/oauth/token"
    static var ACEESS_TOKEN_REMOVE = "/user/access-token?"
    static var FORGOT_PASS = "/user/forgot-passwd"
    static var RESET_PASS = "/user/reset-passwd"
    static var DEVICE_LIST = "/device"
    static var ONE_EVENT = "/event"
    static var USERS = "/user"
    static var GOOGLE_STORAGE_URL_USER = "https://storage.googleapis.com/pioctave-user/"
    static var AUTH_TOKEN_WITHOUT_LOGIN = "Basic NTY4M2FhMzNlNTY0ZDYxNDI1YjI5Y2IxOiQyYSQxMCR4Rm1Qdzc0NE11SGpkNXFHMGhXOHR1QVhUSUFlZlpuS2dKRHlXOVI1RE9xLk94V1A2M2RXYQ=="
    
    
    // Alert Messages
    static var EMAIL_REQUIRED = "Please enter your Email Address."
    static var PHONE_REQUIRED = "Please enter your Phone Number."
    static var OLD_PASSWORD_REQUIRED = "Please enter your Old Password."
    static var USERNAME_REQUIRED = "Please enter your Username."
    static var PASSWORD_REQUIRED = "Please enter your Password."
    static var FIRST_NAME_REQUIRED = "Please enter your First Name."
    static var LAST_NAME_REQUIRED = "Please enter your Last Name."
    static var NEW_PASSWORD_REQUIRED = "Please enter your New Password."
    static var PASSWORD_SIZE = "Your Password should be greater than 5 characters."
    static var DEVICE_NAME_REQUIRED = "Please enter a Device Name."
    static var DEVICE_NAME_REQUIRED2 = "Device Name can't be empty."
    static var PASSWORD_MATCH_MESSAGE = "Password and Confirm Password should be the same."
    static var CONFIRM_PASSWORD_REQUIRED_MESSAGE = "Confirm your Password."
    static var INVALID_EMAIL = "Please enter a valid Email Address "
    static var INVALID_PHONE = "Please enter a valid Phone Number. Special characters and 0/+ at the beginning are not allowed."
    static var INVALID_EMAIL_PHONE = "Please enter a valid Email Address or Phone Number."
    static var LOCATION_REQUIRED = "Please select your Device Location."
    static var ALERT_TITLE = "Alert!"
    static var VERIFICATION_CODE_SENT_MESSAGE = "We have sent you the link to reset, please check your mail."
    static var VERIFICATION_CODE_SENT_PHONE_MESSAGE = "We have sent you the code to reset your password, please check your phone."
    static var PASSWORD_RESET_SUCCESS = "You have changed your password successfully."
    static var BASIC_ERROR_MSG = "Something went wrong.Please try again."
    static var NO_EVENT_ON_DATE = "No events found on this date."
    static var NO_MORE_EVENT = "No more events found on this date."
    static var NO_DIVICES_FOUND = "No devices found, please add."
    static var NO_INTERNET = "Problem with network connection. Please try again."
    static var EMAIL_INVITED_SUCCESS_MSG_1 = "You have been successfully invited "
    static var RESEND_INVITE_MSG = "The invitation has been sent to "
    static var DELETE_USER_MSG = " removed successfully."
    static var DELETE_INVITE_MSG = "Invite removed successfully."
    static var MAKE_ADMIN_MSG = "Admin rights given to  "
    static var DEVICE_REMOVED_MSG = "Device removed successfully"
    static var CONNECTION_ERROR_TITLE = "Connection Error!"
    static var RESET_PASSWORD_VERIFICATION_FAILED = "Verification code entered is incorrect."
    static var CONNECTIONERROR_REFRESH = "No Internet connection! Check your internet connection and refresh the device list."
    static var ERROR_MESSAGE = "Error"
    static var ADD_DEVICE_NAME_TITLE = "Add Device Name"
    static var ENTER_DEVICE_PASSWORD_MESSAGE = "Enter your device password."
    static var PASSWORD_REQUIRED_MESSAGE = "Password required."
    static var CODE_REQUIRED_MESSAGE = "Verification code required."
    static var USER_NAME_PASSWORD_WRONG = "Your Username or Password is incorrect."
    static var ALERT_DIALOG_HIEGHT = "dsjhgjdshgjdhsg dhjsgfhjds ghjfg dsfdhjsg fhjdsgfhjd sfdhjf hjd fdhjg sfhdgfhjdgf hjgd fhjdg fsfhgds fhjg dhjsgf dhjsg fhjdsg fdhg sfgdsfdsfdsfdsfhjdg swkjfg s hdb cdbsddchbjsb bjdbdjksbvdsjhkb vdhjsdsh jhcjghcjjj vhhj hjvj hvhj vhvhvhhjhvjhcbhd cbhjbv ksdksjbvjdskb kjgjhhjfhjf jfh fjf jvjsdbvjkdsbvkdjjdsg fjgsfdsg fjsks dwkejh kfjghew kjfgewkjgf jkgf ewg fgfewjgfjehgf dfghjs"
    static var CONFIGURE_WITH_CHIME = "Configure with a chime."
    static var CONFIGURE_WITHOUT_CHIME = "Configure without a chime."
    static var WITHCHIME_MESSAGE = "This setting will enable the exsiting Sure Adapter along with Musical chime installed."
    static var WITOUT_CHIME_MESSAGE = "This setting will disable the exsiting Sure Adapter along with Musical chime installed."
    static var NOT_CONNECTED_WITH_HOME_WIFI = "You are not connected with your home Wi-Fi. Go to Settings and connect your device."
    static var WIFI_CONNECTED_NOTIFICATION_MESSAGE = "Go to the Sûre app to select home Wi-Fi."
    static var NO_NEAR_BY_WIFI_FOUND = "No Wi-Fi network found."
    static var WEAK_WIFI_CONFIG_ALERT = "The above LED pattern indicates. the Wi-Fi signal is weak"
    static var PASSWORD_INCORRECT_ALERT = "The above LED pattern indicates. the Wi-Fi password entered is incorrect."
    static var DEVICE_CONFIGURATION_SUCCESS_ALERT = "The above LED pattern indicates device has been configured successfully."
    static var WEAK_WIFI_SIGNAL_LED_MESSAGE = "This pattern signals that your device configuration is incomplete. Weak Wi-Fi Signal is reason for the same."
    static var WRONG_WIFI_PASSWORD_LED_MESSAGE = "When a Wrong Password is administered, configuration will be incomplete and the Sure Bell shall display this particular pattern."
    static var DEVICE_CONFIGURED_SUCCESSFULL_LED_MESSAGE = "Well done! You have now Successfully Configured your Sûre bell."
    static var EVENT_VIDEO_NOT_READY_MESSAGE = "Video is not yet ready to play, please try again after sometime."
    static var PHOTO_ACCESS_PERMISSION = "Access to Photos has not been given. Please enable the same from Settings."
    static var SCREENSHOT_CAPTURED_MESSAGE = "Snapshot taken successfully."
    static var ROI_ADD_MESSAGE = "We currently support only one region of interest."
    //Video call messages
    static var VIDEO_CALL_COULD_NOT_CONNECTED = "Unable to contact device, please try again."
    static var VIDEO_CALL_DEVICE_BUSY_MESSAGE = "Device is busy, please try again later."
    static var VIDEO_CALL_DEVICE_OFFLINE = "Device is not reachable, please try again."
    static var CALL_DISCONNECTED = "Call disconnected, please try again."
    static var UPDATE_DEVICE_NAME_ALERT_TITLE = "Device name" // TODO use for alert placeholder should be used.
    static var UPDATE_DEVICE_NAME_BODY_MESSAGE = "Please edit your device name." // TODO remove it
    static var REVERT_SETTINGS_MESSAGE = "Do you want to save your changes?"
    static var REMOVE_DEVICE_CONFIRMATION_MESSAGE = "Are you sure want to remove this device?"
    static var USER_ALREADY_EXIST = "User already exists, do you want to login?"
    static var LOGOUT_CONFIRMATION_MESSAGE = "Are you sure, you want to logout? "
    static var WRONG_OTP = "Please enter the valid OTP number sent to your phone number."
    static var USER_DOES_NOT_EXIST = "Looks like you have not signed up yet, please Sign Up."
    static var LOGIN_FAILED_GOTOLOGIN = "Something went wrong, please login again."
    
    static var NO_DEVICE_CONFIGURED = "No devices have been configured with the app as of yet."
    static var SUCCESSFULY_CHANGED_PASWORD = "You have successfully changed your password."
    static var OLD_PASSWORD_DOESNOT_MATCHED = "Your Old Password is not correct."
    static var OLD_AND_NEW_PASSWORD_IS_SAME = "Your new password is same as old password."
    static var PROFILE_UPDATED_SUCCESS = "Profile updated successfully."
    static var ENABLE_PROFILE_UPDATE = "To update your profile, please tap on Edit."
    // static var PHONE_VERIFICATION_PENDDING = "Your phone verification is pending."
    static var PHONE_VERIFICATION_FAILED = "Phone Verification failed."
    static var CHECK_YOUR_SMS_FOR_OTP = "Please check your Messages for verification code."
    static var OTP_ALERT_DESCRIPTION = "Enter the five digit code received on your phone number.\n\n\n\n"
    static var PHONE_NOT_VERIFIED_MESSAGE = "Your phone number is not verified."
    static var EMAIL_NOT_VERIFIED_MESSAGE = "Your email is not verified."
    static var KEYCHAIN_ALLOW_MESSAGE = "Allow login through Keychain Access."
    static var DEVICE_CONFIGURATION_UPDATED = "Device configuration updated."
    static var ADMIN_ACCESS_REMOVED_MESAGE = "Your admin rights have been revoked. Please contact an admin."
    //Error message
    static var SERVICE_CONNECTION_ERROR = "#COMMUNICATION_ERROR"
    static var GOOGLE_ANALYTICS_ERROR_MESSAGE = "Error configuring Google services:"
    static var CHIME_SETTING_WARNIG = "Sure Bell can work with AC mechanical chimes.  Incorrect configuration of the device with chime can lead to Short circuit damages. Kindly refer the installation and setup guide for more details."
    static var DISABLE_MOTION_CLOUD_UPLOAD_SETTING_WARNIG = "Disabling cloud uploads will stop device from uploading any motion event to cloud. You may miss events for this time."
    static var DISABLE_RING_CLOUD_UPLOAD_SETTING_WARNIG = "Disabling cloud uploads will stop device from uploading any ring event to cloud. You may miss events for this time."
    static var ENABLE_MOTION_CLOUD_UPLOAD_SETTING_WARNIG = "Device will now start to upload motion events to cloud."
    static var ENABLE_RING_CLOUD_UPLOAD_SETTING_WARNIG = "Device will now start to upload ring events to cloud."
    static var ENABLE_CLOUD_UPLOAD_SETTING_WARNIG = "Device will now start to upload events to cloud."
    static var DISABLE_CLOUD_UPLOAD_SETTING_WARNIG = "Disabling cloud uploads will stop device from uploading any event to cloud. You may miss events for this time."
    static var ASK_REMOVING_ADMIN_MSG = "Do you want to revoke admin rights for this user?"
    static var ASK_GIVING_ADMIN_RIGHTS_MSG = "Do you want to give admin rights to this user?"
    
    // Tool Tip Messages
    static var EVENTLIST_FILTER_NAV_ITEM_TOOLTIP = "Filter only those events which are relevant to you. Tap to dismiss."
    static var EVENTLIST_FILTER_DATE_TOOLTIP = "You can also check all the events on a particular date. Tap to dismiss."
    static var VIDEO_CALL_SCRRENSHOT_TOOLTIP = "Take a snapshot of a live event. Tap to dismiss."
    static var VIDEO_CALL_PINCH_TO_ZOOM_TOOLTIP = "Use pinch to zoom in or zoom out of live view. Tap to dismiss."
    static var DEVICELIST_LIVEBUTTON_TOOLTIP = "Click Live View to get a real time view of your Sûre Bell. Tap to dismiss."
    static var DEVICELIST_EVENTLISTNAV_TOOLTIP = "Check all your motion based and doorbell events. Tap to dismiss."
    static var DRAWER_RING_TOOLTIP = "Stop or Start Ring notifications for a specific period of time. Tap to dismiss."
    static var DRAWER_MOTION_TOOLTIP = "Stop or Start Motion notifications for a specific period of time. Tap to dismiss."
    static var SHAREDEVICE_SEARCHBAR_TOOLTIP = "You can invite someone to access this device with an email or mobile. Tap to dismiss."
    static var PROFILE_EDIT_TOOLTIP = "Update your Profile. Tap to dismiss."
    static var PROFILE_PHOTO_TOOLTIP = "Change your Profile Picture. Tap to dismiss."
    
    
    
    
    // Walkthrough message
    static var WALKTHROUGH_MOTION_ROI = "Select your region of interest to safeguard those pesky hiding spots around your home."
    static var WALKTHROUGH_EVENT_LIST = "Footage captured by your Sûre Bell can be accessed anytime. You can view and share them no matter where you are."
    static var WALKTHROUGH_CALL = "Never miss a call from your visitors. Active notifications ensure that you are always aware of what happens around your home."
    static var WALKTHROUGH_DEVICE_INFO = "Access and manage all your smart devices that are part of the Sûre network."
    
    
    
    //NSUSERDEFAULTS KEYS
    static var SELECTED_ALERT = "SELECTED_ALERT"
    static var SELECTED_RING = "SELECTED_RING"
    static var MOTION_DATE_KEY = "MOTIONDATE"
    static var MOTION_TIME_KEY = "MOTIONTIME"
    static var RING_DATE_KEY = "RINGDATE"
    static var RING_TIME_KEY = "RINGTIME"
    static var PUSH_TOKEN_KEY = "PUSHTOKEN"
    static var MEDIUM_ID_KEY = "MEDIUMID"
    static var FIRST_DEVICE_KEY = "FIRSTDEVICE"
    static var GIHASKEY = "githash_preference"
    static var USER_LOGIN_STATUS_KEY = "USER_LOGIN_STATUS"
    static var CONNECTED_SSID = "CONNECTEDSSID"
    static var WALK_THROUGH_SCREEN = "WALK_THROUGH_SCREEN"
    static var DEVICE_CONFIGURED_REFRESH = "DEVICE_CONFIGURED_REFRESH"
    static var ADD_DEVICE_CHIME = "ADD_DEVICE_CHIME"
    static var ADD_DEVICE_NAME = "ADD_DEVICE_NAME"
    static var ADD_DEVICE_LOC_LAT = "ADD_DEVICE_LOC_LAT"
    static var ADD_DEVICE_LOC_LNG = "ADD_DEVICE_LOC_LNG"
    static var EVENTLIST_FILTER_NAV_ITEM_TOOLTIP_KEY = "EVENTLIST_FILTER_NAV_ITEM_TOOLTIP"
    static var EVENTLIST_FILTER_DATE_TOOLTIP_KEY = "EVENTLIST_FILTER_DATE_TOOLTIP"
    static var VIDEO_CALL_SCRRENSHOT_TOOLTIP_KEY = "VIDEO_CALL_SCRRENSHOT_TOOLTIP"
    static var VIDEO_CALL_PINCH_TO_ZOOM_TOOLTIP_KEY = "VIDEO_CALL_PINCH_TO_ZOOM_TOOLTIP"
    static var DEVICELIST_LIVEBUTTON_TOOLTIP_KEY = "DEVICELIST_LIVEBUTTON_TOOLTIP"
    static var SHAREDEVICE_SEARCHBAR_TOOLTIP_KEY = "SHAREDEVICE_SEARCHBAR_TOOLTIP"
    static var PROFILE_EDIT_TOOLTIP_KEY = "PROFILE_EDIT_TOOLTIP"
    static var PROFILE_PHOTO_TOOLTIP_KEY = "PROFILE_PHOTO_TOOLTIP"
    static var DRAWER_RING_TOOLTIP_KEY = "DRAWER_RING_TOOLTIP"
    static var DEVICELIST_EVENTLISTNAV_TOOLTIP_KEY = "DEVICELIST_EVENTLISTNAV_TOOLTIP"
    static var DRAWER_MOTION_TOOLTIP_KEY = "DRAWER_MOTION_TOOLTIP"
    static var USER_LOGIN_STATUS = "USER_LOGIN_STATUS"
    static var ADD_DEVICE_LOC = "ADD_DEVICE_LOC"
    static var DEVICE_TOKEN_UPDATED = "DEVICE_TOKEN_UPDATED"
    static var PROFILE_PIC_KEY = "PROFILE_PIC"
    static var NOTIFICATIONONOFFFORTIME = "NOTIFICATIONONOFFFORTIME"
    static var REMEMBERED_USER = "REMEMBERED_USER"
    static var REMEMBERED_USER_TOUCH = "REMEMBERED_USER_TOUCH"
    
    //HTTP METHODS
    static var GET_METHOD = "GET"
    static var POST_METHOD = "POST"
    static var PUT_METHOD = "PUT"
    static var DELETE_METHOD = "DELETE"
    
    //HTTP Content type
    static var CONTENT_TYPE_JSON_VALUE = "application/json; charset=utf-8"
    static var CONTENT_TYPE_URLENCODED_VALUE = "application/x-www-form-urlencoded"
    static var CONTENT_TYPE_KEY = "Content-type"
    static var AUTHORIZATION_KEY = "Authorization"
    static var BEARER_KEY = "Bearer "
    
    //Storyboard id's
    static var SPLASH_VIEW_CONTROLLER = "SplashViewController"
    static var EVENT_VIEW_CONTROLLER = "EventViewController"
    static var VIDEO_VIEW_CONTROLLER = "VideoStreamViewController"
    static var LOGIN_VIEW_CONTROLLER = "LoginViewController"
    static var DEVICE_LIST_VIEW_CONTROLLER = "DeviceListViewController"
    static var ADD_DEVICE_VIEW_CONTROLLER = "AddDeviceViewController"
    static var NAVIGATION_VIEW_CONTROLLER = "NavigationController"
    static var STORE_VIEW_CONTROLLER = "StoreViewController"
    static var PRODUCT_DETAIL_VIEW_CONTROLLER = "ProductDetailViewController"
    static var WIFI_SETUP_VIEW_CONTROLLER = "WiFiSetupViewController"
    static var RESET_PASSWORD_VIEW_CONTROLLER = "ResetPasswordViewController"
    static var REGISTER_VIEW_CONTROLLER = "RegisterViewController"
    static var STORYBOARD_NAME = "Main"
    
    //CoreData Classes
    static var LOGIN_CRED = "LoginCred"
    static var USERDETAILS = "LoggedInUser"
    static var USEREMAIL = "email"
    static var ACCESS_TOKEN_CORE_DATA_KEY = "access_token"
    static var REFRESH_TOKEN_CORE_DATA_KEY = "refresh_token"
    
    //VALUES
    static var EMAIL_VALIDATION_RANGE = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    static var EMAIL_HINT_DETAIL = "xxxx@xxx.xxx"
    static var EMAIL_PLACEHOLDER = "       email"
    static var OK_LBL = "Ok"
    static var CANCEL_LBL = "Cancel"
    
    class NotificationType{
        static var UPDATE = "update"
        static var ADS = "ads"
        //DEVICE NOTIFICATIONS
        static var EVENT_DEVICE_REGISTERED = "_newDeviceRegistered"
        static var EVENT_DEVICE_LAST_IMAGE_UPDATED = "_deviceLastImageUpdated"
        static var EVENT_DEVICE_MOTIONSETTINGS_CHANGED = "_deviceMotionSettingsChanged"
        static var EVENT_DEVICE_ADMIN_ADDED = "_adminForDeviceAdded"
        static var EVENT_DEVICE_ADMIN_REMOVED = "_adminForDeviceRemoved"
        static var EVENT_DEVICE_USER_ADDED = "_userForDeviceAdded"
        static var EVENT_DEVICE_INVITE_REMOVED = "_inviteForDeviceRemoved"
        static var EVENT_DEVICE_USER_REMOVED = "_userForDeviceRemoved"
        static var EVENT_DEVICE_UPDATED = "_deviceUpdated"
        static var EVENT_DEVICE_CLOUD_UPLOAD_STATUS = "_deviceCloudUploadStatusChanged"
        static var EVENT_DEVICE_INVITE_ACCEPTED = "_deviceInviteAccepted"
        static var EVENT_DEVICE_INVITE_REJECTED = "_deviceInviteRejected"
        static var EVENT_DEVICE_CONNECTIVITY_GAINED = "_deviceConnectivityGained"
        static var EVENT_DEVICE_CONNECTIVITY_LOST = "_deviceConnectivityLost"
        static var EVENT_DEVICE_AUDIOSETTINGS_CHANGED = "_deviceAudioSettingsChanged"
        static var EVENT_DEVICE_VIDEOSETTINGS_CHANGED = "_deviceVideoSettingsChanged"
        static var EVENT_DEVICE_CHIMESETTINGS_CHANGED = "_deviceChimeSettingsChanged"
        static var EVENT_DEVICE_FIRMWARE_CHANGED = "_deviceFirmwareChanged"
        static var EVENT_DEVICE_CLOUD_PLAN_UPDATED = "_deviceCloudPlanUpdated"
        // Events related notification
        static var EVENT_PIEVENT_REGISTERED = "_newEventRegistered"
        static var EVENT_PIEVENT_COMMENT_PUSHED = "_eventCommentPushed"
        static var EVENT_PIEVENT_STATUS_UPDATED = "_eventStatusUpdated"
        static var EVENT_PIEVENT_LIKE_PUSHED = "_eventlikePushed"
        static var EVENT_PIEVENT_MEDIA_UPDATED = "_eventMediaUpdated"
        static var EVENT_PIEVENT_MEDIA_SUMMARY_UPDATED = "_eventMediaSummaryUpdated"
        static var EVENT_PIEVENT_UPDATED = "_eventUpdated"
        
        //UserProfile
        static var EVENT_UNAUTHORISED_ACCESS_TO_DEVICE = "_unauthorisedAccess"
        static var EVENT_DEVICE_USER_UPDATED = "_userUpdated"
        static var UPDATE_SHAREVIEW = "_updateShareView"
        
        static var PROFILE_PIC_DOWNLOADED = "PROFILE_PIC_DOWNLOADED"
        static var POP_TO_HOME_ADMIN_REMOVED = "POP_TO_HOME_ADMIN_REMOVED"
        static var USER_INFO_UPDATED = "USER_INFO_UPDATED"
        static var APP_IS_ACTIVE = "APP_IS_ACTIVE"
        static var DEVICE_LIST_FOR_WATCH = "DEVICE_LIST_FOR_WATCH"
        
        
    }
    
    
    class SupportInfo {
        static var SUPPORT_PHONE_NUMBER = "9036600404"
        static var SUPPORT_EMAIL_ADDRESS = "support@securewithsure.com"
    }
}

extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+"]
        return Set(self).isSubset(of: nums)
    }
}
