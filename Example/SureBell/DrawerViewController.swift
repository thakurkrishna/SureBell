//
//  DrawerViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import CoreData
import SureBell


class DrawerViewController:UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    typealias SettingInfo = (title: String, status:Bool, timerTime:String, titleImage:String)
    fileprivate var items = [SettingInfo]()
    var index:IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let customBackButton = UIBarButtonItem(image: UIImage(named: "back") , style: .plain, target: self, action: #selector(backAction(sender:)))
        customBackButton.imageInsets = UIEdgeInsets(top: 2, left: -8, bottom: 0, right: 0)
        navigationItem.leftBarButtonItem = customBackButton

        
        items.append(("Add New Device",true,"","drawer_sure"))
        items.append(("Ring Notifications",true,"","drawer_ringnotification"))
        items.append(("Motion Notifications",true,"","drawer_notification"))
        items.append(("Ringtone",true,"","drawer_ringtone"))
        items.append(("Store",true,"","drawer_store"))
        items.append(("Help",true,"","drawer_help"))
        items.append(("Support",true,"","drawer_support"))
        items.append(("Logout",true,"","drawer_logout"))
        
        tableView.dataSource = self
        tableView.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.barStyle = UIBarStyle.black
        navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.title = "Settings"
    }
    
    @objc func backAction(sender: UIBarButtonItem) {
        // custom actions here
        navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        //CellAnimator.animateCell(cell: cell, withTransform: CellAnimator.TransformTilt, andDuration: 0.3)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DrawerTableViewCell
        
        if items.count > 0{
            cell.drawerTitle.text =  items[indexPath.row].title
            cell.itemImage.image = UIImage(named:items[indexPath.row].titleImage)
            if items[indexPath.row].title == "Ring Notifications"{
                cell.switchControl.isHidden = false
                cell.notificationTimer.isHidden = true
                cell.switchControl.tag = indexPath.row
                if items[indexPath.row].status{
                    cell.switchControl.isOn = true
                }else{
                    cell.switchControl.isOn = false
                }
//                cell.switchControl.addTarget(self, action: #selector(DrawerViewController.switchIsChangedRing(_:)), for: UIControlEvents.valueChanged)
            }else if items[indexPath.row].title == "Motion Notifications"{
                cell.switchControl.isHidden = false
                cell.notificationTimer.isHidden = true
                cell.switchControl.tag = indexPath.row
                if items[indexPath.row].status{
                    cell.switchControl.isOn = true
                }else{
                    cell.switchControl.isOn = false
                }
//                cell.switchControl.addTarget(self, action: #selector(DrawerViewController.switchIsChangedMotion(_:)), for: UIControlEvents.valueChanged)
            }else{
                cell.notificationTimer.isHidden = true
                cell.switchControl.isHidden = true
            }
        }
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _ = tableView.cellForRow(at: indexPath) as! DrawerTableViewCell
        if items[indexPath.row].title == "Logout"{
            let message = SureConstantResources.LOGOUT_CONFIRMATION_MESSAGE
            
            // Create the dialog
            let myAlert = UIAlertController(title: message, message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil)
            myAlert.addAction(cancelAction)
            self.present(myAlert, animated: true, completion: nil)
            
            myAlert.addAction(UIAlertAction(title: "Logout", style: UIAlertActionStyle.destructive, handler: { action in
                DispatchQueue.main.async {
                    User.instance.deleteNotificationToken(token: "token", callback: { (user) in
                        Token.instance.deleteToken()
                        let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
                        let login: LoginViewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        self.navigationController?.pushViewController(login, animated: false)

                    }, failure: { (error) in
                        
                    })
                }

            }))
        }else if items[indexPath.row].title == "Add New Device"{
            
            let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
            let deviceadd: DeviceNameViewController = storyboard.instantiateViewController(withIdentifier: "DeviceNameViewController") as! DeviceNameViewController
            self.navigationController?.pushViewController(deviceadd, animated: true)
        }
        else if items[indexPath.row].title == "Support"{
            let uiAlert = UIAlertController(title: "Choose an action", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
            
            uiAlert.addAction(UIAlertAction(title: "Call us", style: .default, handler: { action in
                if let url = URL(string: "tel://\(SureConstantResources.SupportInfo.SUPPORT_PHONE_NUMBER)"), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }))
            uiAlert.addAction(UIAlertAction(title: "reach us on Email", style: .default, handler: { action in
                let email = "\(SureConstantResources.SupportInfo.SUPPORT_EMAIL_ADDRESS)"
                let url = URL(string: "mailto:\(email)")
                UIApplication.shared.open(url!)
            }))
            uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            
            uiAlert.popoverPresentationController?.sourceView = self.view
            uiAlert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0,y:  self.view.bounds.size.height / 2.0,width: 1.0,height: 1.0)
            self.present(uiAlert, animated: true, completion: nil)
        }
    }
}

