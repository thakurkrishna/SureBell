//
//  Networks.swift
//  PiCloud
//
//  Created by Krishna Thakur on 02/09/16.
//  Copyright © 2016 PiOctave. All rights reserved.
//

import Foundation

class Networks{
    var bssid:String?
    var connected:String?
    var capability:String?
    var ssid:String?
    var mode:String?
    var frequency:String?
    var channel:String?
    var sigstrength:Int?
    var security:String?
    var encryption:String?
    //
    //    static var instance = Networks()
    //
    //    static func getInstance() -> Networks {
    //        return self.instance
    //    }
    
    required init(bssid:String?,connected:String?,capability:String?,ssid:String?,mode:String?,frequency:String?,channel:String?,sigstrength:Int?,security:String?,encryption:String?){
        
        self.bssid = bssid
        self.connected = connected
        self.capability = capability
        self.ssid = ssid
        self.mode = mode
        self.frequency = frequency
        self.channel = channel
        self.sigstrength = sigstrength
        self.security = security
        self.encryption = encryption
    }
    
}

