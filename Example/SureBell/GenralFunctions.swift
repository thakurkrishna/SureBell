//
//  GenralFunctions.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import Foundation
import UIKit
import SystemConfiguration.CaptiveNetwork

class GenralFunctions: NSObject {
    
    
    func convertDateFormaterForEventList(_ date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let datee = dateFormatter.date(from: date)
        dateFormatter.dateStyle = .medium
        dateFormatter.doesRelativeDateFormatting = true
        
        let timeStamp = dateFormatter.string(from: datee!)
        return timeStamp
    }
    
    
    func getHumanReadbelTime(_ date: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let datee = dateFormatter.date(from: date)
        dateFormatter.dateStyle = .medium
        dateFormatter.doesRelativeDateFormatting = true
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let time = timeFormatter.date(from: date)
        timeFormatter.dateFormat = "h:mm a"
        
        let timeFormated = "\(dateFormatter.string(from: datee!)), \(timeFormatter.string(from: time!))"
        return timeFormated
    }
    
    // This method will force you to use base on how you configure.
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            //delegate.orientationLock = orientation
        }
    }
    
    // This method done pretty well where we can use this for best user experience.
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
        self.lockOrientation(orientation)
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
    
    static func rotateScreen(_ rotateOrientation:UIInterfaceOrientation) {
        UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
    }
    
    
    func getNotificationTime(_ date: String)->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let datee = dateFormatter.date(from: date)
        dateFormatter.dateStyle = .long
        //dateFormatter.doesRelativeDateFormatting = true
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let time = timeFormatter.date(from: date)
        timeFormatter.dateFormat = "h:mm a"
        
        let timeFormated = "\(timeFormatter.string(from: time!)), \(dateFormatter.string(from: datee!))"
        return timeFormated
    }
    
    
    static func get24HoursTime(_ date: Date)->String{
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        let time = formatter.string(from: date)
        print(date,time)
        return time
    }
    
    static func convert24To12( time:String) ->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "H:mm"
        let date12 = dateFormatter.date(from: time)!
        
        dateFormatter.dateFormat = "h:mm a"
        let date22 = dateFormatter.string(from: date12)
        return date22
    }
    
    func convertWebDate(_ date: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        
        let date = dateFormatter.date(from: date)
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        return date!
    }
    
    static func getCurrentMillis()->Int64 {
        return Int64(Date().timeIntervalSince1970 * 1000)
    }
    
    static func getSecondsOfDate(_ date: Date)->Int64{
        return Int64(date.timeIntervalSince1970)
    }
    
    func generalNSDateToString(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        return dateFormatter.string(from: date)
    }
    
    
    static func timeTODate(time:String)->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .short
        dateFormatter.dateFormat = "k:mm"
        let date = dateFormatter.date(from: time)
        return date!
    }
    
    func generalStringToNSDate(_ date: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
        let date = dateFormatter.date(from: date)
        return date!
    }
    
    //    func getTimeOnly(date: String) -> NSDate{
    //        let dateFormatter = NSDateFormatter()
    //        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"//this your string date format
    //        let date = dateFormatter.dateFromString(date)
    //        return date!
    //    }
    
    static func convertDateFormaterForRequestString(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let timeStamp = dateFormatter.string(from: date)
        return timeStamp
    }
    
    static func convertDateFormaterForDisplayString(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let timeStamp = dateFormatter.string(from: date)
        return timeStamp
    }
    
    
    func convertDateFormaterForRequest(_ date: Date) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let formatedDate = dateFormatter.string(from: date)
        let convertedDate = dateFormatter.date(from: formatedDate)
        let epochTime = Int64((convertedDate!.timeIntervalSince1970) * 1000)
        return "\(epochTime)"
    }
    
    
//    static func imageWithGradient(_ img:UIImage!) -> UIImage{
//
//        UIGraphicsBeginImageContext(img.size)
//        let context = UIGraphicsGetCurrentContext()
//
//        img.draw(at: CGPoint(x: 0, y: 0))
//
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//        let locations:[CGFloat] = [0.50, 1.0]
//        //1 = opaque
//        //0 = transparent
//        //let bottom = UIColor.appPrimary.cgColor
//        let top = UIColor.clear.cgColor
//        //let colours = [top, bottom] as CFArray
//        //let gradient = CGGradient(colorsSpace: colorSpace,colors: colours, locations: locations)
//        
//
//        let startPoint = CGPoint(x: img.size.width/2, y: 0)
//        let endPoint = CGPoint(x: img.size.width/2, y: img.size.height)
//
//        context!.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: CGGradientDrawingOptions(rawValue: UInt32(0)))
//
//        let image = UIGraphicsGetImageFromCurrentImageContext()
//
//        UIGraphicsEndImageContext()
//
//        return image!
//    }
    
    
    // Convert from NSData to json object
    static func dataToJSON(_ data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .allowFragments)
        } catch let myJSONError {
            print("JSONPARSEERROR",myJSONError)
        }
        return nil
    }
    // Convert from JSON to nsdata
    static func jsonToData(_ json: AnyObject) -> Data? {
        do {
            return try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
        } catch let myJSONError {
            print("JSONPARSEERROR",myJSONError)
        }
        return nil;
    }
    
    static func randomStringWithLength (_ len : Int) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 0 ..< len{
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString as String
    }
    
    func offsetFrom(date: Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: Date());
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    static func offsetOfTwoDate(fromDate: Date, toDate: Date) -> String {
        
        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: fromDate, to: toDate);
        
        let seconds = "\(difference.second ?? 0)s"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours
        
        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }
    
    func removeSpecialCharsFromString(_ text: String) -> String {
        let okayChars : Set<Character> =
            Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890")
        return String(text.filter {okayChars.contains($0) })
    }
    
    
    func secondsToHoursMinutesSeconds (seconds : Int64) -> String {
        let sec = (seconds % 3600) % 60
        let min = (seconds % 3600) / 60
        let hr = seconds / 3600
        return "\(hr) : \(min) : \(sec)"
    }
    
    func humanReadableTime(seconds : Int64) -> String {
        let sec = (seconds % 3600) % 60
        let min = (seconds % 3600) / 60
        let hr = seconds / 3600
        let days = (seconds / 86400)
        
        if days > 0{
            return "\(days) Days"
        }
        
        if hr > 0{
            return "\(hr) Hours"
        }
        
        if min > 0{
            return "\(min) Minutes"
        }
        
        if sec > 0{
            return "\(sec) Seconds"
        }
        
        return "unknown"
    }
    
    
    static func fetchSSIDInfo() -> [String] {
        var wifiInfoList = [String]()
        if let interfaces:CFArray = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                if unsafeInterfaceData != nil {
                    
                    let interfaceData = unsafeInterfaceData! as Dictionary!
                    wifiInfoList.append(((interfaceData as? [String : AnyObject])?["SSID"])! as! String)
                    
                    wifiInfoList.append(((interfaceData as? [String : AnyObject])?["BSSID"])! as! String)
                }
            }
        }
        return wifiInfoList
    }
}







