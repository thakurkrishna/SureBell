//
//  AppHomeViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 5/16/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import SureBell
import CoreData

class AppHomeViewController: UIViewController {
    
    @IBOutlet weak var profilepic: UIButton!
    @IBAction func profilepicAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
        let controller: ProfileViewController = storyboard.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(controller, animated: false)
    }
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var settingear: UIImageView!
    @IBOutlet weak var settingcontainer: UIView!
    
    //let moc = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    //var user = [UserDetails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Devices"
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.settingsAction))
        settingcontainer.addGestureRecognizer(tapGestureRecognizer)

        
    }
    
    @objc func settingsAction(_ sender: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.2, animations: {
            self.settingear.transform = CGAffineTransform(rotationAngle: CGFloat.pi / 1)
            }, completion: {
            (value: Bool) in
            let storyboard = UIStoryboard(name: SureConstantResources.STORYBOARD_NAME, bundle: Bundle.main)
            let controller: DrawerViewController = storyboard.instantiateViewController(withIdentifier: "DrawerViewController") as! DrawerViewController
//            self.radialPushViewController(controller, 0.4, CGRect(x:self.view.frame.width - 60,y:60,width:10,height:10), revealType: .reveal, {
//
//            })
            self.navigationController?.pushViewController(controller, animated: false)
        })
    }

    
    override func viewWillAppear(_ animated: Bool) {
        self.userName.text = User.instance.firstname! + " " + User.instance.lastname!
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
