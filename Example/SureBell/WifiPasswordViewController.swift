//
//  WifiPasswordViewController.swift
//  SureBell_Example
//
//  Created by piOctave on 6/5/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit

@objc protocol ViewControllerDelegate{
    func keyboardWillShowWithSize(_ size:CGSize, andDuration duration:TimeInterval)
    func keyboardWillHideWithSize(_ size:CGSize,andDuration duration:TimeInterval)
}

class WifiPasswordViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var headingMesage: UILabel!
    
    @IBOutlet weak var done_btn: UIButton!
    @IBOutlet weak var password_input: UITextField!
    
    var keyboardDelegate:ViewControllerDelegate?
    let resources  = SureConstantResources()
    var selected_channel = ""
    var selected_ssid = ""
    var selected_security = ""
    var selected_encryption = ""
    let resource = SureConstantResources()
    var btnLeftMenu: UIButton?
    var passwordiconClick = true
    var deviceAddress = SureConstantResources.AP_DEFAULT_IP
    
    
    
    @IBAction func doneAction(_ sender: Any) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        btnLeftMenu = UIButton()
        let origImage = UIImage(named: "ic_eye_closed");
        let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        btnLeftMenu?.setImage(tintedImage, for: UIControlState())
        btnLeftMenu?.tintColor = UIColor.darkGray
        btnLeftMenu?.contentMode = .center
        btnLeftMenu!.addTarget(self, action: #selector(self.togglePasswordVisible(_:)), for: UIControlEvents.touchUpInside)
        btnLeftMenu!.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        password_input.rightView = btnLeftMenu
        password_input.rightViewMode = .always
        
        self.headingMesage.text = "WIFI PASSWORD FOR \(self.selected_ssid)"
        self.password_input.placeholder = "Enter WiFi Password for \(self.selected_ssid)"
        done_btn.backgroundColor = UIColor.darkGray
        self.password_input!.delegate = self
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool{
        if textField == self.password_input {
            //nextAction()
        }
        return true
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        let imageView = UIImageView(image:UIImage(named:"Sûre logo copy")?.withRenderingMode(.alwaysTemplate))
        self.navigationItem.titleView = imageView
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.default
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
        }
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.tintColor = UIColor.darkGray
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.darkGray]
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.darkGray]
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
    }
    
    
    
    
    @IBAction func backAction(_ sender: AnyObject) {
        // Get the previous Controller.
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
//    func nextAction(){
//        if SharedData.AddDeviceFlow.ADD_DEVICE_FROM == "UPDATE"{
//            if password_input.text  != ""{
//                let connectioJson:NSDictionary = ["channel":"\(selected_channel)","ssid":"\(selected_ssid)","password":"\(password_input.text!)","security":"\(selected_security)","encryption":"\(selected_encryption)","adminToken":"\(SharedData.AddDeviceFlow.ADMIN_TOKEN)","secondary":SharedData.AddDeviceFlow.IS_SECONDARY_WIFI]
//                print(connectioJson)
//                self.conncetion.devicePair(self.deviceAddress + SureConstantResources.AP_UPDATE_ADDRESS, httpMethod: "POST",params: connectioJson, status: { (statuscode) in
//                    if statuscode == 200 {
//                        print("UPDATE SUCCESS")
//                        DispatchQueue.main.async {
//                            self.performSegue(withIdentifier: "next", sender: self)
//                        }
//
//                    }else if statuscode ==  1001{
//                        print("UPDATE FAILED")
//                        DispatchQueue.main.async {
//                            self.performSegue(withIdentifier: "next", sender: self)
//                        }
//                    }
//                })
//            }else{
//                NotificationAlert.showStripBanner(SureConstantResources.PASSWORD_REQUIRED)
//            }
//        }else if SharedData.AddDeviceFlow.ADD_DEVICE_FROM == "NEW"{
//            if password_input.text  != ""{
//                var devicename = ""
//                var deviceLocLat = 0.0
//                var deviceLocLng = 0.0
//                var chime = "false"
//
//                let pref = UserDefaults.standard
//                if let name = pref.object(forKey: SureConstantResources.ADD_DEVICE_NAME){
//                    devicename = (name as? String)!
//                }
//
//                if let lat = pref.object(forKey: SureConstantResources.ADD_DEVICE_LOC_LAT){
//                    deviceLocLat = (lat as? Double)!
//                }
//
//                if let lng = pref.object(forKey: SureConstantResources.ADD_DEVICE_LOC_LNG){
//                    deviceLocLng = (lng as? Double)!
//                }
//
//                if let chim = pref.object(forKey: SureConstantResources.ADD_DEVICE_CHIME){
//                    chime = (chim as? String)!
//                }
//
//                var location = [Double]()
//                location.append(deviceLocLat)
//                location.append(deviceLocLng)
//
//                let connectioJson:NSDictionary = ["channel":"\(selected_channel)","ssid":"\(selected_ssid)","password":"\(password_input.text!)","security":"\(selected_security)","encryption":"\(selected_encryption)","name":"\(devicename)","location":location,"access_token":"\(SharedData.UserDetails.USER_ACCESS_TOKEN)","chime":chime.boolValueFromString()]
//                print(connectioJson)
//                self.conncetion.devicePair(self.deviceAddress +  SureConstantResources.AP_PAIR_ADDRESS, httpMethod: "POST",params: connectioJson, status: { (statuscode) in
//                    if statuscode == 200 {
//                        print("UPDATE SUCCESS")
//                        DispatchQueue.main.async {
//                            self.performSegue(withIdentifier: "next", sender: self)
//                        }
//                    }else if statuscode ==  1001{
//                        print("UPDATE FAILED")
//                        DispatchQueue.main.async {
//                            self.performSegue(withIdentifier: "next", sender: self)
//                        }
//                    }
//                })
//            }else{
//                NotificationAlert.showStripBanner(SureConstantResources.PASSWORD_REQUIRED)
//            }
//        }
//    }
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if (segue.identifier == "next") {
//            let destinationVC:LEDPatternViewController = segue.destination as! LEDPatternViewController
//            destinationVC.wifiName = self.selected_ssid
//        }
//    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 35
        let currentString: NSString =  NSString(string: textField.text!)
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        if (string == "\n") {}
        return newString.length <= maxLength
    }
    
    @objc func togglePasswordVisible(_ sender:AnyObject){
        if(passwordiconClick == true) {
            password_input!.isSecureTextEntry = false
            let origImage = UIImage(named: "ic_eye_open");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            passwordiconClick = false
        } else {
            password_input!.isSecureTextEntry = true
            let origImage = UIImage(named: "ic_eye_closed");
            let tintedImage = origImage?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            btnLeftMenu!.setImage(tintedImage, for: UIControlState())
            btnLeftMenu!.tintColor = UIColor.darkGray
            passwordiconClick = true
        }
    }
    
    func doYourStuff(){
        NotificationCenter.default.removeObserver(self)
    }
    
}

