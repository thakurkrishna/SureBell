//
//  SharedData.swift
//  PiCloud
//
//  Created by Krishna Thakur on 07/02/17.
//  Copyright © 2017 PiOctave. All rights reserved.
//

import Foundation
import UIKit


class SharedData{
    
    class AppSetting{
        static var IS_INTERNET_CONNECTED = false
    }
    
    
    class UserDetails{
        static var USER_ACCESS_TOKEN = "Error"
        static var USER_REFRESH_TOKEN = ""
        static var IS_USER_LOGGED_IN = false
        static var IS_USER_ADMIN = false
    }
    
    //    class DeviceDataInNavigation{
    //        static var DEVICE_ID = ""
    //        static var DEVICE_NAME = ""
    //        static var DEVICE_MAC = ""
    //        static var DEVICE_VERSION = ""
    //        static var DEVICE_TYPE = ""
    //        static var DEVICE_ADMIN_TOKEN = ""
    //        static var VENDOR_ID = ""
    //
    //        static var NAVIGATION_FROM = ""
    //
    //        static var EVENT_ID = ""
    //        static var EVENT_IMAGE_URL = ""
    //        static var EVENT_VIDEO_URL = ""
    //        static var EVENT_SELECTED_DATE = Date()
    //
    //        static var VIDEO_CALL_IMAGE = UIImage()
    //    }
    
    class AddDeviceFlow{
        static var ADD_DEVICE_FROM = ""
        static var ADMIN_TOKEN = ""
        static var DEVICE_TYPE = ""
        static var IS_SECONDARY_WIFI = false
        static var IS_DIGNOSTIC = false
    }
    
}

